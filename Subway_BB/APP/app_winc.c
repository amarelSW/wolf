#include <string.h>
#include <stdlib.h>
#include "driver/include/m2m_wifi.h"
#include "driver/source/nmasic.h"
#include "driver/source/m2m_hif.h"
#include "Config/conf_winc.h"
#include "spi_flash/include/spi_flash.h"
#include "socket/include/socket.h"
#include "spi.h"
#ifdef QSPIFLASH_STORAGE
#include "Class/MSC/Storage/QSPIFlash/usbd_storage.h"
#endif
#ifdef RAMDISK_STORAGE
#include "Class/MSC/Storage/RAMDisk/usbd_storage.h"
#endif
#include "app_log.h"
#include "m2m_ota.h"
#include "app_version.h"

#ifdef UNIT_STP
#define UDP_SERVER
#endif

extern int count_debounce_in, count_debounce_out;
extern uint8_t IWDG_enable_refresh;
void OtaUpdateCb(uint8_t u8OtaUpdateStatusType, uint8_t u8OtaUpdateStatus);

tstrM2MAPConfig strM2MAPConfig;

  void AppWinc(void *p_arg);

  int8_t SendUDPMsg(char* msg, uint16_t size);

  void OtaNotifCb(tstrOtaUpdateInfo *pv);

  struct Sub_Msg
  	{
  		char msgType;
  		short data;
  		short buffLen;
  		char buff[1024];
  	}  __attribute__((packed));

#define SUB_MSG_HEADER_LEN	5

  	typedef enum   {
		READ_NVM_BLOCK		= 0,
		WRITE_NVM_BLOCK		= 1,
		READ_BLOCK_SIZE		= 2,
		READ_NUM_BLOCKS		= 3,
		GET_DEVICE_IP		= 4,
		HALT_DEVICE			= 5,
		TEST_PACKET			= 6,
		GET_WINC_REV		= 7,
		GET_LOGS			= 8,
		START_OTA			= 9,
		GET_IN_OUT_COUNTERS = 10,
		ERASE_LOGS			= 11,
		WRITE_LOG_RECORD	= 12,
		SAVE_LOG_TO_FLASH	= 13,
		START_AP			= 14,
  	} Sub_Msg_Msgtype;



  uint8_t pu8IPAddress[4];

#ifdef TESTAP
/** Wi-Fi Settings */
#define MAIN_WLAN_SSID        "DEMO_AP" /* < Destination SSID */
#define MAIN_WLAN_AUTH        M2M_WIFI_SEC_WPA_PSK /* < Security manner */
#define MAIN_WLAN_PSK         "12345678" /* < Password for Destination SSID */

/** Index of scan list to request scan result. */
static uint8_t scan_request_index = 0;
/** Number of APs found. */
static uint8_t num_founded_ap = 0;

#endif

int8_t lastRssiVal = 0;

/** AP mode Settings */
#define MAIN_WLAN_SSID_SEFAP           "DEMO_AP" /* < SSID */
#if USE_WEP
#define MAIN_WLAN_AUTH           M2M_WIFI_SEC_WEP /* < Security manner */
#define MAIN_WLAN_WEP_KEY        "1234567890" /* < Security Key in WEP Mode */
#define MAIN_WLAN_WEP_KEY_INDEX  (0)
#else
#define MAIN_WLAN_AUTH_SEFAP            M2M_WIFI_SEC_OPEN /* < Security manner */
#endif
#define MAIN_WLAN_CHANNEL_SEFAP         (6) /* < Channel number */



#ifdef UDP_SERVER

/** Wi-Fi Settings */
#define MAIN_OTA_URL          			"http://10.0.0.101/m2m_ota_3a0.bin"
#define MAIN_WLAN_SSID                    "OpenWrt" /**< Destination SSID */
//#define MAIN_WLAN_SSID                    "KabelBox-2A7C" /**< Destination SSID */
#define MAIN_WLAN_AUTH                    M2M_WIFI_SEC_WPA_PSK /**< Security manner */
#define MAIN_WLAN_PSK                    "alexander77"/**< Password for Destination SSID */
//#define MAIN_WLAN_PSK                    "70798428398314259865"  /**< Password for Destination SSID */
#define MAIN_WIFI_M2M_PRODUCT_NAME        "NMCTemp"
#define MAIN_WIFI_M2M_SERVER_IP           0xFFFFFFFF /* 255.255.255.255 */
#define RX_PORT					          (6666)
#define TX_PORT					          (7777)
#define MAIN_WIFI_M2M_REPORT_INTERVAL     (1000)

#define MAIN_WIFI_M2M_BUFFER_SIZE         1460

/** UDP MAX packet count */
#define MAIN_WIFI_M2M_PACKET_COUNT        1024

#define MYBUFFERSIZE                      10

/** Test buffer */
static uint8_t gau8SocketTestBuffer[MAIN_WIFI_M2M_BUFFER_SIZE] = {0};


/** Socket for Rx */
static SOCKET rx_socket = -1;

/** Socket for Tx */
static SOCKET tx_socket = -1;

/** Wi-Fi connection state */
static uint8_t wifi_connected;

/** UDP packet count */
static uint8_t packetCnt = 0;

 int resualt_pack_compare=0;

double precentage;
double N_error_Pack;

struct sockaddr_in rx_addr;
struct sockaddr_in tx_addr;

uint8_t RSSICALLBACK=0;
#endif

static AP_Mode_on = 0;

#ifdef GIGEMAC
/** Mac address information. */
static uint8_t mac_addr[M2M_MAC_ADDRES_LEN];

/** User define MAC Address. */
const char main_user_define_mac_address[] = {0xf8, 0xf0, 0x05, 0x20, 0x0b, 0x09};

#else
/** Mac address information. */
static uint8_t mac_addr[M2M_MAC_ADDRES_LEN];
/** User define MAC Address. */
const char main_user_define_mac_address[] = {0x00, 0xA0, 0xC9, 0x14, 0xC8, 0x29};
#endif

int8_t SendUDPMsg(char* msg, uint16_t size)
{
	return sendto(tx_socket, msg, size, 0, (struct sockaddr *)&tx_addr, sizeof(tx_addr));
}

#ifdef TESTAP

static void wifi_cb(uint8_t u8MsgType, void *pvMsg)
{
	switch (u8MsgType) {
	case M2M_WIFI_RESP_SCAN_DONE:
	{
		tstrM2mScanDone *pstrInfo = (tstrM2mScanDone *)pvMsg;
		scan_request_index = 0;
		if (pstrInfo->u8NumofCh >= 1) {
			m2m_wifi_req_scan_result(scan_request_index);
			scan_request_index++;
		} else {
			m2m_wifi_request_scan(M2M_WIFI_CH_ALL);
		}

		break;
	}

	case M2M_WIFI_RESP_SCAN_RESULT:
	{
		tstrM2mWifiscanResult *pstrScanResult = (tstrM2mWifiscanResult *)pvMsg;
		uint16_t demo_ssid_len;
		uint16_t scan_ssid_len = strlen((const char *)pstrScanResult->au8SSID);

		/* display founded AP. */
		APP_TRACE_DBG(("[%d] SSID:%s\r\n", scan_request_index, pstrScanResult->au8SSID));

		num_founded_ap = m2m_wifi_get_num_ap_found();
		if (scan_ssid_len) {
			/* check same SSID. */
			demo_ssid_len = strlen((const char *)MAIN_WLAN_SSID);
			if
			(
				(demo_ssid_len == scan_ssid_len) &&
				(!memcmp(pstrScanResult->au8SSID, (uint8_t *)MAIN_WLAN_SSID, demo_ssid_len))
			) {
				/* A scan result matches an entry in the preferred AP List.
				 * Initiate a connection request.
				 */
				APP_TRACE_DBG(("Found %s \r\n", MAIN_WLAN_SSID));
				m2m_wifi_connect((char *)MAIN_WLAN_SSID,
						sizeof(MAIN_WLAN_SSID),
						MAIN_WLAN_AUTH,
						(void *)MAIN_WLAN_PSK,
						M2M_WIFI_CH_ALL);
				break;
			}
		}

		if (scan_request_index < num_founded_ap) {
			m2m_wifi_req_scan_result(scan_request_index);
			scan_request_index++;
		} else {
			APP_TRACE_DBG(("can not find AP %s\r\n", MAIN_WLAN_SSID));
			m2m_wifi_request_scan(M2M_WIFI_CH_ALL);
		}

		break;
	}

	case M2M_WIFI_RESP_CON_STATE_CHANGED:
	{
		tstrM2mWifiStateChanged *pstrWifiState = (tstrM2mWifiStateChanged *)pvMsg;
		if (pstrWifiState->u8CurrState == M2M_WIFI_CONNECTED) {
			m2m_wifi_request_dhcp_client();
		} else if (pstrWifiState->u8CurrState == M2M_WIFI_DISCONNECTED) {
			APP_TRACE_DBG(("Wi-Fi disconnected\r\n"));

			/* Request scan. */
			m2m_wifi_request_scan(M2M_WIFI_CH_ALL);
		}

		break;
	}

	case M2M_WIFI_REQ_DHCP_CONF:
	{
		uint8_t *pu8IPAddress = (uint8_t *)pvMsg;
		APP_TRACE_DBG(("Wi-Fi connected\r\n"));
		APP_TRACE_DBG(("Wi-Fi IP is %u.%u.%u.%u\r\n",
				pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3]));
		break;
	}

	default:
	{
		break;
	}
	}
}
#endif



#ifdef UDP_SERVER

CPU_INT08U block_data[512];
struct Sub_Msg subMsgOut;

CPU_INT08U inOTATest = 0;

static void socket_cb(SOCKET sock, uint8_t u8Msg, void *pvMsg)

{
	static int formerVal = 0;
	static size_t numChecks = 0;
	static short lastTestPacket;


	static CPU_INT64U numBlocks;
	static CPU_INT32U blockSize;
	static short missingPackets = 0;
	static short testPacketCount = 0;
	static CPU_INT16U missingPacketsToReport = 0;

	static USBD_STORAGE_LUN storageLun;
	tstrM2mRev strtmp;

	USBD_ERR err;

 	struct sockaddr_in in_addr;
 	tstrWifiInitParam param;
	int8_t ret;
	uint8_t u8IsMacAddrValid;

	int blockNum;

//	APP_TRACE_DBG(("socket_cb: start\r\n"));

	/* Initialize socket address structure. */
	in_addr.sin_family = AF_INET;
	in_addr.sin_port = _htons(RX_PORT);
	in_addr.sin_addr.s_addr = _htonl(MAIN_WIFI_M2M_SERVER_IP);

	if (u8Msg == SOCKET_MSG_BIND)
	{
		tstrSocketBindMsg *pstrBind = (tstrSocketBindMsg *)pvMsg;
		if (pstrBind && pstrBind->status == 0)
		{
			/* Prepare next buffer reception. */
			APP_TRACE_DBG(("socket_cb: bind success!\r\n"));
			recvfrom(sock, gau8SocketTestBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);
		}
		else
		{
			APP_TRACE_DBG(("socket_cb: bind error!\r\n"));
		}
	}
	else if (u8Msg == SOCKET_MSG_RECVFROM)
	{
		tstrSocketRecvMsg *pstrRx = (tstrSocketRecvMsg *)pvMsg;
		if (packetCnt >= MAIN_WIFI_M2M_PACKET_COUNT)
		{
			return;
		}

		if (pstrRx->pu8Buffer && pstrRx->s16BufferSize)
		{
			struct Sub_Msg* subMsgIn = pstrRx->pu8Buffer;
			switch (subMsgIn->msgType)
			{
			case  READ_NUM_BLOCKS:
				/* Send num of blocks */
				storageLun.LunNbr = 0;
				USBD_StorageCapacityGet(&storageLun, &numBlocks, &blockSize, &err);
				subMsgOut.msgType = READ_NUM_BLOCKS;
				subMsgOut.data = numBlocks;
				subMsgOut.buffLen = 0;
				ret = SendUDPMsg((char*)&subMsgOut, SUB_MSG_HEADER_LEN + subMsgOut.buffLen);
				break;
			case READ_BLOCK_SIZE:
				/* Send num of blocks */
				storageLun.LunNbr = 0;
				USBD_StorageCapacityGet(&storageLun, &numBlocks, &blockSize, &err);
				subMsgOut.msgType = READ_BLOCK_SIZE;
				subMsgOut.data = blockSize;
				subMsgOut.buffLen = 0;
				ret = SendUDPMsg((char*)&subMsgOut, SUB_MSG_HEADER_LEN + subMsgOut.buffLen);
				break;
			case GET_DEVICE_IP:
				sprintf(subMsgOut.buff, "IP %u.%u.%u.%u SW Version %s", pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3], APP_VERSION);
				subMsgOut.msgType = GET_DEVICE_IP;
				subMsgOut.data = blockSize;
				subMsgOut.buffLen = strlen(subMsgOut.buff) + 1;
				ret = SendUDPMsg((char*)&subMsgOut, SUB_MSG_HEADER_LEN + subMsgOut.buffLen);
				break;
			case READ_NVM_BLOCK:
				storageLun.LunNbr = 0;
				blockNum = subMsgIn->data;
				USBD_StorageRd (&storageLun, blockNum, 1, subMsgOut.buff, &err);
				subMsgOut.msgType = READ_NVM_BLOCK;
				subMsgOut.data = blockNum;
				subMsgOut.buffLen = blockSize;
				ret = SendUDPMsg((char*)&subMsgOut, SUB_MSG_HEADER_LEN + subMsgOut.buffLen);
				break;
			case WRITE_NVM_BLOCK:
				storageLun.LunNbr = 0;
				blockNum = subMsgIn->data;
				USBD_StorageWr(&storageLun, blockNum, 1, subMsgIn->buff, &err);
				subMsgOut.msgType = WRITE_NVM_BLOCK;
				subMsgOut.data = subMsgIn->data;
				subMsgOut.buffLen = 0;
				ret = SendUDPMsg((char*)&subMsgOut, SUB_MSG_HEADER_LEN + subMsgOut.buffLen);
				break;
			case HALT_DEVICE:
//				 *(__IO uint32_t *) 0xA0003000 = 0xFF;
				IWDG_enable_refresh = 0;
				while (1)
				{
				}
				break;
			case TEST_PACKET:
				m2m_wifi_req_curr_rssi();
				if ((subMsgIn->data == 0) || (subMsgIn->data < lastTestPacket)) // First packet
				{
					missingPackets = 0;
					lastTestPacket = 0;
					testPacketCount = 0;
				}
				if (lastTestPacket + 1 < subMsgIn->data)
				{
					missingPackets +=  subMsgIn->data - (lastTestPacket + 1);
				}
				lastTestPacket = subMsgIn->data;

				if (testPacketCount == 1000)
				{
					missingPacketsToReport = missingPackets;
				}

				subMsgOut.msgType = TEST_PACKET;
				subMsgOut.data = missingPacketsToReport;
				subMsgOut.buffLen = 1;
				subMsgOut.buff[0] = lastRssiVal;
				ret = SendUDPMsg((char*)&subMsgOut, SUB_MSG_HEADER_LEN + subMsgOut.buffLen);
				break;
			case GET_WINC_REV:
				ret = nm_get_firmware_info(&strtmp);
				sprintf(subMsgOut.buff, "Firmware ver: %u.%u.%u\n Min driver ver: %u.%u.%u\n Curr driver ver: %u.%u.%u\n",
						strtmp.u8FirmwareMajor, strtmp.u8FirmwareMinor, strtmp.u8FirmwarePatch,
						strtmp.u8DriverMajor, strtmp.u8DriverMinor, strtmp.u8DriverPatch,
						M2M_DRIVER_VERSION_MAJOR_NO, M2M_DRIVER_VERSION_MINOR_NO, M2M_DRIVER_VERSION_PATCH_NO);
				subMsgOut.msgType = GET_WINC_REV;
				subMsgOut.data = missingPacketsToReport;
				subMsgOut.buffLen = strlen(subMsgOut.buff) + 1;
				ret = SendUDPMsg((char*)&subMsgOut, SUB_MSG_HEADER_LEN + subMsgOut.buffLen);
				break;
			case GET_LOGS:
				subMsgOut.msgType = GET_LOGS;
				if (App_logGetNumLogsToRead() == 0)
				{
					subMsgOut.data 		= -1;
					subMsgOut.buffLen 	= 0;
				}
				else
				{
					App_logReadStr(subMsgOut.buff);
					subMsgOut.data 		= 0;
					subMsgOut.buffLen 	= strlen(subMsgOut.buff) + 1;
				}

				ret = SendUDPMsg((char*)&subMsgOut, SUB_MSG_HEADER_LEN + subMsgOut.buffLen);
				break;
			case START_OTA:
				inOTATest = 1;
			   /* Init ota function. */
			   m2m_ota_init(OtaUpdateCb, OtaNotifCb);
		       /* Start OTA Firmware download. */
		       m2m_ota_start_update((uint8_t *)MAIN_OTA_URL);

				break;
			case GET_IN_OUT_COUNTERS:
				sprintf(subMsgOut.buff, "In counter %d Out counter %d", count_debounce_in, count_debounce_out);
				subMsgOut.msgType = GET_IN_OUT_COUNTERS;
				subMsgOut.data = missingPacketsToReport;
				subMsgOut.buffLen = strlen(subMsgOut.buff) + 1;
				ret = SendUDPMsg((char*)&subMsgOut, SUB_MSG_HEADER_LEN + subMsgOut.buffLen);
				break;
			case ERASE_LOGS:
				App_log_eraseAll();
				break;
			case WRITE_LOG_RECORD:
				App_logWriteStr(subMsgIn->buff);
				break;
			case SAVE_LOG_TO_FLASH:
				App_log_save();
				break;
			case START_AP:
				/* Bring up AP mode with parameters structure. */
				AP_Mode_on = 1;
                m2m_wifi_disconnect();
			   break;
			}

			packetCnt++;
			if (RSSICALLBACK)
			     m2m_wifi_req_curr_rssi();

			if (formerVal+1 < pstrRx->pu8Buffer[0])
			{
				missingPackets = pstrRx->pu8Buffer[0] - (formerVal + 1);
			}
			else if (formerVal+1 < pstrRx->pu8Buffer[0])
			{
				missingPackets = 255 - (formerVal + 1) + pstrRx->pu8Buffer[0];
			}
			formerVal = pstrRx->pu8Buffer[0];
			numChecks++;
			if (numChecks%200==0)
				 m2m_wifi_req_curr_rssi();

			if (numChecks==1000)
			{
				APP_TRACE_DBG(("Packet Count......%d \t", packetCnt));
				m2m_wifi_req_curr_rssi();
				APP_TRACE_DBG(("Missing......%d out of 1000\r\n", missingPackets));
				numChecks = 0;
				missingPackets = 0;
			}


			/* Prepare next buffer reception. */
			recvfrom(sock, gau8SocketTestBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);
		}
		else
		{
			if (pstrRx->s16BufferSize == SOCK_ERR_TIMEOUT)
			{
				/* Prepare next buffer reception. */
				recvfrom(sock, gau8SocketTestBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);
			}
		}
	}
}


static void wifi_cb(uint8_t u8MsgType, void *pvMsg)
{
	APP_TRACE_DBG(("wifi_cb: start\r\n"));

	switch (u8MsgType) {
	case M2M_WIFI_RESP_CON_STATE_CHANGED:
	{
		tstrM2mWifiStateChanged *pstrWifiState = (tstrM2mWifiStateChanged *)pvMsg;
		if (pstrWifiState->u8CurrState == M2M_WIFI_CONNECTED)
		{
			APP_TRACE_DBG(("wifi_cb: M2M_WIFI_RESP_CON_STATE_CHANGED: CONNECTED\r\n"));
			if (!AP_Mode_on)
			{
				m2m_wifi_request_dhcp_client();
			}
		}
		else if (pstrWifiState->u8CurrState == M2M_WIFI_DISCONNECTED)
		{
			APP_TRACE_DBG(("wifi_cb: M2M_WIFI_RESP_CON_STATE_CHANGED: DISCONNECTED\r\n"));
			if (!AP_Mode_on)
			{
				m2m_wifi_req_curr_rssi();
				m2m_wifi_connect((char *)MAIN_WLAN_SSID, sizeof(MAIN_WLAN_SSID), MAIN_WLAN_AUTH, (char *)MAIN_WLAN_PSK, M2M_WIFI_CH_ALL);
			}
			else
			{
			   uint8_t ret = m2m_wifi_enable_ap(&strM2MAPConfig);
			   if (M2M_SUCCESS != ret)
			   {
				   App_logWriteStr("main: m2m_wifi_enable_ap call error!\r\n");
			   }

			   App_logWriteStr("AP mode started. You can connect to %s.\r\n", (char *)MAIN_WLAN_SSID_SEFAP);
			}
		}
	}
	break;

	case M2M_WIFI_REQ_DHCP_CONF:
	{
		/* Create socket for Rx UDP */
		if (rx_socket < 0) {
			if ((rx_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
			{
				APP_TRACE_DBG(("main : failed to create RX UDP Client socket error!\r\n"));
			}

			/* Socket bind */
			bind(rx_socket, (struct sockaddr *)&rx_addr, sizeof(struct sockaddr_in));

		}
		/* Create socket for Tx UDP */
		if (tx_socket < 0)
		{
			if ((tx_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
			{
				APP_TRACE_DBG(("main : failed to create TX UDP client socket error!\r\n"));
			}
		}

		memcpy(pu8IPAddress, (uint8_t *)pvMsg, 4);

		wifi_connected = M2M_WIFI_CONNECTED;

		APP_TRACE_DBG(("wifi_cb: M2M_WIFI_REQ_DHCP_CONF : IP is %u.%u.%u.%u\r\n", pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3]));
#ifdef UNIT_STP
		char buff[80];
		sprintf(buff, "IP:  %u.%u.%u.%u", pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3]);
		SendUDPMsg(buff, strlen(buff));
#endif
	}
	break;

	case M2M_WIFI_RESP_CURRENT_RSSI:
	{
		// This message type is triggered by "m2m_wifi_req_curr_rssi()" function.
		int8_t *rssi = (int8_t *)pvMsg;
		lastRssiVal = *rssi;
		APP_TRACE_DBG(("RSSI for the current connected AP (%d)\r\n", (int8_t)(*rssi)));
		break;
	}

	default:
		break;
	}
}
#endif

void AppWinc(void *p_arg)
  {


#ifdef TESTAP
		tstrWifiInitParam param;
		int8_t ret;


		  //Initialize Wi-Fi parameters structure.
		 memset((uint8_t *)&param, 0, sizeof(tstrWifiInitParam));

		 // Initialize Wi-Fi driver with data and status callbacks.
		 param.pfAppWifiCb = wifi_cb;
		 ret = m2m_wifi_init(&param);
		 if (M2M_SUCCESS != ret) {
			 APP_TRACE_DBG(("main: m2m_wifi_init call error!(%d)\r\n", ret));

		 }


		 // Request scan.
		 	m2m_wifi_request_scan(M2M_WIFI_CH_ALL);

#endif

#ifdef UDP_SERVER
	tstrWifiInitParam param;
	int8_t ret;
	uint8_t u8IsMacAddrValid;

	/* Initialize socket address structure. */
	rx_addr.sin_family = AF_INET;
	rx_addr.sin_port = _htons(RX_PORT);
	rx_addr.sin_addr.s_addr = _htonl(MAIN_WIFI_M2M_SERVER_IP);

	tx_addr.sin_family = AF_INET;
	tx_addr.sin_port = _htons(TX_PORT);
	tx_addr.sin_addr.s_addr = _htonl(MAIN_WIFI_M2M_SERVER_IP);

	/* Initialize Wi-Fi parameters structure. */
	memset((uint8_t *)&param, 0, sizeof(tstrWifiInitParam));

	/* Initialize Wi-Fi driver with data and status callbacks. */
	param.pfAppWifiCb = wifi_cb;
	ret = m2m_wifi_init(&param);
	if (M2M_SUCCESS != ret)
	{
		APP_TRACE_DBG(("main: m2m_wifi_init call error!(%d)\r\n", ret));
		while (1)
		{
		}
	}

	/* Initialize socket module */
	socketInit();
	registerSocketCallback(socket_cb, NULL);

	/* Connect to router. */
	m2m_wifi_connect((char *)MAIN_WLAN_SSID, sizeof(MAIN_WLAN_SSID), MAIN_WLAN_AUTH, (char *)MAIN_WLAN_PSK, M2M_WIFI_CH_ALL);

	packetCnt = 0;

	/* Get MAC Address from OTP. */
	m2m_wifi_get_otp_mac_address(mac_addr, &u8IsMacAddrValid);
	if (!u8IsMacAddrValid) {
		APP_TRACE_DBG(("USER MAC Address : "));

		/* Cannot found MAC Address from OTP. Set user define MAC address. */
		m2m_wifi_set_mac_address((uint8_t *)main_user_define_mac_address);
	} else {
		APP_TRACE_DBG(("OTP MAC Address : "));
	}

	/* Get MAC Address. */
	m2m_wifi_get_mac_address(mac_addr);

	APP_TRACE_DBG(("%02X:%02X:%02X:%02X:%02X:%02X\r\n",
			mac_addr[0], mac_addr[1], mac_addr[2],
			mac_addr[3], mac_addr[4], mac_addr[5]));

	/* Initialize AP mode parameters structure with SSID, channel and OPEN security type. */
	memset(&strM2MAPConfig, 0x00, sizeof(tstrM2MAPConfig));
	strcpy((char *)&strM2MAPConfig.au8SSID, MAIN_WLAN_SSID_SEFAP);
	strM2MAPConfig.u8ListenChannel = MAIN_WLAN_CHANNEL_SEFAP;
	strM2MAPConfig.u8SecType = MAIN_WLAN_AUTH_SEFAP;

	strM2MAPConfig.au8DHCPServerIP[0] = 10;
	strM2MAPConfig.au8DHCPServerIP[1] = 0;
	strM2MAPConfig.au8DHCPServerIP[2] = 0;
	strM2MAPConfig.au8DHCPServerIP[3] = 227;

#endif

	    while (DEF_TRUE) {                                           //Task body, always written as an infinite loop.

#ifdef TESTAP
	    	 while (m2m_wifi_handle_events(NULL) != M2M_SUCCESS) {}
#endif

#ifdef UDP_SERVER

			if (packetCnt >= MAIN_WIFI_M2M_PACKET_COUNT) {
				APP_TRACE_DBG(("UDP Server test Complete!\r\n"));
				close(rx_socket);
				rx_socket = -1;
				break;
			}

			m2m_wifi_handle_events(NULL);

//			static uint32_t diluteNum = 0;
//			if (wifi_connected == M2M_WIFI_CONNECTED)
//			{
//				if (wifi_test == WIFI_TEST_NONE)
//				{
//					diluteNum++;
//					if (diluteNum == 1000000)
//					{
//						char buff[20];
//						sprintf(buff, "IP:  %u.%u.%u.%u", pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3]);
//						SendUDPMsg(buff, strlen(buff));
//						diluteNum = 0;
//						BSP_OS_TimeDlyMs(300);
//					}
//				}
//			}


#endif


	    }

  }


void wifi_cb_ota(uint8_t u8MsgType, void *pvMsg)
{
     switch (u8MsgType) {
     case M2M_WIFI_RESP_CON_STATE_CHANGED:
     {
           tstrM2mWifiStateChanged *pstrWifiState = (tstrM2mWifiStateChanged *)pvMsg;
           if (pstrWifiState->u8CurrState == M2M_WIFI_CONNECTED)
           {
                m2m_wifi_request_dhcp_client();
           }
           else if (pstrWifiState->u8CurrState == M2M_WIFI_DISCONNECTED)
           {
        	   APP_TRACE_DBG(("Wi-Fi disconnected\r\n"));

                /* Connect to defined AP. */
                m2m_wifi_connect((char *)MAIN_WLAN_SSID, sizeof(MAIN_WLAN_SSID), MAIN_WLAN_AUTH, (void *)MAIN_WLAN_PSK, M2M_WIFI_CH_ALL);
           }

           break;
     }

     case M2M_WIFI_REQ_DHCP_CONF:
     {
           uint8_t *pu8IPAddress = (uint8_t *)pvMsg;
           APP_TRACE_DBG(("Wi-Fi connected\r\n"));
           APP_TRACE_DBG(("Wi-Fi IP is %u.%u.%u.%u\r\n",
                      pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3]));
           /* Start OTA Firmware download. */
           m2m_ota_start_update((uint8_t *)MAIN_OTA_URL);
           break;
     }

     default:
     {
           break;
     }
     }
}

void OtaUpdateCb(uint8_t u8OtaUpdateStatusType, uint8_t u8OtaUpdateStatus)
{
	APP_TRACE_DBG(("OtaUpdateCb %d %d\r\n", u8OtaUpdateStatusType, u8OtaUpdateStatus));
     if (u8OtaUpdateStatusType == DL_STATUS) {
           if (u8OtaUpdateStatus == OTA_STATUS_SUCSESS) {
                /* Start Host Controller OTA HERE ... Before switching.... */
        	   APP_TRACE_DBG(("OtaUpdateCb m2m_ota_switch_firmware start.\r\n"));
                m2m_ota_switch_firmware();
           } else {
        	   APP_TRACE_DBG(("OtaUpdateCb FAIL u8OtaUpdateStatus %d\r\n", u8OtaUpdateStatus));
           }
     } else if (u8OtaUpdateStatusType == SW_STATUS) {
           if (u8OtaUpdateStatus == OTA_STATUS_SUCSESS) {
        	   APP_TRACE_DBG(("OTA Success. Press reset your board.\r\n"));
                /* system_reset(); */
           }
     }
}


void OtaNotifCb(tstrOtaUpdateInfo *pv)
{
	APP_TRACE_DBG(("OtaNotifCb \r\n"));
}



