/*
*********************************************************************************************************
*                                            EXAMPLE CODE
*
*               This file is provided as an example on how to use Micrium products.
*
*               Please feel free to use any application code labeled as 'EXAMPLE CODE' in
*               your application products.  Example code may be used as is, in whole or in
*               part, or may be used as a reference only. This file can be modified as
*               required to meet the end-product requirements.
*
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*
*               You can find our product's user manual, API reference, release notes and
*               more information at https://doc.micrium.com.
*               You can contact us at www.micrium.com.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
*                                     ST Microelectronics STM32
*                                              on the
*
*                                           STM3240G-EVAL
*                                         Evaluation Board
*
* Filename      : app.c
* Version       : V1.00
* Programmer(s) : DC
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include  <stdarg.h>
#include  <stdio.h>
#include  <math.h>
#include  <stm32f4xx_hal.h>


#include  <cpu.h>
#include  <lib_math.h>
#include  <lib_mem.h>
#include  <os.h>

#include  <app_cfg.h>
#include  <bsp.h>

#if (APP_CFG_SERIAL_EN == DEF_ENABLED)
#include  <app_serial.h>
#endif

#include "app_usbd_msc.h"
#ifdef STM32446E_EVAL
#include "stm32446e_eval.h"
#endif

#include "driver/include/m2m_wifi.h"
#include "driver/source/nmasic.h"
#include "driver/source/m2m_hif.h"
#include "Config/conf_winc.h"
#include "spi_flash/include/spi_flash.h"
#include "socket/include/socket.h"
#include "spi.h"
#include <app_iwdg.h>
#include "app_log.h"

#include <stdbool.h>

 void AppWinc(void *p_arg);
 void App_iwdg_thread_func( void *p_arg);

/*
*********************************************************************************************************
*                                            LOCAL DEFINES
*********************************************************************************************************
*/

#define  APP_TASK_EQ_0_ITERATION_NBR              16u
#define  APP_TASK_EQ_1_ITERATION_NBR              18u

#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/

                                                                /* --------------- APPLICATION GLOBALS ---------------- */
static  OS_STK       AppTaskStartStk[APP_CFG_TASK_START_STK_SIZE];
static  OS_STK       AppTaskWincStk[APP_CFG_TASK_WINC_STK_SIZE];
static  OS_STK       AppTaskIwdgStk[APP_CFG_TASK_IWDG_STK_SIZE];

                                                                /* ------------- PROBE EXAMPLE VARIABLES -------------- */
        CPU_INT32U   AppProbe_CtrVal;
        CPU_INT32U   AppProbe_Slider;
#if 0
                                                                /* --------------- SEMAPHORE TASK TEST ---------------- */
static  OS_STK       AppTaskEvent0Stk[APP_CFG_TASK_OBJ_STK_SIZE];

static  OS_STK       AppTaskEvent1Stk[APP_CFG_TASK_OBJ_STK_SIZE];
                                                                /* ------------ FLOATING POINT TEST TASK -------------- */
static  OS_STK       AppTaskEq0FpStk[APP_CFG_TASK_WINC_STK_SIZE];
static  OS_STK       AppTaskEq1FpStk[APP_CFG_TASK_WINC_STK_SIZE];
#endif
#if (OS_SEM_EN > 0u)
static  OS_EVENT    *AppTraceSem;
#endif

#if (OS_SEM_EN > 0u)
static  OS_EVENT    *AppTaskObjSem;
#endif

#if (OS_MUTEX_EN > 0u)
static  OS_EVENT    *AppTaskObjMutex;
#endif

#if (OS_Q_EN > 0u)
static  OS_EVENT    *AppTaskObjQ;
#endif

#if (OS_FLAG_EN > 0u)
static  OS_FLAG_GRP *AppTaskObjFlag;
#endif


/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void  AppTaskCreate  (void);
static  void  AppEventCreate (void);

static  void  AppTaskStart   (void  *p_arg);
//static  void AppWinc(void *p_arg);


CPU_BOOLEAN  App_USBD_Init (bool use_ulpi);
#ifdef INCLUDE_WINC
void MX_GPIO_Init(void);
#endif


volatile int ready_in,ready_out;

extern TIM_HandleTypeDef htim3;

/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Arguments   : none
*
* Returns     : none
*
* Notes       : 1) STM32F4xx HAL library initialization:
*                      a) Configures the Flash prefetch, intruction and data caches.
*                      b) Configures the Systick to generate an interrupt. However, the function ,
*                         HAL_InitTick(), that initializes the Systick has been overwritten since Micrium's
*                         RTOS has its own Systick initialization and it is recommended to initialize the
*                         Systick after multitasking has started.
*
*********************************************************************************************************
*/

int main(void)
{
#if (OS_TASK_NAME_EN > 0)
    CPU_INT08U  err;
#endif

    HAL_Init();                                                 /* See Note 1.                                          */

    Mem_Init();                                                 /* Initialize Memory Managment Module                   */
    Math_Init();                                                /* Initialize Mathematical Module                       */

    BSP_IntDisAll();                                            /* Disable all Interrupts.                              */

    OSInit();                                                   /* Init uC/OS-II.  7                                     */

    OSTaskCreateExt( AppTaskStart,                              /* Create the start task                                */
                     0,
                    &AppTaskStartStk[APP_CFG_TASK_START_STK_SIZE - 1],
                     APP_CFG_TASK_START_PRIO,
                     APP_CFG_TASK_START_PRIO,
                    &AppTaskStartStk[0],
                     APP_CFG_TASK_START_STK_SIZE,
                     0,
                    (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));

#if (OS_TASK_NAME_EN > 0)
    OSTaskNameSet(         APP_CFG_TASK_START_PRIO,
                  (INT8U *)"Start Task",
                           &err);
#endif

//    WWDG_Init(); // Init WatchDog

    OSStart();                                                  /* Start multitasking (i.e. give control to uC/OS-II).  */

    while (DEF_ON) {                                            /* Should Never Get Here.                               */
        ;
    }
}


/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

bool is_ulpi_port_occupied()
{
	return HAL_GPIO_ReadPin(USB0_SEL_HW_GPIO_Port, USB0_SEL_HW_Pin) == GPIO_PIN_SET;
}

bool is_fs_port_occupied()
{
	return HAL_GPIO_ReadPin(USB0_SEL_HW_GPIO_Port, USB1_SEL_HW_Pin) == GPIO_PIN_SET;
}

void enable_ulpi_dok(bool enable)
{
	HAL_GPIO_WritePin(USB0_SEL_SW_GPIO_Port, USB0_SEL_SW_Pin, enable?GPIO_PIN_RESET:GPIO_PIN_SET);
}


void enable_fs_dok(bool enable)
{
	HAL_GPIO_WritePin(USB1_SEL_SW_GPIO_Port, USB1_SEL_SW_Pin, enable?GPIO_PIN_RESET:GPIO_PIN_SET);
}

static  void  AppTaskStart (void *p_arg)
{

   (void)p_arg;


    BSP_Init();                                                 /* Initialize BSP functions								*/
//    MX_GPIO_Init();
    CPU_Init();                                                 /* Initialize the uC/CPU services                       */

    App_log_init();

#if (OS_TASK_STAT_EN > 0)
    OSStatInit();                                               /* Determine CPU capacity                               */
#endif

#ifdef CPU_CFG_INT_DIS_MEAS_EN
    CPU_IntDisMeasMaxCurReset();
#endif

#if (APP_CFG_SERIAL_EN == DEF_ENABLED)
    App_SerialInit();                                           /* Initialize Serial Communication for Application ...  */
#endif

    APP_TRACE_DBG(("Creating Application Events\n\r"));
    AppEventCreate();                                           /* Create Applicaiton kernel objects                    */

    APP_TRACE_DBG(("Creating Application Tasks\n\r"));
    AppTaskCreate();                                            /* Create Application tasks                             */


//    BSP_LED_Off(0u);
bool use_ulpi = true;
#ifndef USE_ULPI
	use_ulpi = false;
#endif

#if (APP_CFG_USBD_EN == DEF_ENABLED)
    App_USBD_Init(use_ulpi);
#endif

    	Pre_Config_Winc();
     MX_SPI4_Init();
     MX_TIM3_Init();
     HAL_TIM_Base_Start_IT(&htim3);

     nm_bsp_init();
     APP_TRACE_DBG(("Winc wifi up\n\r"));

    if (HAL_GPIO_ReadPin(USB0_SEL_HW_GPIO_Port, USB0_SEL_HW_Pin) == GPIO_PIN_SET)
	 {
    	 HAL_GPIO_WritePin(USB0_SEL_SW_GPIO_Port, USB0_SEL_SW_Pin, GPIO_PIN_SET);
	 }
	 if (HAL_GPIO_ReadPin(USB0_SEL_HW_GPIO_Port, USB0_SEL_HW_Pin) == GPIO_PIN_RESET)
	 {
		 HAL_GPIO_WritePin(USB0_SEL_SW_GPIO_Port, USB0_SEL_SW_Pin, GPIO_PIN_RESET);
	 }
#if 0
     if (is_ulpi_port_occupied())
	 {
    	 enable_ulpi_dok(false);
    	 if (is_fs_port_occupied())
    	 {
    		 enable_fs_dok(true);
    	 }
    	 else
    	 {
    		 enable_fs_dok(false);
    	 }
 	 }
     else
     {
    	 enable_ulpi_dok(true);
    	 enable_fs_dok(false);
     }
#endif

    // Dump USB regs
//              int i=0;
//              static CPU_REG32 buff[0xBF0];
//              static char str[20];
//              for (i=0; i<0xBF0; i++)
//              {
//              	buff[i] = *((CPU_REG32*)0x40040000 + i);
//              	sprintf(str, "%d \r\n", buff[i]);
//              	APP_TRACE_DBG((str));
//              }

#ifdef STM32446E_EVAL
    BSP_LED_Off(LED1);
    BSP_LED_Off(LED3);
#endif
    AppProbe_CtrVal = 0u;
    AppProbe_Slider = 200u;

    while (DEF_TRUE) {                                          /* Task body, always written as an infinite loop.       */
//        BSP_LED_Toggle(0u);
#ifdef STM32446E_EVAL
        BSP_LED_Toggle(LED1);
        BSP_LED_Toggle(LED3);
#endif
        OSTimeDlyHMSM(0u, 0u, 0u, (AppProbe_Slider));

        AppProbe_CtrVal++;

         if (AppProbe_CtrVal >= DEF_INT_32_MASK) {
            AppProbe_CtrVal = 0u;
        }
    }
}

/*
*********************************************************************************************************
*                                          AppTaskCreate()
*
* Description : Create Application Tasks.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : AppTaskStart()
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  AppTaskCreate (void)
{

#if (OS_TASK_NAME_EN > 0)
    CPU_INT08U  err;
#endif


    OSTaskCreateExt( AppWinc,
                     0,
                    &AppTaskWincStk[APP_CFG_TASK_WINC_STK_SIZE - 1],
					APP_CFG_TASK_WINC_WIFI_PRIO,
                     10,
                    &AppTaskWincStk[0],
                     APP_CFG_TASK_WINC_STK_SIZE,
                     0,
                    (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR | OS_TASK_OPT_SAVE_FP));

#if (OS_TASK_NAME_EN > 0)
    OSTaskNameSet(         APP_CFG_TASK_WINC_WIFI_PRIO,
                  (INT8U *)"WINC WIFI ",
                           &err);
#endif

#if 0
    OSTaskCreateExt( App_iwdg_thread_func,
                     0,
                    &AppTaskIwdgStk[APP_CFG_TASK_IWDG_STK_SIZE - 1],
					APP_CFG_TASK_IWDG_PRIO,
                     11,
                    &AppTaskIwdgStk[0],
					APP_CFG_TASK_IWDG_STK_SIZE,
                     0,
                    (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR | OS_TASK_OPT_SAVE_FP));

#if (OS_TASK_NAME_EN > 0)
    OSTaskNameSet(         APP_CFG_TASK_IWDG_PRIO,
                  (INT8U *)"WAtchDog ",
                           &err);
#endif

#endif

#if 0
#if (OS_TASK_NAME_EN > 0)
    CPU_INT08U  err;
#endif
                                                                /* ----------- CREATE KERNEL EVENT TEST TASK ---------- */
    OSTaskCreateExt( AppTaskEvent0,
                     0,
                    &AppTaskEvent0Stk[APP_CFG_TASK_OBJ_STK_SIZE - 1],
                     APP_CFG_TASK_EVENT0_PRIO,
                     APP_CFG_TASK_EVENT0_PRIO,
                    &AppTaskEvent0Stk[0],
                     APP_CFG_TASK_OBJ_STK_SIZE,
                     0,
                    (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));

#if (OS_TASK_NAME_EN > 0)
    OSTaskNameSet(         APP_CFG_TASK_EVENT0_PRIO,
                  (INT8U *)"Kernel Events Task 0",
                           &err);
#endif

    OSTaskCreateExt( AppTaskEvent1,
                     0,
                    &AppTaskEvent1Stk[APP_CFG_TASK_OBJ_STK_SIZE - 1],
                     APP_CFG_TASK_EVENT1_PRIO,
                     APP_CFG_TASK_EVENT1_PRIO,
                    &AppTaskEvent1Stk[0],
                     APP_CFG_TASK_OBJ_STK_SIZE,
                     0,
                    (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));

#if (OS_TASK_NAME_EN > 0)
    OSTaskNameSet(         APP_CFG_TASK_EVENT1_PRIO,
                  (INT8U *)"Kernel Events Task 1",
                           &err);
#endif

                                                             /* ------------- CREATE FLOATING POINT TASK ----------- */
    OSTaskCreateExt( AppTaskEq0Fp,
                     0,
                    &AppTaskEq0FpStk[APP_CFG_TASK_WINC_STK_SIZE - 1],
                     APP_CFG_TASK_EQ0_PRIO,
                     APP_CFG_TASK_EQ0_PRIO,
                    &AppTaskEq0FpStk[0],
                     APP_CFG_TASK_WINC_STK_SIZE,
                     0,
                    (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR | OS_TASK_OPT_SAVE_FP));

#if (OS_TASK_NAME_EN > 0)
    OSTaskNameSet(         APP_CFG_TASK_EQ0_PRIO,
                  (INT8U *)"FP  Equation 0",
                           &err);
#endif

    OSTaskCreateExt( AppTaskEq1Fp,
                     0,
                    &AppTaskEq1FpStk[APP_CFG_TASK_WINC_STK_SIZE - 1],
                     APP_CFG_TASK_EQ1_PRIO,
                     APP_CFG_TASK_EQ1_PRIO,
                    &AppTaskEq1FpStk[0],
                     APP_CFG_TASK_WINC_STK_SIZE,
                     0,
                    (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR | OS_TASK_OPT_SAVE_FP));

#if (OS_TASK_NAME_EN > 0)
    OSTaskNameSet(         APP_CFG_TASK_EQ1_PRIO,
                  (INT8U *)"FP  Equation 1",
                           &err);
#endif
#endif
}


/*
*********************************************************************************************************
*                                           AppEventCreate()
*
* Description : Create application kernel event tasks.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : AppTaskStart()
*
* Note(s)     : none.
*********************************************************************************************************
*/
static  void  AppEventCreate (void)
{
    CPU_INT08U  os_err;

#if (OS_SEM_EN > 0u)
    AppTaskObjSem = OSSemCreate(0);
    AppTraceSem   = OSSemCreate(1);

#if (OS_EVENT_NAME_EN > 0)
    OSEventNameSet(         AppTaskObjSem,
                   (INT8U *)"Trace Lock",
                            &os_err);

    OSEventNameSet(         AppTraceSem,
                   (INT8U *)"Sem Test",
                            &os_err);
#endif
#endif

#if (OS_MUTEX_EN > 0u)
    AppTaskObjMutex = OSMutexCreate( APP_CFG_MUTEX_PRIO,
                                     &os_err);

#if (OS_EVENT_NAME_EN > 0)
    OSEventNameSet(         AppTaskObjMutex,
                   (INT8U *)"Mutex Test",
                            &os_err);
#endif
#endif

#if (OS_Q_EN > 0u)
    AppTaskObjQ = OSQCreate((void *)&AppTaskObjQ,
                                      1);

#if (OS_EVENT_NAME_EN > 0)
    OSEventNameSet(          AppTaskObjQ,
                   (INT8U *)"Queue Test",
                            &os_err);
#endif
#endif

#if (OS_FLAG_EN > 0u)
    AppTaskObjFlag = OSFlagCreate( DEF_BIT_NONE,
                                   &os_err);

#if (OS_EVENT_NAME_EN > 0)
    OSFlagNameSet(         AppTaskObjFlag,
                  (INT8U *)"Flag Test",
                           &os_err);
#endif
#endif
}


/*
*********************************************************************************************************
*                                          AppTaskEvent0()
*
* Description : Test uC/OS-II Events.
*
* Argument(s) : p_arg is the argument passed to 'AppTaskEvent0' by 'OSTaskCreate()'.
*
* Return(s)   : none
*
* Caller(s)   : This is a task
*
* Note(s)     : none.
*********************************************************************************************************
*/
#if 0
static  void  AppTaskEvent0 (void  *p_arg)
{
    CPU_INT08U  os_err;

    (void)p_arg;


    while (DEF_TRUE) {
#if (OS_SEM_EN > 0u)
        os_err = OSSemPost(AppTaskObjSem);
#endif

#if (OS_MUTEX_EN > 0u)
        OSMutexPend( AppTaskObjMutex,
                     0,
                    &os_err);

        OSTimeDlyHMSM(0u, 0u, 0u, 100u);

        os_err = OSMutexPost(AppTaskObjMutex);
#endif

#if (OS_Q_EN > 0u)
        os_err = OSQPost(        AppTaskObjQ,
                         (void *)1u);
#endif

#if (OS_FLAG_EN > 0u)
        OSFlagPost( AppTaskObjFlag,
                    DEF_BIT_00,
                    OS_FLAG_SET,
                   &os_err);
#endif
        OSTimeDlyHMSM(0u, 0u, 0u, 10u);
        APP_TRACE_INFO(("Event test task 0 running ....\n"));
    }
}


/*
*********************************************************************************************************
*                                          AppTaskEvent1()
*
* Description : Test uC/OS-II events.
*
* Argument(s) : p_arg is the argument passed to 'AppTaskEvent1' by 'OSTaskCreate()'.
*
* Return(s)   : none
*
* Caller(s)   : This is a task
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  AppTaskEvent1 (void  *p_arg)
{
    CPU_INT08U  os_err;


    (void)p_arg;

    while (DEF_TRUE) {

#if (OS_SEM_EN > 0u)
        OSSemPend( AppTaskObjSem,
                   0,
                  &os_err);
#endif

#if (OS_MUTEX_EN > 0u)
        OSMutexPend( AppTaskObjMutex,
                     0,
                    &os_err);

        os_err = OSMutexPost(AppTaskObjMutex);
#endif

#if (OS_Q_EN > 0u)
        OSQPend( AppTaskObjQ,
                 0,
                &os_err);
#endif

#if (OS_FLAG_EN > 0u)
        OSFlagPend( AppTaskObjFlag,
                    DEF_BIT_00,
                    OS_FLAG_WAIT_SET_ALL + OS_FLAG_CONSUME,
                    0,
                   &os_err);
#endif
        OSTimeDlyHMSM(0u, 0u, 0u, 10u);
        APP_TRACE_INFO(("Event test task 1 running ....\n"));
    }
}
#endif

/*
*********************************************************************************************************
*                                             App_Trace()
*
* Description : Thread-safe version of printf
*
* Argument(s) : Format string follwing the C format convention..
*
* Return(s)   : none.
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  App_Trace (CPU_CHAR *format, ...)
{
    CPU_CHAR  buf_str[80 + 1];
    va_list   v_args;
    CPU_INT08U  os_err;


    va_start(v_args, format);
   (void)vsnprintf((char       *)&buf_str[0],
                   (size_t      ) sizeof(buf_str),
                   (char const *) format,
                                  v_args);
    va_end(v_args);

    OSSemPend((OS_EVENT  *)AppTraceSem,
              (INT32U     )0,
              (INT8U     *)&os_err);

    printf("%s", buf_str);

    os_err = OSSemPost((OS_EVENT *)AppTraceSem);
}



#if 0
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOI, GPIO_PIN_7|GPIO_PIN_3|GPIO_PIN_6|GPIO_PIN_5
                          |GPIO_PIN_10|GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pins : PI2 PI1 PI0 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_1|GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : PD1 PD7 PD4 PD2
                           PD5 PD3 PD0 PD6
                           PD13 PD11 PD15 PD14
                           PD8 PD12 PD10 PD9 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_7|GPIO_PIN_4|GPIO_PIN_2
                          |GPIO_PIN_5|GPIO_PIN_3|GPIO_PIN_0|GPIO_PIN_6
                          |GPIO_PIN_13|GPIO_PIN_11|GPIO_PIN_15|GPIO_PIN_14
                          |GPIO_PIN_8|GPIO_PIN_12|GPIO_PIN_10|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PG12 PG11 PG10 PG15
                           PG13 PG5 PG4 PG6
                           PG3 PG2 PG8 PG7
                           PG0 PG1 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_11|GPIO_PIN_10|GPIO_PIN_15
                          |GPIO_PIN_13|GPIO_PIN_5|GPIO_PIN_4|GPIO_PIN_6
                          |GPIO_PIN_3|GPIO_PIN_2|GPIO_PIN_8|GPIO_PIN_7
                          |GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : PI7 PI3 PI6 PI5
                           PI10 PI9 */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_3|GPIO_PIN_6|GPIO_PIN_5
                          |GPIO_PIN_10|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : PH13 PH14 PH15 PH5
                           PH9 PH10 PH8 PH11
                           PH3 PH12 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15|GPIO_PIN_5
                          |GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_8|GPIO_PIN_11
                          |GPIO_PIN_3|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : PB8 PB9 PB6 PB15
                           PB14 PB2 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_6|GPIO_PIN_15
                          |GPIO_PIN_14|GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PC12 PC10 PC13 PC14
                           PC15 PC6 PC8 PC9
                           PC7 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_10|GPIO_PIN_13|GPIO_PIN_14
                          |GPIO_PIN_15|GPIO_PIN_6|GPIO_PIN_8|GPIO_PIN_9
                          |GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PI4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA10 PA12 PA11 PA1
                           PA0 PA4 PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_12|GPIO_PIN_11|GPIO_PIN_1
                          |GPIO_PIN_0|GPIO_PIN_4|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PE3 */
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PE2 PE14 PE9 PE13
                           PE11 PE7 PE15 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_14|GPIO_PIN_9|GPIO_PIN_13
                          |GPIO_PIN_11|GPIO_PIN_7|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PF0 PF2 PF15 PF1
                           PF3 PF5 PF13 PF4
                           PF11 PF10 PF14 PF12 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_2|GPIO_PIN_15|GPIO_PIN_1
                          |GPIO_PIN_3|GPIO_PIN_5|GPIO_PIN_13|GPIO_PIN_4
                          |GPIO_PIN_11|GPIO_PIN_10|GPIO_PIN_14|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pin : PA2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PC1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

}
#endif

