/*
 * app_IWDG.c
 *
 */


  /*##-2- Configure the IWDG peripheral ######################################*/
  /* IWDG clock counter = (PCLK1 (45MHz)/4096)/8) = 1373 Hz (~728 us)
     IWDG Window value = 80 means that the IWDG counter should be refreshed only
     when the counter is below 80 (and greater than 64/0x40) otherwise a reset will
     be generated.
     IWDG Counter value = 127, IWDG timeout = ~728 us * 64 = 46 ms */
/* USER CODE END 0 */
#include "stm32f4xx_hal.h"
#include "app_cfg.h"
#include "uCOS_II.H"
#include "bsp.h"
#include "app_log.h"
//#include "os.h"





#define IWDG_TIMER_PERIOD	200

void App_IWDG_handler(void *ptmr, void *p_arg);
static uint32_t GetLSIFrequency(void);
void TIM5_IRQHandler(void);

IWDG_HandleTypeDef IwdgHandle;

TIM_HandleTypeDef  TimInputCaptureHandle;
RCC_ClkInitTypeDef RCC_ClockFreq;

uint16_t tmpCC4[2] = {0, 0};

static __IO uint32_t uwLsiFreq = 0;
__IO uint32_t uwCaptureNumber = 0;
__IO uint32_t uwPeriodValue = 0;
__IO uint32_t uwMeasurementDone = 0;

uint8_t IWDG_enable_refresh = 1;

/* IWDG init function */
void IWDG_Init(void)
{

	  BSP_IntVectSet (BSP_INT_ID_TIM5, TIM5_IRQHandler);

	  /*##-2- Get the LSI frequency: TIM5 is used to measure the LSI frequency ###*/
	   uwLsiFreq = GetLSIFrequency();

	   /*##-3- Configure the IWDG peripheral ######################################*/
	   /* Set counter reload value to obtain 250ms IWDG TimeOut.
	      IWDG counter clock Frequency = LsiFreq / 32
	      Counter Reload Value = 250ms / IWDG counter clock period
	                           = 0.25s / (32/LsiFreq)
	                           = LsiFreq / (32 * 4)
	                           = LsiFreq / 128 */
	   IwdgHandle.Instance = IWDG;

	   IwdgHandle.Init.Prescaler = IWDG_PRESCALER_32;
	   IwdgHandle.Init.Reload    = uwLsiFreq / 128;


  App_logWriteStr(("WatchDog has started\r\n"));

  if (HAL_IWDG_Init(&IwdgHandle) != HAL_OK)
  {
 		App_logWriteStr(("Error in HAL_IWDG_Init\r\n"));
  }

}

#if 0
void HAL_IWDG_MspInit(IWDG_HandleTypeDef* IWDGHandle)
{

//	IWDG_Init();

  if(IWDGHandle->Instance==IWDG)
  {

  /* USER CODE BEGIN IWDG_MspInit 0 */

  /* USER CODE END IWDG_MspInit 0 */
    /* IWDG clock enable */
    __HAL_RCC_IWDG_CLK_ENABLE();
  /* USER CODE BEGIN IWDG_MspInit 1 */

  /* USER CODE END IWDG_MspInit 1 */
  }
}
#endif

void App_iwdg_thread_func( void *p_arg)
{
	  IWDG_Init();

	/*##-1- Check if the system has resumed from IWDG reset ####################*/
	  if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != RESET)
	  {		App_logWriteStr(("System has resumed from WatchDog reset\r\n"));

	    /* Clear reset flags */
	    __HAL_RCC_CLEAR_RESET_FLAGS();

		App_logWriteStr(("*********************************************************\r\n"));
		App_logWriteStr(("********                                        *********\r\n"));
		App_logWriteStr(("********            N O T E :                    *********\r\n"));
		App_logWriteStr(("********   System resumed from WatchDog reset   *********\r\n"));
		App_logWriteStr(("********                                        *********\r\n"));
		App_logWriteStr(("*********************************************************\r\n"));
	  }

	  /* USER CODE BEGIN 2 */
	  App_logWriteStr(("WatchDog has started\r\n"));
	  /* USER CODE END 2 */

	  /* Infinite loop */
	  /* USER CODE BEGIN WHILE */
	  while (1)
	  {
	  /* USER CODE END WHILE */

	  /* USER CODE BEGIN 3 */
		  OSTimeDly(IWDG_TIMER_PERIOD);

		  if (IWDG_enable_refresh)
		  {

		  if (HAL_IWDG_Refresh(&IwdgHandle) != HAL_OK)
	     {
			  App_logWriteStr(("HAL_IWDG_Refresh error!\r\n"));
	     }
		  }
	  }
	  /* USER CODE END 3 */

}

/* USER CODE BEGIN 1 */
void TIM5_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&TimInputCaptureHandle);
}


void HAL_TIM_IC_MspInit(TIM_HandleTypeDef *htim)
{
  /* TIMx Peripheral clock enable */
	__HAL_RCC_TIM5_CLK_ENABLE();


  /* Configure the NVIC for TIMx */
  HAL_NVIC_SetPriority(TIM5_IRQn, 0, 0);

  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIM5_IRQn);
}

/**
  * @brief  DeInitializes TIM Input Capture MSP.
  * @param  htim: TIM handle
  * @retval None
  */
void HAL_TIM_IC_MspDeInit(TIM_HandleTypeDef *htim)
{
   /* Enable the TIMx global Interrupt */
  HAL_NVIC_DisableIRQ(TIM5_IRQn);

  /* TIMx Peripheral clock disable */
  __HAL_RCC_TIM5_CLK_DISABLE();

}


void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
  /* Get the Input Capture value */
  tmpCC4[uwCaptureNumber++] = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);

  if (uwCaptureNumber >= 2)
  {
    /* Compute the period length */
    uwPeriodValue = (uint16_t)(0xFFFF - tmpCC4[0] + tmpCC4[1] + 1);
    uwMeasurementDone = 1;
    uwCaptureNumber = 0;
  }
}

static uint32_t GetLSIFrequency(void)
{
  uint32_t pclk1 = 0, latency = 0;
  TIM_IC_InitTypeDef timinputconfig = {0};
  RCC_OscInitTypeDef oscinit = {0};
  RCC_ClkInitTypeDef  clkinit =  {0};

  /* Enable LSI Oscillator */
  oscinit.OscillatorType 	= RCC_OSCILLATORTYPE_LSI;
  oscinit.LSIState 			= RCC_LSI_ON;
  oscinit.PLL.PLLState 		= RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&oscinit)!= HAL_OK)
  {
	  App_logWriteStr(("HAL_RCC_OscConfig error!\r\n"));
	  return 1;
  }

  /* Configure the TIM peripheral */
  /* Set TIMx instance */
  TimInputCaptureHandle.Instance = TIM5;

  /* TIMx configuration: Input Capture mode ---------------------
  The LSI clock is connected to TIM5 CH4.
  The Rising edge is used as active edge.
  The TIM5 CCR4 is used to compute the frequency value.
  ------------------------------------------------------------ */
  TimInputCaptureHandle.Init.Prescaler         = 0;
  TimInputCaptureHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TimInputCaptureHandle.Init.Period            = 0xFFFF;
  TimInputCaptureHandle.Init.ClockDivision     = 0;
  TimInputCaptureHandle.Init.RepetitionCounter = 0;

  if (HAL_TIM_IC_Init(&TimInputCaptureHandle) != HAL_OK)
  {
    /* Initialization Error */
	  App_logWriteStr(("HAL_TIM_IC_Init error!\r\n"));
	  return 1;

  }
  /* Connect internally the  TIM5 CH4 Input Capture to the LSI clock output */
  HAL_TIMEx_RemapConfig(&TimInputCaptureHandle, TIM_TIM5_LSI);

  /* Configure the Input Capture of channel 4 */
  timinputconfig.ICPolarity  = TIM_ICPOLARITY_RISING;
  timinputconfig.ICSelection = TIM_ICSELECTION_DIRECTTI;
  timinputconfig.ICPrescaler = TIM_ICPSC_DIV8;
  timinputconfig.ICFilter    = 0;

  if (HAL_TIM_IC_ConfigChannel(&TimInputCaptureHandle, &timinputconfig, TIM_CHANNEL_4) != HAL_OK)
  {
    /* Initialization Error */
	  App_logWriteStr(("HAL_TIM_IC_ConfigChannel error!\r\n"));
	  return 1;
  }

  /* Reset the flags */
  TimInputCaptureHandle.Instance->SR = 0;

  /* Start the TIM Input Capture measurement in interrupt mode */
  if (HAL_TIM_IC_Start_IT(&TimInputCaptureHandle, TIM_CHANNEL_4) != HAL_OK)
  {
    /* Starting Error */
	  App_logWriteStr(("HAL_TIM_IC_Start_IT error!\r\n"));
	  return 1;
  }

  /* Wait until the TIM5 get 2 LSI edges (refer to TIM5_IRQHandler() in
  stm32f4xx_it.c file) */
  while (uwMeasurementDone == 0)
  {
  }
  uwCaptureNumber = 0;

  /* Deinitialize the TIM5 peripheral registers to their default reset values */
  HAL_TIM_IC_DeInit(&TimInputCaptureHandle);

  /* Compute the LSI frequency, depending on TIM5 input clock frequency (PCLK1)*/
  /* Get PCLK1 frequency */
  pclk1 = HAL_RCC_GetPCLK1Freq();
  HAL_RCC_GetClockConfig(&clkinit, &latency);

  /* Get PCLK1 prescaler */
  if ((clkinit.APB1CLKDivider) == RCC_HCLK_DIV1)
  {
    /* PCLK1 prescaler equal to 1 => TIMCLK = PCLK1 */
    return ((pclk1 / uwPeriodValue) * 8);
  }
  else
  {
    /* PCLK1 prescaler different from 1 => TIMCLK = 2 * PCLK1 */
    return (((2 * pclk1) / uwPeriodValue) * 8) ;
  }
}

