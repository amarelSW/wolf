/*
 * app_log.c
 *

 */

#include "app_log.h"
#include "string.h"
#include "stdarg.h"
#include "stdio.h"
#include "ucos_ii.h"
#include "stm32f4xx_hal.h"


#define MAX_LOG_STR 		80
#define MAX_LOG_RECORDS		100

// The log size is its records + 3 counters
#define MAX_LOG_SIZE 	(MAX_LOG_RECORDS*MAX_LOG_STR + 3*sizeof(CPU_INT32U))

#define LOG_FLASH_SECTOR		FLASH_SECTOR_11
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */
#define FLASH_USER_START_ADDR   ADDR_FLASH_SECTOR_11   /* Start @ of user Flash area */
#define FLASH_SECTOR_SIZE		(128*1024)
#define FLASH_USER_END_ADDR     ADDR_FLASH_SECTOR_11  + FLASH_SECTOR_SIZE - 1 /* End @ of user Flash area : sector start address + sector size -1 */

char LogSectorCopy[MAX_LOG_SIZE];

uint32_t*	pLog_read_pos 	;
uint32_t*	pLog_write_pos 	;
uint32_t*	pLog_num_records ;
char*		pLog_buff;


OS_EVENT* 	log_sem = (void *)0;

CPU_BOOLEAN	log_sem_created = OS_FALSE;
static FLASH_EraseInitTypeDef EraseInitStruct;

void App_log_update_flash()
{
	 /* Unlock the Flash to enable the flash control register access *************/
	  HAL_FLASH_Unlock();

	uint32_t* data_address = 0;
	uint32_t flash_address = 0;
	uint32_t SECTORError = 0;
	uint32_t i = 0;

	EraseInitStruct.TypeErase     = FLASH_TYPEERASE_SECTORS;
	EraseInitStruct.VoltageRange  = FLASH_VOLTAGE_RANGE_3;
	EraseInitStruct.Sector        = LOG_FLASH_SECTOR;
	EraseInitStruct.NbSectors     = 1;

	HAL_StatusTypeDef err = HAL_FLASHEx_Erase(&EraseInitStruct, &SECTORError);
	if ( err != HAL_OK)
	{
		App_logWriteStr("Error in erasing flash sector, err %d SECTOR err %d", err, SECTORError);
	}

	data_address = (uint32_t*)(&LogSectorCopy[0]);
	flash_address = FLASH_USER_START_ADDR;

	while (i < MAX_LOG_SIZE)
	{
		uint32_t data32 = *(data_address + (i/4));
		err = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, flash_address+i, data32);
		if ( err != HAL_OK)
		{
			App_logWriteStr("Error in writing log word, err %d", err);
			break;
		}

		i += 4;
	}


	/* Lock the Flash to disable the flash control register access (recommended
	     to protect the FLASH memory against possible unwanted operation) *********/
	  HAL_FLASH_Lock();
}
void App_log_init()
{
	uint32_t i = 0;

	pLog_read_pos 	= (uint32_t*)(&LogSectorCopy[0]);
	pLog_write_pos 	= pLog_read_pos + 1;
	pLog_num_records= pLog_write_pos + 1;
	pLog_buff		= (char*)(pLog_num_records + 1);

	// Check if we are at the very first use of the flash
	char isFlashEmpty = 1;
	for (i = 0; i<16; i++)
	{
		if (*(char*)(FLASH_USER_START_ADDR+i) != 255)
		{
		  isFlashEmpty = 0;
		  break;
		}
	}

	if (isFlashEmpty)
	{
		*pLog_read_pos 	= 0;
		*pLog_write_pos 	= 0;
		*pLog_num_records = 0;
	}
	else
	{
		memcpy(&LogSectorCopy[0], (char*)FLASH_USER_START_ADDR, MAX_LOG_SIZE);
		// Calculate read pos as first record of the log list, write_pos - num_records
		if (*pLog_write_pos < *pLog_num_records)
		{
			*pLog_read_pos = MAX_LOG_RECORDS + *pLog_write_pos - *pLog_num_records;
		}
		else
		{
			*pLog_read_pos = *pLog_write_pos - *pLog_num_records;
		}
	}
}

void  log_semLock(INT8U  *perr)
{
	CPU_SR_ALLOC();

	if (!log_sem_created)
	{
		CPU_CRITICAL_ENTER();
		log_sem = OSSemCreate(1);
		if (log_sem == (void *)0)
		{
			CPU_CRITICAL_EXIT();
			*perr = 1;
			return;
		}
		log_sem_created = OS_TRUE;
		CPU_CRITICAL_EXIT();
	}

    OSSemPend(log_sem, 0, perr);
}

void  log_semRelease(INT8U  *perr)
{
	if (!log_sem_created)
	{
		*perr = 2;
		return;
	}
	OSSemPost(log_sem);
}

void App_logCyclicAdvance(uint32_t* pos)
{
	(*pos)++;
	if (*pos == MAX_LOG_RECORDS)
	{
		*pos = 0;
	}
}

void  App_logWriteStr (CPU_CHAR  *p_fmt, ...)
{
	CPU_CHAR    str[MAX_LOG_STR + 1u];
	va_list     vArgs;

	va_start(vArgs, p_fmt);

	vsprintf((char       *)str,
			 (char const *)p_fmt,
						   vArgs);

	va_end(vArgs);

	INT8U err;
	log_semLock(&err);

	if (err != 0)
	{
		return;
	}

	strcpy(pLog_buff + (*pLog_write_pos) * MAX_LOG_STR, str);

	if (*pLog_num_records != MAX_LOG_RECORDS)
	{
		(*pLog_num_records) ++;
	}

	App_logCyclicAdvance(pLog_write_pos);

	// Check override
	if (*pLog_write_pos == *pLog_read_pos)
	{
		App_logCyclicAdvance(pLog_read_pos);
	}

	log_semRelease(&err);
}

void  App_logReadStr(CPU_CHAR   *p_str)
{
	INT8U err;
	log_semLock(&err);

	if (err != 0)
	{
		return;
	}

	if (*pLog_num_records == 0)
	{
		strcpy(p_str, " ");
		log_semRelease(&err);
		return;
	}

	// If string is too large, the log is corrupted, need to clean it
	if (strlen(pLog_buff + (*pLog_read_pos) * MAX_LOG_STR) > MAX_LOG_STR)
	{
		App_log_eraseAll();
		strcpy(p_str, " ");
		log_semRelease(&err);
		return;
	}

	strcpy(p_str, pLog_buff + (*pLog_read_pos) * MAX_LOG_STR);

	App_logCyclicAdvance(pLog_read_pos);

	log_semRelease(&err);
}

CPU_INT32U  App_logGetNumLogsToRead()
{
	if (*pLog_write_pos < *pLog_read_pos)
	{
		return MAX_LOG_RECORDS + *pLog_write_pos - *pLog_read_pos;
	}
	else
	{
		return *pLog_write_pos - *pLog_read_pos;
	}
}

void		App_log_eraseAll()
{
	*pLog_read_pos 		= 0;
	*pLog_write_pos		= 0;
	*pLog_num_records 	= 0;

}

void		App_log_save()
{
	App_log_update_flash();
}
