
#ifndef  APP_LOG_H
#define  APP_LOG_H

#include  <cpu.h>
void 		App_log_init();
void  		App_logWriteStr (CPU_CHAR  *p_fmt, ...);
void  		App_logReadStr   (CPU_CHAR   *p_str);
void		App_log_eraseAll();
CPU_INT32U  App_logGetNumLogsToRead();
void		App_log_save();


#endif
