/*
 * app_wwdg.h
 *
 */

#ifndef APP_IWDG_H_
#define APP_IWDG_H_

void WWDG_Init(void);

void App_wwdg_thread(void *p_arg);


#endif /* APP_IWDG_H_ */
