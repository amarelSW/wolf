from __future__ import print_function
import select
import socket
import sys, traceback
import re
import os.path
import binascii

UDP_IP = "127.0.0.1"
UDP_TX_PORT = 6666
UDP_RX_PORT = 7777
MESSAGE_TIMEOUT_SEC = 2 

NVM_DEFAULT_FILE_NAME = '.\NVM_data.dat'

unit_ip_txt = ""


# ***********************************************************************************    
def ReadBlockSizeAndNumBlocks():
    print ("Sending num of nlocks request")
    message = "GetNumOfBlocks"

    print ("Waiting for device response")
    data_received = False
    retry_num = 0
    while not data_received:
        tx_sock.sendto(message, (unit_ip_txt, UDP_TX_PORT))
        ready = select.select([rx_sock], [], [], MESSAGE_TIMEOUT_SEC)
        if ready[0]:
            retry_num = 0
            data, addr = rx_sock.recvfrom(1024) # buffer size is 1024 bytes
            if "IP" in data:
                continue
            data_received = True
            data = data.replace(' ', '=')
            str_split = data.split('=')
            num_blocks = int(str_split[1])
            block_size = int(str_split[3])
            print ("Number of blocks", num_blocks, "Block size", block_size)
        else:
            retry_num += 1
            print (" Retrying... ", retry_num)
            if retry_num > 10:
                print ("No response from device!")
                return (0,0)

    num_blocks = 1
    return (block_size, num_blocks)

# ***********************************************************************************    


def ReadNVMBlocks():
    
	# Get num of blocks
    block_size, num_blocks = ReadBlockSizeAndNumBlocks()

    if block_size==0 or num_blocks==0:
        return
        
    if os.path.isfile(NVM_DEFAULT_FILE_NAME):
        os.remove(NVM_DEFAULT_FILE_NAME)
        
    for x in range(0, num_blocks):
        
        # Send request to read block data
        message = "GetBlockData %i" % x
        retry_num = 0
        block_received = False
        while retry_num < 10 and not block_received:
            print ("Reading block", x,"\r", end='')
            tx_sock.sendto(message, (unit_ip_txt, UDP_TX_PORT))
     
            ready = select.select([rx_sock], [], [], MESSAGE_TIMEOUT_SEC)
            if ready[0]:
                data, addr = rx_sock.recvfrom(block_size)
                if "IP" in data:
                    retry_num += 1
                    continue
                
                file = open(NVM_DEFAULT_FILE_NAME, 'ab')
                #print (":".join("{:02x}".format(ord(c)) for c in data))
                #print (''.join(format(x, '02x') for x in data))
                file.write(bytes(data))
                file.flush
                file.close
                block_received = True
            else:
                retry_num += 1
                print (" Retrying... ", retry_num)
                if retry_num > 10:
                    print ("No response from device!")
                    return
    print (" ")
    print ("NVM saved into ", NVM_DEFAULT_FILE_NAME)

# ***********************************************************************************    
def WriteNVMBlocks():
   
 	# Get num of blocks
    block_size, num_blocks = ReadBlockSizeAndNumBlocks()

    if block_size==0 or num_blocks==0:
        return

    nvm_file_name = ""
	# Get file name
    ans=raw_input("Enter NVM data file name: (" + NVM_DEFAULT_FILE_NAME + ")")
    if ans == "":
        nvm_file_name = NVM_DEFAULT_FILE_NAME
    else: 
        nvm_file_name = ans
    
    print ("NVM file name is " + nvm_file_name)

    if not os.path.isfile(NVM_DEFAULT_FILE_NAME):
        print ("File ", nvm_file_name, "not found")
        return
    
    file = open('.\NVM_data.dat', 'rb')
    block = file.read(block_size)
    block_num = 0
    tx_sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
                     
    while block and block_num < num_blocks:
        print  ("Writing block", block_num,"\r", end='')        
        message = bytearray("SetBlockData %i" % block_num)
        message[50:50+block_size-1] = block
        retry_num = 0
        block_written = False
        while not block_written:
            tx_sock.sendto(message, (unit_ip_txt, UDP_TX_PORT))
            #wait for success response
            ready = select.select([rx_sock], [], [], MESSAGE_TIMEOUT_SEC)
            if ready[0]:
                data, addr = rx_sock.recvfrom(block_size)
                if "IP" in data:
                    continue
                    
                data = data.replace(' ', '=', 10)              

                str_split = data.split('=')
                written_block_num = int(str_split[1].strip('\0'))
                if written_block_num == block_num and str_split[0] == "BlockWritten":
                    block_written = True  
                    #print (":".join("{:02x}".format(ord(c)) for c in block))
                    #print ("And the total message is:")
                    #print (":".join("{:02x}".format(ord(d)) for d in str(message)))
                    
                else:
                    retry_num+=1
                    if retry_num > 10: 
                        print ("Failed to send block", block_num)
                        return
                
        block = file.read(block_size)
        block_num+=1
                    
    
        


                    
# ***********************************************************************************    
                    
print ("""
    ************************************************* 
    *                                               *
    *               WIFI Test App                   *
    *                                               *
    *                                               *
    ************************************************* 
    """)
print (sys.version)
 
rx_sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
rx_sock.setblocking(0)
rx_sock.bind(("", UDP_RX_PORT))
	
tx_sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

 
ans=True

#wait for unit to wake up
unit_up = False
print ("""
If tested unit is not connected in 1 minute, please :
- Make sure OpenWrt wifi hotspot is on
- Make sure your PC is connected to it
- restart the tested unit
""")

while not unit_up:
   ready = select.select([rx_sock], [], [], 30*MESSAGE_TIMEOUT_SEC)
   if ready[0]:
       data, addr = rx_sock.recvfrom(1024) # buffer size is 1024 bytes
       print ("Unit is up! ", data)
       unit_ip_txt = data.replace("IP: ", "");
       unit_up = True
    	
while ans:
    print ("""
    1. Read NVM through WIFI
    2. Write NVM through WIFI
    3. option
    4. option
    99.  Exit/Quit
    """)
    ans=raw_input("Select test option: ") 
    if ans=="1": 
      print("\n Read NVM through WIFI") 
      ReadNVMBlocks()
    elif ans=="2":
      print("\n Write NVM through WIFI") 
      WriteNVMBlocks() 
    elif ans=="99":
      print("\n Adios!") 
      ans = False
    elif ans !="":
      print("\n Not implemented, Try again") 
	  
