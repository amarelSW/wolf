
#include "RS232.h"
#include <iostream>

using namespace RS232;



SerialConnection::~SerialConnection()
{
	//close any open ports
	for (unsigned i = 0; i < 16; i++)
	{
		if (m_comPortHandles[i] != INVALID_HANDLE_VALUE)
			CloseHandle(m_comPortHandles[i]);
	}
}

int SerialConnection::OpenPort(const unsigned short port, unsigned baud)
{
	char comPortStr[120];
	sprintf_s(comPortStr, "\\\\.\\COM%d", port);

	//create baudrate string
	switch (baud)
	{
	case 110:
		strcpy_s(m_baudRate, "baud=110 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 300:
		strcpy_s(m_baudRate, "baud=300 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 600:
		strcpy_s(m_baudRate, "baud=600 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 1200:
		strcpy_s(m_baudRate, "baud=1200 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 2400:
		strcpy_s(m_baudRate, "baud=2400 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 4800:
		strcpy_s(m_baudRate, "baud=4800 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 9600:
		strcpy_s(m_baudRate, "baud=9600 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 19200:
		strcpy_s(m_baudRate, "baud=19200 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 38400:
		strcpy_s(m_baudRate, "baud=38400 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 57600:
		strcpy_s(m_baudRate, "baud=57600 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 115200:
		strcpy_s(m_baudRate, "baud=115200 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 128000:
		strcpy_s(m_baudRate, "baud=128000 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 256000:
		strcpy_s(m_baudRate, "baud=256000 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 500000:
		strcpy_s(m_baudRate, "baud=500000 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	case 1000000:
		strcpy_s(m_baudRate, "baud=1000000 data=8 parity=N stop=1 dtr=on rts=on");
		break;
	default:
		std::cout << baud << ": Invalid baud rate" << std::endl;
		return 1;
	}

	//get a handle to requested COM port
	m_comPortHandles[port] = CreateFileA(
		comPortStr,
		GENERIC_READ | GENERIC_WRITE,
		0, //no share
		0, //no security
		OPEN_EXISTING,
		0, //no threads
		0); //no template

	//check if port was created successfully
	if (m_comPortHandles[port] == INVALID_HANDLE_VALUE)
	{
		std::cout << "Unable to open COM port " << port << std::endl;
		return 1;
	}

	//clear the settings struct and apply to port
	memset(&m_portSettings, 0, sizeof(m_portSettings));
	m_portSettings.DCBlength = sizeof(m_portSettings);

	if (!BuildCommDCBA(m_baudRate, &m_portSettings))
	{
		std::cout << "Unable to apply COM port DCB settings." << std::endl;
		CloseHandle(m_comPortHandles[port]);
		return 1;
	}

	if (!SetCommState(m_comPortHandles[port], &m_portSettings))
	{
		std::cout << "Unable to apply COM port cfg settings." << std::endl;
		CloseHandle(m_comPortHandles[port]);
		return 1;
	}

	//update timeout struct
	m_commTimeouts.ReadIntervalTimeout = MAXDWORD;
	m_commTimeouts.ReadTotalTimeoutConstant = 1;
	m_commTimeouts.ReadTotalTimeoutMultiplier = 0;
	m_commTimeouts.WriteTotalTimeoutConstant = 0;
	m_commTimeouts.WriteTotalTimeoutMultiplier = 0;

	//apply timeout settings
	if (!SetCommTimeouts(m_comPortHandles[port], &m_commTimeouts))
	{
		std::cout << "Unable to apply COM port timeout settings." << std::endl;
		CloseHandle(m_comPortHandles[port]);
		return 1;
	}
	//success!!
	return 0;
}

int SerialConnection::ReadByte(const unsigned short port, char& byte)
{
	if (m_comPortHandles[port] != INVALID_HANDLE_VALUE)
	{
		int readResult;
		ReadFile(m_comPortHandles[port], &byte, 1, (LPDWORD)((void*)&readResult), 0);
		if (readResult < 1) return -1;
		return 0;
	}
	return 1;
}

int SerialConnection::ReadExactNumOfBytes(const unsigned short port, char* buffer, unsigned short size)
{
	int readSize = 0;
	const int MAX_WAIT_MSEC = 3000;
	char byte;
	int numWaits = 0;
	while (true)
	{
		if (ReadByte(port, byte) == 0)
		{
			buffer[readSize++] = byte;
			if (readSize == size)
			{
				return size;
			}
		}
		else
		{
			if (numWaits > MAX_WAIT_MSEC)
			{
				return -1;
			}
			Sleep(1);
			numWaits++;
		}
	}


}

int SerialConnection::ReadByteArray(const unsigned short port, char* buffer, unsigned short size, char delimiter)
{
	int readSize = 0;
	const int MAX_WAIT_MSEC = 3000;
	if (delimiter != -1)
	{
		char byte;
		int numWaits = 0;
		while (true)
		{
			if (readSize == size)
			{
				return -1;
			}
			if (ReadByte(port, byte) == 0)
			{
				if (byte == delimiter)
				{
					return readSize;
				}

				buffer[readSize++] = byte;
			}
			else
			{
				if (numWaits > MAX_WAIT_MSEC)
				{
					return -1;
				}
				Sleep(1);
				numWaits++;
			}
		}
	}

	if (m_comPortHandles[port] != INVALID_HANDLE_VALUE)
	{
		//limit size to 4kb
		if (size > 4096) size = 4096;

		ReadFile(m_comPortHandles[port], buffer, size, (LPDWORD)((void*)&readSize), 0);

		return readSize;
	}
	return -1;
}

int SerialConnection::SendByte(const unsigned short port, char byte)
{
	if (m_comPortHandles[port] != INVALID_HANDLE_VALUE)
	{
		int sendResult;
		WriteFile(m_comPortHandles[port], &byte, 1, (LPDWORD)((void*)&sendResult), 0);
		if (sendResult < 0) return 1;
		return 0;
	}
	return 1;
}

int SerialConnection::SendByteArray(const unsigned short port, char* buffer, unsigned short size)
{
	if (m_comPortHandles[port] != INVALID_HANDLE_VALUE)
	{
		int sendResult;
		if (WriteFile(m_comPortHandles[port], buffer, size, (LPDWORD)((void*)&sendResult), 0))
		{
			return sendResult;
		}
		return -1;
	}
	return -1;
}

int SerialConnection::ClosePort(const unsigned short port)
{
	if (m_comPortHandles[port] != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_comPortHandles[port]);
		return 0;
	}
	return 1;
}

