
#ifndef RS232_H_
#define RS232_H_

#include <cstring>

#include <Windows.h>
#define WIN32_LEAN_AND_MEAN

		 //-------------Class Declaration----------------//
namespace RS232
{
	class SerialConnection
	{
	public:
		~SerialConnection(); //closes any ports on destruction

		//attempts to open port at given baud. Port numbers start at 0, returns 1 if error
		int OpenPort(const unsigned short port, unsigned baud);
		//attempts to read a single byte from port. returns 0 if successful
		//else returns 1 if failed or no bytes to read. received data is stored in byte. blocking
		int ReadByte(const unsigned short port, char& byte);
		//fills buffer with any received bytes up to size and returns number of bytes received
		//or -1 if port is invalid. blocking.
		int ReadByteArray(const unsigned short port, char* buffer, unsigned short size, char delimiter = -1);
		//attempts to send byte on port if port is open. returns 1 on failure
		int SendByte(const unsigned short port, char byte);
		//attempts to send an array of bytes up to size on port if port is open. returns -1 on error
		//else returns number of bytes sent. blocking.
		int SendByteArray(const unsigned short port, char* buffer, unsigned short size);
		//closes port if port is open
		int ClosePort(const unsigned short port);

		int ReadExactNumOfBytes(const unsigned short port, char* buffer, unsigned short size);


	private:
		//HANDLEs of available COM ports
		HANDLE m_comPortHandles[100];
		//struct to hold port settings when opening a new port
		DCB m_portSettings;
		//timeout struct
		COMMTIMEOUTS m_commTimeouts;

		static const char m_comPorts[16][10];
		char m_baudRate[64];
	};
};




#endif //RS232_H_
