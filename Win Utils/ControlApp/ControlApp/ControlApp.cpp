// ControlApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>
#include <string.h>
#include <vector>
#define STRICT
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include "RS232.h"

using namespace std;

typedef struct T_Filter
{
	WORD num;
	WORD type;
	WORD index;
	DWORD mask;
	DWORD value;
}  T_Filter;



const char g_mainMenu[] =
			{ "\n\r MAIN MENU"
			"\n\r 1. Set mode 1"
			"\n\r 2. Set mode 2"
			"\n\r 3. Set mode 3 HOST"
			"\n\r 4. Set mode 3 SWITCH"
			"\n\r 5. Set Filters"
			"\n\r 6. Get MOD2 Logs"
			"\n\r ctrl-backspace: EXIT" 
			"\n\r =========================="
			"\n\r Your choice: "};



const char FILTERS_FILE_NAME[] = "Filters.txt";

const unsigned baud = 9600u;
RS232::SerialConnection serConn;


void errorDeSystema(char *name) {
	// Recupera, formatea y despliega el mensaje del ultimo error
	// 'name' que es el argumento pasado al momento del error debe ser una     frase en presente
	//  como por ejemplo "abriendo archivo".
	//
	char ptr[1024];
	sprintf_s(ptr, 1024, "%s %d", name, GetLastError());

	wcout << endl << "Error " << name << ": " << ptr << endl;
	LocalFree(ptr);
}

bool getUserLine(char* line, size_t maxChars)
{
	char ch;
	// Leer solo una linea. Si 127 existe, terminar todo
	size_t numChars = 0;
	ch = 0;
	while (ch != '\r')
	{
		ch = _getch();
		if (ch == 127)
			return false;
		// Echo it
		wcout << ch;

		line[numChars++] = ch;
		if (numChars > maxChars)
		{
			numChars = 0;
		}
	}

	line[numChars] = 0;

	return true;
}


bool readFiltersFile(vector<T_Filter>& filters)
{
	const int NUM_LABELS = 5;
	const string desiredLabes[NUM_LABELS] = { "num", "type", "index", "mask", "value" };
	std::ifstream infile(FILTERS_FILE_NAME);
	std::string line;
	while (std::getline(infile, line))
	{
		std::istringstream iss(line);
		T_Filter filter;
		string labels[5];
		if (!(iss >> labels[0] >> filter.num >> labels[1] >> std::hex >> filter.type >> labels[2] >> std::dec >> filter.index >> labels[3] >> std::hex >> filter.mask >> labels[4] >> std::hex >> filter.value))
		{ 
			return false;
		}

		// Check that all labels are correct
		for (int i = 0; i < NUM_LABELS; i++)
		{
			if (labels[i] != desiredLabes[i])
			{
				return false;
			}
		}
		
		filters.push_back(filter);
	}

	return true;
}


int _tmain(int argc, char* argv[])
{
	int read;

	const char init[] = "?\r"; 
	const char devicePrompt[] = ">";
	char userInBuff[1024];
	char deviceInBuff[1024];

	unsigned short port = 23u;

	if (argc > 1)
	{
		port = atoi(argv[1]);
	}

	// abre el puerto.	
	if (serConn.OpenPort(port, baud) != 0)
	{
		errorDeSystema("Open port");
		return E_FAIL;
	} 

	wcout << "Connecting to device...";
	
	if (!serConn.SendByteArray(port, (char*)init, strlen(init)))
	{
		errorDeSystema("Writing to port");
	}

	//if (written != sizeof(init))
	//{
	//	errorDeSystema("Not all data sent to port");
	//}

	Sleep(10);
	read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');


	if ((read<0) || (!strstr(deviceInBuff, devicePrompt)))
	{
		wcout << "\r\nDevice not responsive ";
		goto EXIT;
	}
	deviceInBuff[read] = 0;

	// ciclo basico de la terminal:
	while (true) {
		// Mostrar al menu
		wcout << g_mainMenu;

		if (!getUserLine(userInBuff, sizeof(userInBuff))) 
		{
			goto EXIT;
		}

		if (strstr(userInBuff, "1") != nullptr)
		{
			serConn.SendByteArray(port, "M1\r", 3);
			wcout << "\n\r Wait a bit...\n\r";

			read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');
			deviceInBuff[read] = 0;
			wcout << deviceInBuff;
		}
		else if (strstr(userInBuff, "2") != nullptr)
		{
			serConn.SendByteArray(port, (char*)"M2\r", 3);
			wcout << "\n\r Wait a bit...\n\r";
			Sleep(3000);
			read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');
			if (read > 0)
			{
				deviceInBuff[read] = 0;
				wcout << deviceInBuff;
			}
		}
		else if (strstr(userInBuff, "3") != nullptr)
		{
			serConn.SendByteArray(port, "M3\r", 3);
			wcout << "\n\r Wait a bit...\n\r";
			Sleep(3000);
			read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');
			if (read > 0)
			{
				deviceInBuff[read] = 0;
				wcout << deviceInBuff;
			}
		}
		else if (strstr(userInBuff, "4") != nullptr)
		{
			serConn.SendByteArray(port, "M4\r", 3);
			wcout << "\n\r Wait a bit...\n\r";
			Sleep(3000);
			read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');
			if (read > 0)
			{
				deviceInBuff[read] = 0;
				wcout << deviceInBuff;
			}
		}
		else if (strstr(userInBuff, "5") != nullptr)
		{
			// read filters
			vector<T_Filter> filters;
			if (!readFiltersFile(filters))
			{
				wcout << "Error in " << FILTERS_FILE_NAME << " file!";
				goto EXIT;
			}

			// Config device 
			// Reset all filters
			serConn.SendByteArray(port, "CLR\r", 4);
			read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');

			for (size_t i = 0; i < filters.size(); i++)
			{
				serConn.SendByteArray(port, "FLTR\r", 5);
				read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');
				char buff[50];
				sprintf_s(buff, 50, "0x%x\r", filters[i].type);
				serConn.SendByteArray(port, buff, strlen(buff));
				read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');
				sprintf_s(buff, 50, "%d\r", filters[i].index);
				serConn.SendByteArray(port, buff, strlen(buff));
				read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');
				sprintf_s(buff, 50, "0x%x\r", filters[i].mask);
				serConn.SendByteArray(port, buff, strlen(buff));
				read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');
				sprintf_s(buff, 50, "0x%x\r", filters[i].value);
				serConn.SendByteArray(port, buff, strlen(buff));
				read = serConn.ReadByteArray(port, (char*)deviceInBuff, sizeof(deviceInBuff), '>');
				if (read > 0)
				{
					deviceInBuff[read] = 0;
				}
				char* response = strstr(deviceInBuff, "Filter added");
				if (!response)
				{
					wcout << "Error, filter not added " ;
					goto EXIT;
				}
				else
				{
					wcout << response;
				}
			}
		}
		else if (strstr(userInBuff, "6") != nullptr)
		{
			serConn.SendByteArray(port, "LOGGET\r", 7);
			read = serConn.ReadExactNumOfBytes(port, (char*)deviceInBuff, 8 ); // First bytes are echo + \n
			while (true)
			{
				read = serConn.ReadExactNumOfBytes(port, (char*)deviceInBuff, 2);
				if (read != 0)
				{
					size_t numBytes = (unsigned char)(deviceInBuff[0]) + ((unsigned char)(deviceInBuff[1]) << 8);
					if (numBytes != 0)
					{
						read = serConn.ReadExactNumOfBytes(port, (char*)deviceInBuff, numBytes);
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
			}
		}
		else
		{
			wcout << "ignored..\r\n";
		}
	}

EXIT:
	wcout << "\r\n Hit any key to exit";
	_getch();

	// cierra todo y bye bye.
	serConn.ClosePort(port);

	return 0;
}

