#pragma once

namespace Sub_Tester {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Windows::Forms;

	/// <summary>
	/// Summary for EnterNumBlocks
	/// </summary>
	public ref class EnterNumBlocks : public System::Windows::Forms::Form
	{
	private:
		int m_initialNum;
	public:
		EnterNumBlocks(int initialNum)
		{
			m_initialNum = initialNum;

			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~EnterNumBlocks()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected:
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Button^  button1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(13, 29);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(152, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Enter number of blocks to read";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(18, 64);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(133, 20);
			this->textBox1->TabIndex = 1;
			this->textBox1->Text = m_initialNum.ToString();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(76, 106);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 2;
			this->button1->Text = L"Enter";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &EnterNumBlocks::Button1_Click);
			// 
			// EnterNumBlocks
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(184, 200);
			this->ControlBox = false;
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"EnterNumBlocks";
			this->Text = L"Enter Num Blocks";
			this->Load += gcnew System::EventHandler(this, &EnterNumBlocks::EnterNumBlocks_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void EnterNumBlocks_Load(System::Object^  sender, System::EventArgs^  e) 
	{
	}

	public:
		int getUserInput()
		{
			return Convert::ToInt32(this->textBox1->Text);
		}
		void GetParentText()
		{
			String^ x = (safe_cast<EnterNumBlocks^>(this->ParentForm))->Text;
		}
private: System::Void Button1_Click(System::Object^  sender, System::EventArgs^  e)
		{
			this->DialogResult = System::Windows::Forms::DialogResult::Yes;
		}
	};



}
