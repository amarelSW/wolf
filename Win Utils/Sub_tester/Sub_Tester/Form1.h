#pragma once

#include <msclr\marshal_cppstd.h>
#include <string>
#include "EnterNumBlocks.h"
#using<system.dll>  


#define IP_IN_PORT			8888
#define IN_PORT				7777
#define OUT_PORT			6666
#define MAX_RETRIES			3
#define NVM_DATA_FILE_NAME	"NVM_DATA.dat"
#define SUB_MSG_HEADER_LEN	5
#define UDP_RECEIVE_TIMEOUT	5000
#define NUM_BYTES_TO_READ	4096
#define LOG_FILE_NAME		".//Sub_Test_Log.txt"


#define _htons(A)   	(short)((((short) (A)) << 8) | (((short) (A)) >> 8))
/*!<
Convert a 2-byte integer (short) from the host representation to the Network byte order representation.
*/
namespace Subway_Tester 
{
	public enum class Sub_Msg_Msgtype : char 
	{
		READ_NVM_BLOCK		= 0,
		WRITE_NVM_BLOCK		= 1,
		READ_BLOCK_SIZE		= 2,
		READ_NUM_BLOCKS		= 3,
		GET_DEVICE_IP		= 4,
		HALT_DEVICE			= 5,
		TEST_PACKET			= 6,
		GET_WINC_REV		= 7,
		GET_LOGS			= 8,
		START_OTA			= 9,
		GET_IN_OUT_COUNTERS = 10,
		ERASE_LOGS			= 11,
		WRITE_LOG_RECORD	= 12,
		SAVE_LOG_TO_FLASH	= 13,
		START_AP			= 14,
		RESET_DEVICE		= 15,
	};

#pragma pack(push, 1)
	typedef struct Sub_Msg
	{
		char	msgType;
		short	data;
		short	buffLen;
		char	buff[1024];
	} Sub_Msg;
#pragma pack(pop)
	
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;
	using namespace System::Net;
	using namespace System::Net::Sockets;
	using namespace System::Text;
	using namespace System::Threading;
	using namespace System::IO;
	using namespace System::Diagnostics;
	using namespace System::Globalization;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	private: IPEndPoint^ m_sendingAddress = gcnew IPEndPoint(IPAddress::Broadcast, OUT_PORT);
	private: bool m_commThreadActive = false;
	private: Thread^ commTestThrd;
	private: System::Windows::Forms::Button^  writeNVMButton;
	private: System::Windows::Forms::Button^  haltDeviceButton;
	private: System::Windows::Forms::Button^  communicationTestbutton;
	private: System::Windows::Forms::Button^  getWincVerButton;
	private: System::Windows::Forms::Button^  dumpLogsButton;
	private: System::Windows::Forms::Button^  startOTAButton;
	private: System::Windows::Forms::Button^  inOutCountersButton;
	private: System::Windows::Forms::Button^  writeLogRecordButton;
	private: System::Windows::Forms::Button^  eraseLogsButton;
	private: System::Windows::Forms::Button^  saveLogToFlashButton;
	private: System::Windows::Forms::Button^  startAPButton;

	private: System::Windows::Forms::Button^  readTestBbutton;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::Button^  resetDeviceButton;


			 IPEndPoint^ m_receivingAddress = gcnew IPEndPoint(IPAddress::Any, IN_PORT);

	public:
		Form1(void)
		{
			InitializeComponent();

			// If logfile exixts, rename it to its date
			if (File::Exists(LOG_FILE_NAME))
			{
				auto ftime = File::GetCreationTime(LOG_FILE_NAME).ToString();

				String^ newFileName = Path::GetDirectoryName(LOG_FILE_NAME) + "//" + Path::GetFileNameWithoutExtension(LOG_FILE_NAME) + " " + ftime->Replace('/', '.')->Replace(':', '-') + Path::GetExtension(LOG_FILE_NAME);
				File::Move(LOG_FILE_NAME, newFileName);
			}
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
			if (commTestThrd->IsAlive)
				commTestThrd->Abort();
		}



	protected: 


	private: System::Windows::Forms::Panel^  panel1;



	private: System::Windows::Forms::Label^  separatorLabel1;

	private: System::Windows::Forms::Button^  initDeviceButton;
	private: System::Windows::Forms::Button^  initDACButton;
	//private: System::Windows::Forms::Label^  label17;


	private: System::Windows::Forms::TextBox^  statusTextBox;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->readTestBbutton = (gcnew System::Windows::Forms::Button());
			this->startAPButton = (gcnew System::Windows::Forms::Button());
			this->saveLogToFlashButton = (gcnew System::Windows::Forms::Button());
			this->writeLogRecordButton = (gcnew System::Windows::Forms::Button());
			this->eraseLogsButton = (gcnew System::Windows::Forms::Button());
			this->inOutCountersButton = (gcnew System::Windows::Forms::Button());
			this->startOTAButton = (gcnew System::Windows::Forms::Button());
			this->dumpLogsButton = (gcnew System::Windows::Forms::Button());
			this->getWincVerButton = (gcnew System::Windows::Forms::Button());
			this->communicationTestbutton = (gcnew System::Windows::Forms::Button());
			this->haltDeviceButton = (gcnew System::Windows::Forms::Button());
			this->writeNVMButton = (gcnew System::Windows::Forms::Button());
			this->initDACButton = (gcnew System::Windows::Forms::Button());
			this->separatorLabel1 = (gcnew System::Windows::Forms::Label());
			this->initDeviceButton = (gcnew System::Windows::Forms::Button());
			this->statusTextBox = (gcnew System::Windows::Forms::TextBox());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->resetDeviceButton = (gcnew System::Windows::Forms::Button());
			this->panel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->panel1->Controls->Add(this->resetDeviceButton);
			this->panel1->Controls->Add(this->readTestBbutton);
			this->panel1->Controls->Add(this->startAPButton);
			this->panel1->Controls->Add(this->saveLogToFlashButton);
			this->panel1->Controls->Add(this->writeLogRecordButton);
			this->panel1->Controls->Add(this->eraseLogsButton);
			this->panel1->Controls->Add(this->inOutCountersButton);
			this->panel1->Controls->Add(this->startOTAButton);
			this->panel1->Controls->Add(this->dumpLogsButton);
			this->panel1->Controls->Add(this->getWincVerButton);
			this->panel1->Controls->Add(this->communicationTestbutton);
			this->panel1->Controls->Add(this->haltDeviceButton);
			this->panel1->Controls->Add(this->writeNVMButton);
			this->panel1->Controls->Add(this->initDACButton);
			this->panel1->Controls->Add(this->separatorLabel1);
			this->panel1->Controls->Add(this->initDeviceButton);
			this->panel1->Location = System::Drawing::Point(5, 5);
			this->panel1->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(612, 506);
			this->panel1->TabIndex = 6;
			// 
			// readTestBbutton
			// 
			this->readTestBbutton->Location = System::Drawing::Point(23, 417);
			this->readTestBbutton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->readTestBbutton->Name = L"readTestBbutton";
			this->readTestBbutton->Size = System::Drawing::Size(213, 28);
			this->readTestBbutton->TabIndex = 30;
			this->readTestBbutton->Text = L"Read File Timing";
			this->readTestBbutton->UseVisualStyleBackColor = true;
			this->readTestBbutton->Click += gcnew System::EventHandler(this, &Form1::readTestBbutton_Click);
			// 
			// startAPButton
			// 
			this->startAPButton->Location = System::Drawing::Point(23, 372);
			this->startAPButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->startAPButton->Name = L"startAPButton";
			this->startAPButton->Size = System::Drawing::Size(213, 28);
			this->startAPButton->TabIndex = 29;
			this->startAPButton->Text = L"Start AP Mode";
			this->startAPButton->UseVisualStyleBackColor = true;
			this->startAPButton->Click += gcnew System::EventHandler(this, &Form1::startAPButton_Click);
			// 
			// saveLogToFlashButton
			// 
			this->saveLogToFlashButton->Location = System::Drawing::Point(301, 442);
			this->saveLogToFlashButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->saveLogToFlashButton->Name = L"saveLogToFlashButton";
			this->saveLogToFlashButton->Size = System::Drawing::Size(213, 28);
			this->saveLogToFlashButton->TabIndex = 28;
			this->saveLogToFlashButton->Text = L"Save Log To Flash";
			this->saveLogToFlashButton->UseVisualStyleBackColor = true;
			this->saveLogToFlashButton->Click += gcnew System::EventHandler(this, &Form1::saveLogToFlashButton_Click);
			// 
			// writeLogRecordButton
			// 
			this->writeLogRecordButton->Location = System::Drawing::Point(301, 393);
			this->writeLogRecordButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->writeLogRecordButton->Name = L"writeLogRecordButton";
			this->writeLogRecordButton->Size = System::Drawing::Size(213, 28);
			this->writeLogRecordButton->TabIndex = 27;
			this->writeLogRecordButton->Text = L"Add Log Record";
			this->writeLogRecordButton->UseVisualStyleBackColor = true;
			this->writeLogRecordButton->Click += gcnew System::EventHandler(this, &Form1::writeLogRecordButton_Click);
			// 
			// eraseLogsButton
			// 
			this->eraseLogsButton->Location = System::Drawing::Point(301, 343);
			this->eraseLogsButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->eraseLogsButton->Name = L"eraseLogsButton";
			this->eraseLogsButton->Size = System::Drawing::Size(213, 28);
			this->eraseLogsButton->TabIndex = 26;
			this->eraseLogsButton->Text = L"Erase Logs ";
			this->eraseLogsButton->UseVisualStyleBackColor = true;
			this->eraseLogsButton->Click += gcnew System::EventHandler(this, &Form1::eraseLogsButton_Click);
			// 
			// inOutCountersButton
			// 
			this->inOutCountersButton->Location = System::Drawing::Point(23, 318);
			this->inOutCountersButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->inOutCountersButton->Name = L"inOutCountersButton";
			this->inOutCountersButton->Size = System::Drawing::Size(213, 28);
			this->inOutCountersButton->TabIndex = 25;
			this->inOutCountersButton->Text = L"Get In/Out counters ";
			this->inOutCountersButton->UseVisualStyleBackColor = true;
			this->inOutCountersButton->Click += gcnew System::EventHandler(this, &Form1::inOutCountersButton_Click);
			// 
			// startOTAButton
			// 
			this->startOTAButton->Location = System::Drawing::Point(23, 268);
			this->startOTAButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->startOTAButton->Name = L"startOTAButton";
			this->startOTAButton->Size = System::Drawing::Size(213, 28);
			this->startOTAButton->TabIndex = 15;
			this->startOTAButton->Text = L"Start OTA";
			this->startOTAButton->UseVisualStyleBackColor = true;
			this->startOTAButton->Click += gcnew System::EventHandler(this, &Form1::startOTAButton_Click);
			// 
			// dumpLogsButton
			// 
			this->dumpLogsButton->Location = System::Drawing::Point(301, 297);
			this->dumpLogsButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->dumpLogsButton->Name = L"dumpLogsButton";
			this->dumpLogsButton->Size = System::Drawing::Size(213, 28);
			this->dumpLogsButton->TabIndex = 14;
			this->dumpLogsButton->Text = L"Dump Logs";
			this->dumpLogsButton->UseVisualStyleBackColor = true;
			this->dumpLogsButton->Click += gcnew System::EventHandler(this, &Form1::dumpLogsButton_Click);
			// 
			// getWincVerButton
			// 
			this->getWincVerButton->Location = System::Drawing::Point(23, 222);
			this->getWincVerButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->getWincVerButton->Name = L"getWincVerButton";
			this->getWincVerButton->Size = System::Drawing::Size(213, 28);
			this->getWincVerButton->TabIndex = 13;
			this->getWincVerButton->Text = L"Get Wifi Device Ver";
			this->getWincVerButton->UseVisualStyleBackColor = true;
			this->getWincVerButton->Click += gcnew System::EventHandler(this, &Form1::getWincVerButton_Click);
			// 
			// communicationTestbutton
			// 
			this->communicationTestbutton->Location = System::Drawing::Point(23, 172);
			this->communicationTestbutton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->communicationTestbutton->Name = L"communicationTestbutton";
			this->communicationTestbutton->Size = System::Drawing::Size(213, 28);
			this->communicationTestbutton->TabIndex = 12;
			this->communicationTestbutton->Text = L"Start Communication Test ";
			this->communicationTestbutton->UseVisualStyleBackColor = true;
			this->communicationTestbutton->Click += gcnew System::EventHandler(this, &Form1::communicatinTestbutton_Click);
			// 
			// haltDeviceButton
			// 
			this->haltDeviceButton->Location = System::Drawing::Point(306, 76);
			this->haltDeviceButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->haltDeviceButton->Name = L"haltDeviceButton";
			this->haltDeviceButton->Size = System::Drawing::Size(208, 28);
			this->haltDeviceButton->TabIndex = 11;
			this->haltDeviceButton->Text = L"Halt Device";
			this->haltDeviceButton->UseVisualStyleBackColor = true;
			this->haltDeviceButton->Click += gcnew System::EventHandler(this, &Form1::haltDeviceButton_Click);
			// 
			// writeNVMButton
			// 
			this->writeNVMButton->Location = System::Drawing::Point(23, 123);
			this->writeNVMButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->writeNVMButton->Name = L"writeNVMButton";
			this->writeNVMButton->Size = System::Drawing::Size(213, 28);
			this->writeNVMButton->TabIndex = 10;
			this->writeNVMButton->Text = L"Write NVM Through WIFI";
			this->writeNVMButton->UseVisualStyleBackColor = true;
			this->writeNVMButton->Click += gcnew System::EventHandler(this, &Form1::writeNVMButton_Click);
			// 
			// initDACButton
			// 
			this->initDACButton->Location = System::Drawing::Point(23, 9);
			this->initDACButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->initDACButton->Name = L"initDACButton";
			this->initDACButton->Size = System::Drawing::Size(148, 28);
			this->initDACButton->TabIndex = 9;
			this->initDACButton->Text = L"Connect Device";
			this->initDACButton->UseVisualStyleBackColor = true;
			this->initDACButton->Click += gcnew System::EventHandler(this, &Form1::ConnectDeviceButton_Click);
			// 
			// separatorLabel1
			// 
			this->separatorLabel1->BackColor = System::Drawing::SystemColors::Control;
			this->separatorLabel1->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->separatorLabel1->Location = System::Drawing::Point(23, 60);
			this->separatorLabel1->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->separatorLabel1->Name = L"separatorLabel1";
			this->separatorLabel1->Size = System::Drawing::Size(385, 1);
			this->separatorLabel1->TabIndex = 7;
			// 
			// initDeviceButton
			// 
			this->initDeviceButton->Location = System::Drawing::Point(23, 76);
			this->initDeviceButton->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->initDeviceButton->Name = L"initDeviceButton";
			this->initDeviceButton->Size = System::Drawing::Size(213, 28);
			this->initDeviceButton->TabIndex = 6;
			this->initDeviceButton->Text = L"Read NVM Through WIFI";
			this->initDeviceButton->UseVisualStyleBackColor = true;
			this->initDeviceButton->Click += gcnew System::EventHandler(this, &Form1::readNVMThroughWIFIButton_Click);
			// 
			// statusTextBox
			// 
			this->statusTextBox->Location = System::Drawing::Point(5, 549);
			this->statusTextBox->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->statusTextBox->Multiline = true;
			this->statusTextBox->Name = L"statusTextBox";
			this->statusTextBox->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->statusTextBox->Size = System::Drawing::Size(612, 245);
			this->statusTextBox->TabIndex = 24;
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// resetDeviceButton
			// 
			this->resetDeviceButton->Location = System::Drawing::Point(306, 123);
			this->resetDeviceButton->Margin = System::Windows::Forms::Padding(4);
			this->resetDeviceButton->Name = L"resetDeviceButton";
			this->resetDeviceButton->Size = System::Drawing::Size(208, 28);
			this->resetDeviceButton->TabIndex = 31;
			this->resetDeviceButton->Text = L"Reset Device";
			this->resetDeviceButton->UseVisualStyleBackColor = true;
			this->resetDeviceButton->Click += gcnew System::EventHandler(this, &Form1::resetDeviceButton_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(653, 866);
			this->Controls->Add(this->statusTextBox);
			this->Controls->Add(this->panel1);
			this->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->Name = L"Form1";
			this->Text = L"Subway Tester";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->panel1->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) 
	{
	}

private: System::Boolean readMessageFromdevice(UdpClient^ udpClient, Sub_Msg* subMsg, Sub_Msg_Msgtype expectedMsgCode, bool isDesiredData, short desiredData)
{

	bool msgReceived = false;
	auto stopWatch = Stopwatch::StartNew();
	auto timeOut = gcnew TimeSpan(0, 0, 0, 0, 10000);
//	updateStatusLine("Reading " + expectedMsgCode.ToString());
	try
	{
		while ((!msgReceived) && (stopWatch->Elapsed.CompareTo(timeOut) < 0 ))
		{
			if (udpClient->Available >= SUB_MSG_HEADER_LEN)
			{
//				updateStatusLine("udpClient->Available " + udpClient->Available.ToString());
				auto data = udpClient->Receive(m_receivingAddress);
				//updateStatusLine("Received message  " +
				//	data[0].ToString() + " " +
				//	data[1].ToString() + " " +
				//	data[2].ToString() + " " +
				//	data[3].ToString() + " " +
				//	data[4].ToString());



				//if ((data[0] != 'I') && (data[1] != 'P'))
				//{
					// Copy the array to unmanaged memory.
					System::Runtime::InteropServices::Marshal::Copy(data, 0, IntPtr(subMsg), data->Length);
					bool desiredDatareceived = !isDesiredData || (desiredData == subMsg->data);
					if ((subMsg->msgType == (char)expectedMsgCode) && (desiredDatareceived))
					{
						//updateStatusLine("Expected message arrived");
						msgReceived = true;
					}
					else
					{
						updateStatusLine("Not Expected message, code is " + ((Sub_Msg_Msgtype)(subMsg->msgType)).ToString() + " instd of" + expectedMsgCode.ToString());
					}
				//}
				//else
				//{
				//	updateStatusLine("Message is IP message" );
				//}
			}
		}
	}
	catch (SocketException^ e)
	{
		if (e->SocketErrorCode == SocketError::TimedOut)
		{
			updateStatusLine("Device not responding");
		}
		else
		{
			updateStatusLine("ReceiveFrom failed" + e->ToString());
		}
		return false;
	}

	if (stopWatch->Elapsed.CompareTo(timeOut) >= 0)
	{
		updateStatusLine("Device did not respond within time limit");
	}
	if (!msgReceived)
	{
		updateStatusLine("msgReceived = false");
	}

	return msgReceived;
}

private: System::Boolean sendMessageToDevice(UdpClient^ udpClient, Sub_Msg* subMsg)
{
	

	int msgLen = SUB_MSG_HEADER_LEN + subMsg->buffLen;
	array<Byte>^ msg = gcnew array<Byte>(msgLen);

	try
	{
		// Copy the message to managed memory.
		System::Runtime::InteropServices::Marshal::Copy(IntPtr(subMsg), msg, 0, msg->Length);
	}
	catch (Exception^ e)
	{
		updateStatusLine("Marshal::Copy failed" + e->ToString());
		return false;
	}

	//---------------------------------------------
	// Send a datagram to the device
	//updateStatusLine("Sending message to device " + 
	//	((Sub_Msg_Msgtype)(subMsg->msgType)).ToString() + " " +
	//	msg[0].ToString() + " " + 
	//	msg[1].ToString() + " " +
	//	msg[2].ToString() + " " +
	//	msg[3].ToString() + " " +
	//	msg[4].ToString() );


	try
	{
		udpClient->Send(msg, msg->Length, m_sendingAddress);
	}
	catch (Exception^ e)
	{
		updateStatusLine("Send failed" + e->ToString());
		return false;
	}

	return true;
}

private: System::Boolean getDataFromDevice( Sub_Msg* subMsgOut, Sub_Msg* subMsgIn, bool isDesiredData, short desiredData)
{
	

	int numRetries = 0;
	bool ret1, ret2;
	while (numRetries < MAX_RETRIES)
	{
		UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
		udpTester->EnableBroadcast = true;
		udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;

		ret1 = sendMessageToDevice(udpTester, subMsgOut);
		ret2 = readMessageFromdevice(udpTester, subMsgIn, (Sub_Msg_Msgtype)(subMsgOut->msgType), isDesiredData, desiredData);
//		updateStatusLine("udpTester->Close()");
		udpTester->Close();

		if (ret1 && ret2)
		{
			break;
		}
		else
		{
			numRetries++;
			updateStatusLine("Retrying");
		}
	}

	if (!ret1)
	{
		updateStatusLine("sendMessageToDevice failed");
	}
	else if (!ret2)
	{
		updateStatusLine("readMessageFromdevice failed");
	}

	if (numRetries >= MAX_RETRIES)
	{
		updateStatusLine("numRetries >= MAX_RETRIES");
	}
	else
	{
		updateStatusLine("Operation Succeeded");
	}

	
	return (numRetries < MAX_RETRIES);
}

private: System::Void readNVMThroughWIFIButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Sub_Msg subMsgOut;
	Sub_Msg subMsgIn;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::READ_BLOCK_SIZE;
	subMsgOut.buffLen = 0;

	if (getDataFromDevice(&subMsgOut, &subMsgIn, false, 0))
	{
		updateStatusLine("Block Size = " + subMsgIn.data.ToString());
	}
	else
	{
		return;
	}

	size_t blockSize = subMsgIn.data;

	subMsgOut.msgType = (char)Sub_Msg_Msgtype::READ_NUM_BLOCKS;
	if (getDataFromDevice(&subMsgOut, &subMsgIn, false, 0))
	{
		updateStatusLine("Num blocks = " + subMsgIn.data.ToString());
	}
	else
	{
		return;
	}

	size_t numBlocks = subMsgIn.data;

	Sub_Tester::EnterNumBlocks^ numBlocksDialog = gcnew Sub_Tester::EnterNumBlocks((int)numBlocks);
	numBlocksDialog->ShowDialog();

	numBlocks = numBlocksDialog->getUserInput();

	subMsgOut.msgType = (char)Sub_Msg_Msgtype::READ_NVM_BLOCK;
	
	if (File::Exists(NVM_DATA_FILE_NAME))
	{
		File::Delete(NVM_DATA_FILE_NAME);
	}

	FileStream^ fs = gcnew FileStream(NVM_DATA_FILE_NAME, FileMode::Create);
	BinaryWriter^ w = gcnew BinaryWriter(fs);

	for (int i = 0; i < numBlocks; i++)
	{
		subMsgOut.data = i;
		if ((getDataFromDevice(&subMsgOut, &subMsgIn, true, i)) && (subMsgIn.buffLen == blockSize))
		{
			array<unsigned char>^ block = gcnew array<unsigned char>((int)blockSize);
			// Copy the message to managed memory.
			System::Runtime::InteropServices::Marshal::Copy(IntPtr(&(subMsgIn.buff[0])), block, 0, (int)blockSize);
			updateStatusLine("Reading block num " + i.ToString());
			w->Write(block);
			w->Flush();
		}
	}
	fs->Close();

	updateStatusLine("NVM Blocks were written to " + NVM_DATA_FILE_NAME);
}

private: System::Void writeNVMThroughWIFI(String^ fileName)
{
	Sub_Msg subMsgOut;
	Sub_Msg subMsgIn;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::READ_BLOCK_SIZE;
	subMsgOut.buffLen = 0;


	
	if (getDataFromDevice(&subMsgOut, &subMsgIn, false, 0))
	{
		updateStatusLine("Block Size = " + subMsgIn.data.ToString());
	}
	else
	{
		return;
	}

	size_t blockSize = subMsgIn.data;

	subMsgOut.msgType = (char)Sub_Msg_Msgtype::READ_NUM_BLOCKS;
	if (getDataFromDevice(&subMsgOut, &subMsgIn, false, 0))
	{
		updateStatusLine("Num blocks = " + subMsgIn.data.ToString());
	}
	else
	{
		return;
	}

	size_t numBlocks = subMsgIn.data;
//	numBlocks = 200;

	BinaryReader^ reader = gcnew BinaryReader(File::Open(fileName, FileMode::Open));

	int blockNum = 0;
	bool blockWritten = false;
	while (true)
	{
		auto data = reader->ReadBytes((int)blockSize);
		if (data->Length != blockSize)
		{
			break;
		}
		subMsgOut.msgType = (char)Sub_Msg_Msgtype::WRITE_NVM_BLOCK;

		subMsgOut.data = blockNum;
		subMsgOut.buffLen = (short)blockSize;

		// Copy the message from managed memory.
		System::Runtime::InteropServices::Marshal::Copy(data, 0, IntPtr(&(subMsgOut.buff[0])), (int)blockSize);
		// Returned message should contain the block num that was written
		int numRetries = 0;
		while (numRetries < MAX_RETRIES)
		{
			updateStatusLine("writing block " + blockNum.ToString());
			subMsgOut.data = blockNum;
			subMsgOut.buffLen = (short)blockSize;
			blockWritten = getDataFromDevice(&subMsgOut, &subMsgIn, true, blockNum);
			if (blockWritten)
			{
				break;
			}
			numRetries++;
		}
		if (!blockWritten)
		{
			break;
		}

		blockNum++;
		if (blockNum >= numBlocks)
		{
			break;
		}

	} 

	reader->Close();

	if (blockWritten)
	{
		updateStatusLine("NVM Blocks were written to device from " + fileName);
	}
	else
	{
		updateStatusLine("Write error on last NVM block");
	}
}

private: System::Void ConnectDeviceButton_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 // Send request to read device's ip address
	Sub_Msg subMsg;
	subMsg.msgType = (char)Sub_Msg_Msgtype::GET_DEVICE_IP;
	subMsg.buffLen = 0;

	int msgLen = SUB_MSG_HEADER_LEN + subMsg.buffLen;
	array<Byte>^ msg = gcnew array<Byte>(msgLen);
	try
	{
		// Copy the message to managed memory.
		System::Runtime::InteropServices::Marshal::Copy(IntPtr(&subMsg), msg, 0, msg->Length);
	}
	catch (Exception^ e)
	{
		updateStatusLine("Marshal::Copy failed" + e->ToString());
	}

	UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
	udpTester->EnableBroadcast = true;
	udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;
//	m_sendingAddress = (gcnew IPEndPoint(deviceAddress->Address, OUT_PORT));
	udpTester->Send(msg, msg->Length, m_sendingAddress);

	int numRetries = 0;
	int rc = 0;

	bool deviceResponded = false;
	//Send command and wait for device to respond
	while (numRetries < MAX_RETRIES)
	{
		//---------------------------------------------	
		// Wait for the device to send its IP address (start of test)
		try
		{
			auto data = udpTester->Receive(m_receivingAddress);
			// Copy the array to unmanaged memory.
			System::Runtime::InteropServices::Marshal::Copy(data, 0, IntPtr(&subMsg), data->Length);

			// display the received address
			updateStatusLine("Device connected " + gcnew String(subMsg.buff)); // System::Text::Encoding::UTF8->GetString(data, 0, data->Length));
			deviceResponded = true;
			m_sendingAddress->Address = m_receivingAddress->Address;
		}
		catch (SocketException^ e)
		{
			if (e->SocketErrorCode == SocketError::TimedOut)
			{
				updateStatusLine("Device not responding");
			}
			else
			{
				updateStatusLine("Receive failed" + e->ToString());
			}
			deviceResponded = false;
		}


		// Check received content
		if (!deviceResponded )
			numRetries++;
		else
			break;

		updateStatusLine("Retry GET_DEVICE_IP message");
	}

	if (numRetries == MAX_RETRIES)
	{
		updateStatusLine("MAX_RETRIES reached");
	}

	udpTester->Close();

}

		 private: void updateStatus(String^ msg) 
		 {
			 static int txtNum = 0;
			 txtNum++;
			 statusTextBox->AppendText(txtNum.ToString() + ". " + msg + "\r\n");
			 statusTextBox->Refresh();
		 }

		private: void updateStatusLine(String^ msg)
		{
			Invoke(gcnew Action<String^>(this, &Form1::updateStatus), msg);
			String^ path = LOG_FILE_NAME;
			
			StreamWriter^ sw = File::AppendText(path);

			try
			{
				sw->WriteLine(msg);
			}
			finally
			{
				if (sw)
					delete (IDisposable^)sw;
			}
		}



private: System::Void writeNVMButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	// Select file to write
	OpenFileDialog^ openFileDialog1 = gcnew OpenFileDialog;

	openFileDialog1->InitialDirectory = ".";
	openFileDialog1->Filter = "dat files (*.dat)|*.dat";
	openFileDialog1->FilterIndex = 0;
	openFileDialog1->RestoreDirectory = true;

	if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		writeNVMThroughWIFI(openFileDialog1->FileName);
	}
}
	private: System::Void haltDeviceButton_Click(System::Object^  sender, System::EventArgs^  e)
	{
		Sub_Msg subMsgOut;
		subMsgOut.msgType = (char)Sub_Msg_Msgtype::HALT_DEVICE;
		subMsgOut.buffLen = 0;

		UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
		udpTester->EnableBroadcast = true;
		udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;

		boolean ret1 = sendMessageToDevice(udpTester, &subMsgOut);

		udpTester->Close();
	}





	private: System::Void communicatinTestbutton_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if (!m_commThreadActive)
		{
			// Start communication test thread
			this->communicationTestbutton->Text = L"Stop Communication Test ";
			ThreadStart^ myThreadDelegate = gcnew ThreadStart(this, &Form1::commThreadFunc);
			commTestThrd = gcnew Thread(myThreadDelegate);
			commTestThrd->IsBackground = true;
			commTestThrd->Start();
			m_commThreadActive = true;
		}
		else
		{
			this->communicationTestbutton->Text = L"Start Communication Test ";
//			commTestThrd->Abort();
			m_commThreadActive = false;
		}
	}
private: void commThreadFunc()
{
	short packetNum = 0;

	UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
	udpTester->EnableBroadcast = true;
	udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;

	while (m_commThreadActive)
	{
		if (packetNum < 32767)
		{
			packetNum++;
		}
		else
		{
			packetNum = 0;
		}
		Sub_Msg subMsgOut, subMsgIn;
		subMsgOut.msgType = (char)Sub_Msg_Msgtype::TEST_PACKET;
		subMsgOut.buffLen = 0;
		subMsgOut.data = packetNum;		

		boolean ret1 = sendMessageToDevice(udpTester, &subMsgOut);

		boolean ret2 = readMessageFromdevice(udpTester, &subMsgIn, Sub_Msg_Msgtype::TEST_PACKET, false, 0);

		if (ret2)
		{
			updateStatusLine("Packet miss " + subMsgIn.data.ToString() + "/1000, RSSI val: " + subMsgIn.buff[0].ToString());
		}

		
		Thread::Sleep(100);
	}

	udpTester->Close();
}

private: System::Void getWincVerButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Sub_Msg subMsgOut, subMsgIn;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::GET_WINC_REV;
	subMsgOut.buffLen = 0;


	boolean ret1 = getDataFromDevice(&subMsgOut, &subMsgIn, false, 0);
	if (ret1)
	{
		updateStatusLine(gcnew String(subMsgIn.buff));
	}

}
private: System::Void dumpLogsButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Sub_Msg subMsgOut, subMsgIn;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::GET_LOGS;
	subMsgOut.buffLen = 0;

	while (true)
	{
		boolean ret1 = getDataFromDevice(&subMsgOut, &subMsgIn, false, 0);
		if (subMsgIn.data == -1) // No more logs
		{
			updateStatusLine("********* END OF LOGS *************");
			break;
		}
		if (ret1)
		{
			updateStatusLine(gcnew String(subMsgIn.buff));
		}
	}
}
private: System::Void startOTAButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Sub_Msg subMsgOut;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::START_OTA;
	subMsgOut.buffLen = 0;

	UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
	udpTester->EnableBroadcast = true;
	udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;

	boolean ret1 = sendMessageToDevice(udpTester, &subMsgOut);

	udpTester->Close();
}

private: System::Void inOutCountersButton_Click(System::Object^  sender, System::EventArgs^  e) 
{

	Sub_Msg subMsgOut, subMsgIn;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::GET_IN_OUT_COUNTERS;
	subMsgOut.buffLen = 0;

	boolean ret1 = getDataFromDevice(&subMsgOut, &subMsgIn, false, 0);
	if (ret1)
	{
		updateStatusLine(gcnew String(subMsgIn.buff));
	}
}

private: System::Void eraseLogsButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Sub_Msg subMsgOut;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::ERASE_LOGS;
	subMsgOut.buffLen = 0;

	UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
	udpTester->EnableBroadcast = true;
	udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;

	boolean ret1 = sendMessageToDevice(udpTester, &subMsgOut);

	udpTester->Close();
}
private: System::Void writeLogRecordButton_Click(System::Object^  sender, System::EventArgs^  e)
{
	Sub_Msg subMsgOut;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::WRITE_LOG_RECORD;
	DateTime localDate = DateTime::Now;
	
	String^ logRecord = "Log Record Inserted " + localDate.ToString();
	subMsgOut.data = 0;
	subMsgOut.buffLen = 0;

	// Copy the array to unmanaged memory.
	for each (char c in logRecord->ToCharArray())
	{
		subMsgOut.buff[subMsgOut.buffLen++] = c;
	}

	subMsgOut.buff[subMsgOut.buffLen++] = 0; // Terminate the STR

	UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
	udpTester->EnableBroadcast = true;
	udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;

	boolean ret1 = sendMessageToDevice(udpTester, &subMsgOut);

	udpTester->Close();
}

private: System::Void saveLogToFlashButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Sub_Msg subMsgOut;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::SAVE_LOG_TO_FLASH;
	subMsgOut.data = 0;
	subMsgOut.buffLen = 0;

	UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
	udpTester->EnableBroadcast = true;
	udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;

	boolean ret1 = sendMessageToDevice(udpTester, &subMsgOut);

	udpTester->Close();
}


private: System::Void startAPButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Sub_Msg subMsgOut;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::START_AP;
	subMsgOut.data = 0;
	subMsgOut.buffLen = 0;

	UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
	udpTester->EnableBroadcast = true;
	udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;

	boolean ret1 = sendMessageToDevice(udpTester, &subMsgOut);

	udpTester->Close();
}
private: System::Void readTestBbutton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		FileOptions FileFlagNoBuffering = (FileOptions)0x20000000;

		Stopwatch ^ timer = Stopwatch::StartNew();
		FileStream^ fs = gcnew FileStream(openFileDialog1->FileName, FileMode::Open, FileAccess::Read, FileShare::None, NUM_BYTES_TO_READ, FileFlagNoBuffering);
		BinaryReader^ br = gcnew BinaryReader(fs);
		array<unsigned char>^ buffer = gcnew array<unsigned char>(br->BaseStream->Length + NUM_BYTES_TO_READ);

		
		while (br->BaseStream->Position < br->BaseStream->Length)
		{
			if (br->Read(buffer, br->BaseStream->Position, NUM_BYTES_TO_READ) != NUM_BYTES_TO_READ)
			{
				break;
			}
		}
		

		timer->Stop();
		Int64 ticks = timer->ElapsedMilliseconds;

		MessageBox::Show("Read time was " + ((double)ticks/1000.0).ToString() + " Seconds");

		fs->Close();
	}
}

private: System::Void resetDeviceButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Sub_Msg subMsgOut;
	subMsgOut.msgType = (char)Sub_Msg_Msgtype::RESET_DEVICE;
	subMsgOut.buffLen = 0;

	UdpClient^ udpTester = gcnew UdpClient((int)IN_PORT);
	udpTester->EnableBroadcast = true;
	udpTester->Client->ReceiveTimeout = UDP_RECEIVE_TIMEOUT;

	boolean ret1 = sendMessageToDevice(udpTester, &subMsgOut);

	udpTester->Close();
}
};
}

