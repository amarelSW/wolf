/*
*********************************************************************************************************
*                                            uC/USB-Device
*                                       The Embedded USB Stack
*
*                         (c) Copyright 2004-2014; Micrium, Inc.; Weston, FL
*
*                  All rights reserved.  Protected by international copyright laws.
*
*                  uC/USB-Device is provided in source form to registered licensees ONLY.  It is
*                  illegal to distribute this source code to any third party unless you receive
*                  written permission by an authorized Micrium representative.  Knowledge of
*                  the source code may NOT be used to develop a similar product.
*
*                  Please help us continue to provide the Embedded community with the finest
*                  software available.  Your honesty is greatly appreciated.
*
*                  You can find our product's user manual, API reference, release notes and
*                  more information at: https://doc.micrium.com
*
*                  You can contact us at: http://www.micrium.com
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                    USB DEVICE CONFIGURATION FILE
*
*                                              TEMPLATE
*
* File          : usbd_dev_cfg.c
* Version       : V4.05.00
* Programmer(s) : FGK
*                 OD
*********************************************************************************************************
*/

#include <Cfg/usbd_dev_cfg.h>
#include  <Source/usbd_core.h>
#include  <Drivers/STM32F_HS/usbd_bsp_stm32f_hs.h>
#include  <Drivers/STM32F_FS/usbd_bsp_stm32f_fs.h>
#ifdef STM32F446xx
#include "stm32f446xx.h"
#endif
#ifdef STM32F469xx
#include "stm32f469xx.h"
#endif

/*
*********************************************************************************************************
*                                      USB DEVICE CONFIGURATION
*********************************************************************************************************
*/

const USBD_DEV_CFG  USBD_DevCfg_stm32f_hs = {
	0xFFFE,// , 1155                                                    /* Vendor  ID.                                          */
    0x1234,//, 22314                                                    /* Product ID.                                          */
    0x0100, //,0x0200                                                     /* Device release number.                               */
   "STMicroelectronics",                                      /* Manufacturer  string.                                */
   "STM32 Mass Storage",                                           /* Product       string.                                */
   "00000000001A",                                          /* Serial number string.                                */
   USBD_LANG_ID_ENGLISH_US                                     /* String language ID.                                  */
};

const USBD_DEV_CFG  USBD_DevCfg_stm32f_fs = {
    0xFFFE,                                                     /* Vendor  ID.                                          */
    0x1234,                                                     /* Product ID.                                          */
    0x0100,                                                     /* Device release number.                               */
   "AAB MANUFACTURER",                                      /* Manufacturer  string.                                */
   "stm32f_fs",                                           /* Product       string.                                */
   "1234567890ABCDEF",                                          /* Serial number string.                                */
    USBD_LANG_ID_ENGLISH_US                                     /* String language ID.                                  */
};

char g_deviceDedicatedMem[127];

/*
*********************************************************************************************************
*                                   USB DEVICE DRIVER CONFIGURATION
*********************************************************************************************************
*/

const USBD_DRV_CFG  USBD_DrvCfg_stm32f_hs = {
		USB_OTG_HS_PERIPH_BASE,                                                 /* Base addr   of device controller hw registers.       */
		(CPU_ADDR)g_deviceDedicatedMem,                                                 /* Base addr   of device controller dedicated mem.      */
             127u,                                                /* Size        of device controller dedicated mem.      */
#ifdef FORCE_FS
			 USBD_DEV_SPD_FULL,                                          /* Speed       of device controller.                    */
#else
	#ifndef USE_ULPI
			 USBD_DEV_SPD_FULL,                                          /* Speed       of device controller.                    */
	#else
			 USBD_DEV_SPD_HIGH,                                          /* Speed       of device controller.                    */
	#endif
#endif
    USBD_DrvEP_InfoTbl_stm32f_hs                                 /* EP Info tbl of device controller.                    */
};

const USBD_DRV_CFG  USBD_DrvCfg_stm32f_fs = {
		USB_OTG_FS_PERIPH_BASE /*0x00000000*/,                                                 /* Base addr   of device controller hw registers.       */
		(CPU_ADDR)g_deviceDedicatedMem /* 0x00000000*/,                                                 /* Base addr   of device controller dedicated mem.      */
		 127u,                                                /* Size        of device controller dedicated mem.      */
		 USBD_DEV_SPD_FULL,                                          /* Speed       of device controller.                    */
		 USBD_DrvEP_InfoTbl_stm32f_fs                                 /* EP Info tbl of device controller.                    */
};
