/*
*********************************************************************************************************
*                                            EXAMPLE CODE
*
*               This file is provided as an example on how to use Micrium products.
*
*               Please feel free to use any application code labeled as 'EXAMPLE CODE' in
*               your application products.  Example code may be used as is, in whole or in
*               part, or may be used as a reference only. This file can be modified as
*               required to meet the end-product requirements.
*
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*
*               You can find our product's user manual, API reference, release notes and
*               more information at: https://doc.micrium.com
*
*               You can contact us at: http://www.micrium.com
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                             USB DEVICE DRIVER BOARD-SPECIFIC FUNCTIONS
*
*                                              stm32f_fs
*
* File          : usbd_bsp_stm32f_fs.h
* Version       : V4.05.00.00
* Programmer(s) : FGK
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                            INCLUDE FILES
*********************************************************************************************************
*/

#include  "../../Source/usbd_core.h"
#include  "usbd_bsp_stm32f_fs.h"
#include "stm32f4xx_hal.h"
#include "../../../bsp/bsp.h"

#define USB1_SEL_SW_Pin GPIO_PIN_9
#define USB1_SEL_SW_GPIO_Port GPIOI
#define DUAL_GREEN_LED_Pin GPIO_PIN_1
#define DUAL_GREEN_LED_GPIO_Port GPIOC

/*
*********************************************************************************************************
*                                            LOCAL DEFINES
*
* Note(s) : $$$$ You may define any register outside the USB device controller registers interface needed
*           to configure the clock, interrupt and I/O.
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                           LOCAL CONSTANTS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                            LOCAL TABLES
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                USB DEVICE ENDPOINT INFORMATION TABLE
*********************************************************************************************************
*/

const USBD_DRV_EP_INFO  USBD_DrvEP_InfoTbl_stm32f_fs[] = {
    {USBD_EP_INFO_TYPE_CTRL                                                   | USBD_EP_INFO_DIR_OUT, 0u,   64u},
    {USBD_EP_INFO_TYPE_CTRL                                                   | USBD_EP_INFO_DIR_IN,  0u,   64u},
    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_OUT, 1u, 1024u},
    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_IN,  1u, 1024u},
    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_OUT, 2u, 1024u},
    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_IN,  2u, 1024u},
    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_OUT, 3u, 1024u},
    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_IN,  3u, 1024u},
    {DEF_BIT_NONE                                                                                 ,   0u,    0u}
};


/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/

static  USBD_DRV  *USBD_BSP_stm32f_fs_DrvPtr;


/*
*********************************************************************************************************
*                                            LOCAL MACRO'S
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void  USBD_BSP_stm32f_fs_Init           (USBD_DRV  *p_drv);
static  void  USBD_BSP_stm32f_fs_Conn           (void            );
static  void  USBD_BSP_stm32f_fs_Disconn        (void            );
static  void  USBD_BSP_stm32f_fs_IntHandler 	(void /*     *p_arg*/);

void HAL_ll_init_fs();
/*
*********************************************************************************************************
*                                   USB DEVICE DRIVER BSP INTERFACE
*********************************************************************************************************
*/

USBD_DRV_BSP_API  USBD_DrvBSP_stm32f_fs =
{
    USBD_BSP_stm32f_fs_Init,
    USBD_BSP_stm32f_fs_Conn,
    USBD_BSP_stm32f_fs_Disconn
};


/*
*********************************************************************************************************
*                                     LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*********************************************************************************************************
*                                           LOCAL FUNCTIONS
*********************************************************************************************************
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                       USBD_BSP_stm32f_fs_Init()
*
* Description : USB device controller board-specific initialization.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
* Return(s)   : none.
*
* Caller(s)   : Device controller driver init function via 'p_drv_api->Init()'
*
* Note(s)     : none.
*********************************************************************************************************
*/

PCD_HandleTypeDef hpcd;


static  void  USBD_BSP_stm32f_fs_Init (USBD_DRV  *p_drv)
{
    (void)&p_drv;

    /* $$$$ If ISR does NOT support argument passing, save reference to USBD_DRV in a local global variable. */
    USBD_BSP_stm32f_fs_DrvPtr = p_drv;

    /* $$$$ This function perform all operations that the device controller cannot do. Typical operations are: */

    /* $$$$ Enable device control registers and bus clock [mandatory]. */
    /* $$$$ Configure main USB device interrupt in interrupt controller (e.g. registering BSP ISR) [mandatory]. */
    BSP_IntVectSet(BSP_INT_ID_OTG_FS, &USBD_BSP_stm32f_fs_IntHandler);
#if 1
    /* Init the low level hardware : GPIO, CLOCK, NVIC... */
    /* Set LL Driver parameters */
    /* Set LL Driver parameters */
    hpcd.Instance = USB_OTG_FS;
    hpcd.Init.dev_endpoints = 4;
//    hpcd.Init.dev_endpoints = 6;
    hpcd.Init.use_dedicated_ep1 = 0;
    hpcd.Init.ep0_mps = 0x40;
//    hpcd.Init.ep0_mps = DEP0CTL_MPS_64;
    hpcd.Init.dma_enable = 0;
    hpcd.Init.low_power_enable = 0;
    hpcd.Init.phy_itface = PCD_PHY_EMBEDDED;
    hpcd.Init.Sof_enable = 0;
    hpcd.Init.speed = PCD_SPEED_FULL;
    hpcd.Init.vbus_sensing_enable = 0;
    hpcd.Init.lpm_enable = 0;

     /* Link The driver to the stack */
     hpcd.pData = p_drv;
     //hpcd.pData = pdev;

//     pdev->pData = &hpcd;
     p_drv->DataPtr = &hpcd;
#endif
     /* Initialize LL Driver */
     HAL_ll_init_fs();
//     HAL_PCD_Init(&hpcd);

     HAL_PCDEx_SetRxFiFo(&hpcd, 0x80);
     HAL_PCDEx_SetTxFiFo(&hpcd, 0, 0x40);
     HAL_PCDEx_SetTxFiFo(&hpcd, 1, 0x80);

    /* $$$$ Disable device transceiver clock [optional]. */
    /* $$$$ Configure I/O pins [optional]. */
}

void HAL_ll_init_fs()
{
	GPIO_InitTypeDef  GPIO_InitStruct;

#ifdef STM32446E_EVAL
    /* Configure USB HS GPIOs */
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();

    /* CLK */
    GPIO_InitStruct.Pin 		= GPIO_PIN_5;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Speed 		= GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate 	= GPIO_AF10_OTG_FS;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* D0 */
    GPIO_InitStruct.Pin 		= GPIO_PIN_3;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Speed 		= GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate 	= GPIO_AF10_OTG_FS;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* D1 D2 D3 D4 D5 D6 D7 */
    GPIO_InitStruct.Pin 		= GPIO_PIN_0  | GPIO_PIN_1  | GPIO_PIN_5 | GPIO_PIN_10 | GPIO_PIN_2 | GPIO_PIN_12 | GPIO_PIN_13;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Alternate	 = GPIO_AF10_OTG_FS;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* STP */
    GPIO_InitStruct.Pin 		= GPIO_PIN_0;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Alternate 	= GPIO_AF10_OTG_FS;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* NXT */
    GPIO_InitStruct.Pin 		= GPIO_PIN_3;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Alternate 	= GPIO_AF10_OTG_FS;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* DIR */
    GPIO_InitStruct.Pin 		= GPIO_PIN_2;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Alternate 	= GPIO_AF10_OTG_FS;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    __HAL_RCC_USB_OTG_FS_ULPI_CLK_ENABLE();

    /* Enable USB HS Clocks */
    __HAL_RCC_USB_OTG_FS_CLK_ENABLE();

    /* Set USBHS Interrupt to the lowest priority */
    HAL_NVIC_SetPriority(OTG_FS_IRQn, 7, 0);

    /* Enable USBHS Interrupt */
    HAL_NVIC_EnableIRQ(OTG_FS_IRQn);

#endif // STM32446E_EVAL

#ifdef SUBWAY_BREADBOARD
    /* Configure USB HS GPIOs */

    /**USB_OTG_FS GPIO Configuration
    PA12     ------> USB_OTG_FS_DP
    PA11     ------> USB_OTG_FS_DM
    */

    __HAL_RCC_GPIOA_CLK_ENABLE();
//    __HAL_RCC_GPIOB_CLK_ENABLE();
//    __HAL_RCC_GPIOC_CLK_ENABLE();
//    __HAL_RCC_GPIOI_CLK_ENABLE();
//    __HAL_RCC_GPIOH_CLK_ENABLE();
    /* Configure DM DP Pins */
    GPIO_InitStruct.Pin 		= GPIO_PIN_12|GPIO_PIN_11;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Speed 		= GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate	= GPIO_AF10_OTG_FS;

    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

//    /* Configure VBUS Pin */
//     GPIO_InitStruct.Pin = GPIO_PIN_9;
//     GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//     GPIO_InitStruct.Pull = GPIO_NOPULL;
//     HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//
//     /* Configure ID pin */
//     GPIO_InitStruct.Pin = GPIO_PIN_10;
//     GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
//     GPIO_InitStruct.Pull = GPIO_PULLUP;
//     GPIO_InitStruct.Alternate = GPIO_AF10_OTG_FS;
//     HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//
     /* Peripheral clock enable */
   __HAL_RCC_USB_OTG_FS_CLK_ENABLE();
//   __HAL_RCC_USB_OTG_HS_ULPI_CLK_ENABLE();

    /* Peripheral interrupt init */
    HAL_NVIC_SetPriority(OTG_FS_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(OTG_FS_IRQn);

#endif // SUBWAY_BREADBOARD

#ifdef SUBWAY_PRODUCT
    /* Configure USB HS GPIOs */

    /**USB_OTG_FS GPIO Configuration
    PB5     ------> USB_OTG_HS_ULPI_D7
    PI11     ------> USB_OTG_HS_ULPI_DIR
    PB12     ------> USB_OTG_HS_ULPI_D5
    PC0     ------> USB_OTG_HS_ULPI_STP
    PB13     ------> USB_OTG_HS_ULPI_D6
    PB11     ------> USB_OTG_HS_ULPI_D4
    PA5     ------> USB_OTG_HS_ULPI_CK
    PH4     ------> USB_OTG_HS_ULPI_NXT
    PB10     ------> USB_OTG_HS_ULPI_D3
    PB1     ------> USB_OTG_HS_ULPI_D2
    PA3     ------> USB_OTG_HS_ULPI_D0
    PB0     ------> USB_OTG_HS_ULPI_D1
    */

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOI_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();

    GPIO_InitStruct.Pin 		= GPIO_PIN_5|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_11
    											|GPIO_PIN_10|GPIO_PIN_1|GPIO_PIN_0;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Speed 		= GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate	= GPIO_AF10_OTG_FS;

    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin 		= GPIO_PIN_11;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Speed 		= GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate 	= GPIO_AF10_OTG_FS;

    HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

    GPIO_InitStruct.Pin 		= GPIO_PIN_0;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Speed 		= GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate 	= GPIO_AF10_OTG_FS;

    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin 		= GPIO_PIN_5|GPIO_PIN_3;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Speed 		= GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate 	= GPIO_AF10_OTG_FS;

    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin 		= GPIO_PIN_4;
    GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 		= GPIO_NOPULL;
    GPIO_InitStruct.Speed 		= GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate 	= GPIO_AF10_OTG_FS;

    HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

    /* Peripheral clock enable */
   __HAL_RCC_USB_OTG_FS_CLK_ENABLE();
   __HAL_RCC_USB_OTG_HS_ULPI_CLK_ENABLE();

    /* Peripheral interrupt init */
    HAL_NVIC_SetPriority(OTG_FS_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(OTG_FS_IRQn);

#endif // SUBWAY_PRODUCT

}
#if 0
/**
  * @brief  Initializes the PCD MSP.
  * @param  hpcd: PCD handle
  * @retval None
  */
void HAL_PCD_ll_init_fs(PCD_HandleTypeDef *hpcd)
{
  GPIO_InitTypeDef  GPIO_InitStruct;


    /* Configure USB FS GPIOs */
     __HAL_RCC_GPIOA_CLK_ENABLE();

    /* Configure DM DP Pins */
    GPIO_InitStruct.Pin = (GPIO_PIN_11 | GPIO_PIN_12);
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF10_OTG_FS;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
#if 0
    /* Configure VBUS Pin */
    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* Configure ID pin */
    GPIO_InitStruct.Pin = GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Alternate = GPIO_AF10_OTG_FS;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
#endif
    /* Enable USB FS Clock */
    __HAL_RCC_USB_OTG_FS_CLK_ENABLE();

    /* Set USBFS Interrupt priority */
    HAL_NVIC_SetPriority(OTG_FS_IRQn, 7, 0);

    /* Enable USBFS Interrupt */
    HAL_NVIC_EnableIRQ(OTG_FS_IRQn);

}
#endif
/**
  * @brief  De-Initializes the PCD MSP.
  * @param  hpcd: PCD handle
  * @retval None
  */
/*
void HAL_PCD_MspDeInit(PCD_HandleTypeDef *hpcd)
{
  if(hpcd->Instance == USB_OTG_FS)
  {
     Disable USB FS Clock
    __HAL_RCC_USB_OTG_FS_CLK_DISABLE();
    __HAL_RCC_SYSCFG_CLK_DISABLE();
  }
  else if(hpcd->Instance == USB_OTG_HS)
  {
     Disable USB HS Clocks
    __HAL_RCC_USB_OTG_HS_CLK_DISABLE();
    __HAL_RCC_SYSCFG_CLK_DISABLE();
  }
}
*/


/*
*********************************************************************************************************
*                                       USBD_BSP_stm32f_fs_Conn()
*
* Description : Connect pull-up on DP.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : Device controller driver start function via 'p_drv_api->Conn()'
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  USBD_BSP_stm32f_fs_Conn (void)
{
//	  USB_DevConnect (hpcd.Instance);
    /* $$$$ Enable device transceiver clock [optional]. */
    /* $$$$ Enable pull-up resistor (this operation may be done in the driver) [mandatory]. */
    /* $$$$ Enable main USB device interrupt [mandatory]. */
}


/*
*********************************************************************************************************
*                                     USBD_BSP_stm32f_fs_Disconn()
*
* Description : Disconnect pull-up on DP.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : Device controller driver stop function via 'p_drv_api->Disconn()'
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  USBD_BSP_stm32f_fs_Disconn (void)
{
	 USB_DevDisconnect (hpcd.Instance);
    /* $$$$ Disable device transceiver clock [optional]. */
    /* $$$$ Disable pull-up resistor (this operation may be done in the driver) [mandatory]. */
    /* $$$$ Disable main USB device interrupt [mandatory]. */
}


/*
*********************************************************************************************************
*                                     USBD_BSP_<controller>_IntHandler()
*
* Description : USB device interrupt handler.
*
* Argument(s) : p_arg   Interrupt handler argument.
*
* Return(s)   : none.
*
* Caller(s)   : This is a ISR.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  USBD_BSP_stm32f_fs_IntHandler (void /* *p_arg*/)
{
    USBD_DRV      *p_drv;
    USBD_DRV_API  *p_drv_api;


                                                                /* Get a reference to USBD_DRV.                         */
    /* $$$$ If ISR does NOT support argument passing, get USBD_DRV from a global variable initialized in USBD_BSP_stm32f_fs_Init(). */
    p_drv = USBD_BSP_stm32f_fs_DrvPtr;
    /* $$$$ Otherwise if ISR supports argument passing, get USBD_DRV from the argument. Do not forget to pass USBD_DRV structure when registering the BSP ISR in USBD_BSP_stm32f_fs_Init(). */
   // p_drv = (USBD_DRV *)p_arg;

    p_drv_api = p_drv->API_Ptr;                                 /* Get a reference to USBD_DRV_API.                     */
    p_drv_api->ISR_Handler(p_drv);                              /* Call the USB Device driver ISR.                      */
}


