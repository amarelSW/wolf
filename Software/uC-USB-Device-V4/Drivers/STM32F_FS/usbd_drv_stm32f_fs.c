/*
*********************************************************************************************************
*                                            uC/USB-Device
*                                       The Embedded USB Stack
*
*                         (c) Copyright 2004-2014; Micrium, Inc.; Weston, FL
*
*                  All rights reserved.  Protected by international copyright laws.
*
*                  uC/USB-Device is provided in source form to registered licensees ONLY.  It is
*                  illegal to distribute this source code to any third party unless you receive
*                  written permission by an authorized Micrium representative.  Knowledge of
*                  the source code may NOT be used to develop a similar product.
*
*                  Please help us continue to provide the Embedded community with the finest
*                  software available.  Your honesty is greatly appreciated.
*
*                  You can find our product's user manual, API reference, release notes and
*                  more information at: https://doc.micrium.com
*
*                  You can contact us at: http://www.micrium.com
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                          USB DEVICE DRIVER
*
*                                  USB ON-THE-GO FULL-SPEED (OTG_FS)
*
* File          : usbd_drv_stm32f_fs.c
* Version       : V4.05.00.00
* Programmer(s) : FGK
*                 FF
*                 JFD
*********************************************************************************************************
* Note(s)       : (1) You can find specific information about this driver at:
*                     https://doc.micrium.com/display/USBDDRV/STM32F_FS
*
*                 (2) With an appropriate BSP, this device driver will support the OTG_FS device module on
*                     the STMicroelectronics STM32F10xxx, STM32F2xx, and STM32F4xx MCUs, this applies to
*                     the following devices:
*
*                         STM32F105xx series.
*                         STM32F107xx series.
*                         STM32F205xx series.
*                         STM32F207xx series.
*                         STM32F215xx series.
*                         STM32F217xx series.
*                         STM32F405xx series.
*                         STM32F407xx series.
*                         STM32F415xx series.
*                         STM32F417xx series.
*
*
*                 (3) This device driver DOES NOT support the USB on-the-go high-speed(OTG_HS) device module
*                     of the STM32F2xx and STM32F4xx series.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                            INCLUDE FILES
*********************************************************************************************************
*/

#define    MICRIUM_SOURCE
#include  "../../Source/usbd_core.h"
#include  "usbd_drv_stm32f_fs.h"

/*
*********************************************************************************************************
*                                            LOCAL DEFINES
*********************************************************************************************************
*/

#define  STM32F_FS_REG_VAL_TO                        200000U
#define  STM32F_FS_REG_FMOD_TO                       0x7FFFFu

#define  USBD_DBG_DRV_EP(msg, ep_addr)               USBD_DBG_GENERIC((msg),                                            \
                                                                      (ep_addr),                                        \
                                                                       USBD_IF_NBR_NONE)

#define  USBD_DBG_DRV_EP_ARG(msg, ep_addr, arg)      USBD_DBG_GENERIC_ARG((msg),                                        \
                                                                          (ep_addr),                                    \
                                                                           USBD_IF_NBR_NONE,                            \
                                                                          (arg))

/*
*********************************************************************************************************
*                                   STM32 OTG USB DEVICE CONSTRAINTS
*********************************************************************************************************
*/
#define  STM32F_FS_USE_DMA							DEF_DISABLED
#define  STM32F_FS_MAX_NBR_EPS                         15u      /* Maximum number of endpoints							*/
#define  STM32F_FS_NBR_EPS                              6u      /* current number of endpoints                          */
#define  STM32F_FS_NBR_CHANNEL                          8u      /* Maximum number of channels                           */
#define  STM32F_FS_DFIFO_SIZE                        1024u      /* Number of entries                                    */
#define  STM32F_FS_MAX_PKT_SIZE                        64u

#define  STM32F_FS_RXFIFO_SIZE                        0x80
#define  STM32F_FS_TXFIFO_EP0_SIZE                     0x40
#define  STM32F_FS_TXFIFO_EP1_SIZE                     0x80
#define  STM32F_FS_TXFIFO_EP2_SIZE                     128u
#define  STM32F_FS_TXFIFO_EP3_SIZE                     128u

//#define  STM32F_FS_RXFIFO_SIZE                        100u
//#define  STM32F_FS_TXFIFO_EP0_SIZE                     64u
//#define  STM32F_FS_TXFIFO_EP1_SIZE                     52u
//#define  STM32F_FS_TXFIFO_EP2_SIZE                     52u
//#define  STM32F_FS_TXFIFO_EP3_SIZE                     52u

#define STM32F_FS_TRDT_VALUE           6U

#define STM32F_SPEED_HIGH                     0U
#define STM32F_SPEED_HIGH_IN_FULL             1U
#define STM32F_SPEED_LOW                      2U
#define STM32F_SPEED_FULL                     3U

/*
*********************************************************************************************************
*                                        REGISTER BIT DEFINES
*********************************************************************************************************
*/

#define  STM32F_FS_GOTGINT_BIT_SEDET             DEF_BIT_02
#define  STM32F_FS_GOTGINT_BIT_SRSSCHG           DEF_BIT_08
#define  STM32F_FS_GOTGINT_BIT_HNSSCHG           DEF_BIT_09
#define  STM32F_FS_GOTGINT_BIT_HNGDET            DEF_BIT_17
#define  STM32F_FS_GOTGINT_BIT_ADTOCHG           DEF_BIT_18
#define  STM32F_FS_GOTGINT_BIT_DBCDNE            DEF_BIT_19


#define  STM32F_FS_GAHBCFG_BIT_TXFELVL           DEF_BIT_07
#define  STM32F_FS_GAHBCFG_BIT_GINTMSK           DEF_BIT_00
#define  STM32F_FS_GAHBCFG_BIT_DMAEN			 DEF_BIT_05
#define  STM32F_FS_GAHBCFG_HBSTLEN_INCR4         DEF_BIT_MASK(3u, 1u)

#define  STM32F_FS_GUSBCFG_BIT_FDMOD             DEF_BIT_30
#define  STM32F_FS_GUSBCFG_BIT_FHMOD             DEF_BIT_29
#define	 STM32F_FS_GUSBCFG_BIT_PHYLPCS			 DEF_BIT_15
#define  STM32F_FS_GUSBCFG_BIT_HNPCAP            DEF_BIT_09
#define  STM32F_FS_GUSBCFG_BIT_SRPCAP            DEF_BIT_08
//#define  STM32F_FS_GUSBCFG_BIT_PHYSEL            DEF_BIT_07
#define  STM32F_FS_GUSBCFG_BIT_PHYSEL            DEF_BIT_06
#define  STM32F_FS_GUSBCFG_TOCAL_MASK            DEF_BIT_FIELD(7u, 0u)
#define  STM32F_FS_GUSBCFG_TRDT_MASK             DEF_BIT_FIELD(15u, 10u)

#define  STM32F_FS_GRSTCTL_BIT_AHBIDL            DEF_BIT_31
#define  STM32F_FS_GRSTCTL_FLUSH_TXFIFO_00       DEF_BIT_NONE
#define  STM32F_FS_GRSTCTL_FLUSH_TXFIFO_01       DEF_BIT_MASK(1u, 6u)
#define  STM32F_FS_GRSTCTL_FLUSH_TXFIFO_02       DEF_BIT_MASK(2u, 6u)
#define  STM32F_FS_GRSTCTL_FLUSH_TXFIFO_03       DEF_BIT_MASK(3u, 6u)
#define  STM32F_FS_GRSTCTL_FLUSH_TXFIFO_ALL      DEF_BIT_MASK(16u, 6u)
#define  STM32F_FS_GRSTCTL_BIT_TXFFLSH           DEF_BIT_05
#define  STM32F_FS_GRSTCTL_BIT_RXFFLSH           DEF_BIT_04
#define  STM32F_FS_GRSTCTL_BIT_HSRST             DEF_BIT_01
#define  STM32F_FS_GRSTCTL_BIT_CSRST             DEF_BIT_00

#define  STM32F_FS_GINTSTS_BIT_WKUPINT           DEF_BIT_31
#define  STM32F_FS_GINTSTS_BIT_SRQINT            DEF_BIT_30
#define  STM32F_FS_GINTSTS_BIT_DISCINT           DEF_BIT_29
#define  STM32F_FS_GINTSTS_BIT_CIDSCHG           DEF_BIT_28
#define  STM32F_FS_GINTSTS_BIT_INCOMPISOOUT      DEF_BIT_21
#define  STM32F_FS_GINTSTS_BIT_IISOIXFR          DEF_BIT_20
#define  STM32F_FS_GINTSTS_BIT_OEPINT            DEF_BIT_19
#define  STM32F_FS_GINTSTS_BIT_IEPINT            DEF_BIT_18
#define  STM32F_FS_GINTSTS_BIT_EOPF              DEF_BIT_15
#define  STM32F_FS_GINTSTS_BIT_ISOODRP           DEF_BIT_14
#define  STM32F_FS_GINTSTS_BIT_ENUMDNE           DEF_BIT_13
#define  STM32F_FS_GINTSTS_BIT_USBRST            DEF_BIT_12
#define  STM32F_FS_GINTSTS_BIT_USBSUSP           DEF_BIT_11
#define  STM32F_FS_GINTSTS_BIT_ESUSP             DEF_BIT_10
#define  STM32F_FS_GINTSTS_BIT_GONAKEFF          DEF_BIT_07
#define  STM32F_FS_GINTSTS_BIT_GINAKEFF          DEF_BIT_06
#define  STM32F_FS_GINTSTS_BIT_RXFLVL            DEF_BIT_04
#define  STM32F_FS_GINTSTS_BIT_SOF               DEF_BIT_03
#define  STM32F_FS_GINTSTS_BIT_OTGINT            DEF_BIT_02
#define  STM32F_FS_GINTSTS_BIT_MMIS              DEF_BIT_01
#define  STM32F_FS_GINTSTS_BIT_CMOD              DEF_BIT_00
                                                                /* Interrupts are clear by writing 1 to its bit         */
#define  STM32F_FS_GINTSTS_INT_ALL              (STM32F_FS_GINTSTS_BIT_WKUPINT  | STM32F_FS_GINTSTS_BIT_SRQINT       |                 \
                                                 STM32F_FS_GINTSTS_BIT_CIDSCHG  | STM32F_FS_GINTSTS_BIT_INCOMPISOOUT |                 \
                                                 STM32F_FS_GINTSTS_BIT_IISOIXFR | STM32F_FS_GINTSTS_BIT_EOPF         |                 \
                                                 STM32F_FS_GINTSTS_BIT_ISOODRP  | STM32F_FS_GINTSTS_BIT_ENUMDNE      |                 \
                                                 STM32F_FS_GINTSTS_BIT_USBRST   | STM32F_FS_GINTSTS_BIT_USBSUSP      |                 \
                                                 STM32F_FS_GINTSTS_BIT_ESUSP    | STM32F_FS_GINTSTS_BIT_SOF          |                 \
                                                 STM32F_FS_GINTSTS_BIT_MMIS)

#define  STM32F_FS_GINTMSK_BIT_WUIM              DEF_BIT_31
#define  STM32F_FS_GINTMSK_BIT_SRQIM             DEF_BIT_30
#define  STM32F_FS_GINTMSK_BIT_DISCINT           DEF_BIT_29
#define  STM32F_FS_GINTMSK_BIT_CIDSCHGM          DEF_BIT_28
#define  STM32F_FS_GINTMSK_BIT_IISOOXFRM         DEF_BIT_21
#define  STM32F_FS_GINTMSK_BIT_IISOIXFRM         DEF_BIT_20
#define  STM32F_FS_GINTMSK_BIT_OEPINT            DEF_BIT_19
#define  STM32F_FS_GINTMSK_BIT_IEPINT            DEF_BIT_18
//#define  STM32F_FS_GINTMSK_BIT_EPMISM            DEF_BIT_17
#define  STM32F_FS_GINTMSK_BIT_EOPFM             DEF_BIT_15
#define  STM32F_FS_GINTMSK_BIT_ISOODRPM          DEF_BIT_14
#define  STM32F_FS_GINTMSK_BIT_ENUMDNEM          DEF_BIT_13
#define  STM32F_FS_GINTMSK_BIT_USBRST            DEF_BIT_12
#define  STM32F_FS_GINTMSK_BIT_USBSUSPM          DEF_BIT_11
#define  STM32F_FS_GINTMSK_BIT_ESUSPM            DEF_BIT_10
#define  STM32F_FS_GINTMSK_BIT_GONAKEFFM         DEF_BIT_07
#define  STM32F_FS_GINTMSK_BIT_GINAKEFFM         DEF_BIT_06
#define  STM32F_FS_GINTMSK_BIT_RXFLVLM           DEF_BIT_04
#define  STM32F_FS_GINTMSK_BIT_SOFM              DEF_BIT_03
#define  STM32F_FS_GINTMSK_BIT_OTGINT            DEF_BIT_02
#define  STM32F_FS_GINTMSK_BIT_MMISM             DEF_BIT_01

#define  STM32F_FS_GRXSTSx_PKTSTS_OUT_NAK        1u
#define  STM32F_FS_GRXSTSx_PKTSTS_OUT_RX         2u
#define  STM32F_FS_GRXSTSx_PKTSTS_OUT_COMPL      3u
#define  STM32F_FS_GRXSTSx_PKTSTS_SETUP_COMPL    4u
#define  STM32F_FS_GRXSTSx_PKTSTS_SETUP_RX       6u
#define  STM32F_FS_GRXSTSx_PKTSTS_MASK           DEF_BIT_FIELD(4u, 17u)
#define  STM32F_FS_GRXSTSx_EPNUM_MASK            DEF_BIT_FIELD(2u, 0u)
#define  STM32F_FS_GRXSTSx_BCNT_MASK             DEF_BIT_FIELD(11u, 4u)

#define  STM32F_FS_GCCFG_BIT_VBDEN				 DEF_BIT_21
#define  STM32F_FS_GCCFG_BIT_SOFOUTEN            DEF_BIT_20
#define  STM32F_FS_GCCFG_BIT_VBUSBEN             DEF_BIT_19
#define  STM32F_FS_GCCFG_BIT_VBUSAEN             DEF_BIT_18
#define  STM32F_FS_GCCFG_BIT_PWRDWN              DEF_BIT_16

#define STM32F_FS_GOTGCTL_BVALOEN                 DEF_BIT_06         /*!< B-peripheral session valid override enable */
#define STM32F_FS_GOTGCTL_BVALOVAL                DEF_BIT_07         /*!< B-peripheral session valid override value  */
#define STM32F_FS_GOTGCTL_BIT_SRQ              	 DEF_BIT_01


#define  STM32F_FS_DCFG_PFIVL_80                 DEF_BIT_MASK(0u, 11u)
#define  STM32F_FS_DCFG_PFIVL_85                 DEF_BIT_MASK(1u, 11u)
#define  STM32F_FS_DCFG_PFIVL_90                 DEF_BIT_MASK(2u, 11u)
#define  STM32F_FS_DCFG_PFIVL_95                 DEF_BIT_MASK(3u, 11u)
#define  STM32F_FS_DCFG_BIT_NZLSOHSK             DEF_BIT_02
#define  STM32F_FS_DCFG_DSPD_FULLSPEED           DEF_BIT_MASK(3u, 0u)
#define  STM32F_FS_DCFG_DAD_MASK                 DEF_BIT_FIELD(7u, 4u)

#define  STM32F_FS_DCTL_BIT_POPRGDNE             DEF_BIT_11
#define  STM32F_FS_DCTL_BIT_CGONAK               DEF_BIT_10
#define  STM32F_FS_DCTL_BIT_SGONAK               DEF_BIT_09
#define  STM32F_FS_DCTL_BIT_CGINAK               DEF_BIT_08
#define  STM32F_FS_DCTL_BIT_SGINAK               DEF_BIT_07
#define  STM32F_FS_DCTL_BIT_GONSTS               DEF_BIT_03
#define  STM32F_FS_DCTL_BIT_GINSTS               DEF_BIT_02
#define  STM32F_FS_DCTL_BIT_SDIS                 DEF_BIT_01
#define  STM32F_FS_DCTL_BIT_RWUSIG               DEF_BIT_00

#define  STM32F_FS_DSTS_BIT_EERR                 DEF_BIT_03
#define  STM32F_FS_DSTS_ENUMSPD_MASK     		 DEF_BIT_MASK(3u, 1u)
#define  STM32F_FS_DSTS_ENUMSPD_FS_PHY_48MHZ     DEF_BIT_MASK(3u, 1u)
#define  STM32F_FS_DSTS_ENUMSPD_LS_PHY_6MHZ      DEF_BIT_MASK(2u, 1u)
#define  STM32F_FS_DSTS_ENUMSPD_FS_PHY_30MHZ_OR_60MHZ	DEF_BIT_MASK(1u, 1u)
#define  STM32F_FS_DSTS_BIT_SUSPSTS              DEF_BIT_00
#define  STM32F_FS_DSTS_FNSOF_MASK               DEF_BIT_FIELD(14u, 8u)

#define  STM32F_FS_DIEPMSK_BIT_TXFURM            DEF_BIT_08  /*!< FIFO underrun mask                                */
#define  STM32F_FS_DIEPMSK_BIT_INEPNEM           DEF_BIT_06
#define  STM32F_FS_DIEPMSK_BIT_INEPNMM           DEF_BIT_05
#define  STM32F_FS_DIEPMSK_BIT_ITTXFEMSK         DEF_BIT_04
#define  STM32F_FS_DIEPMSK_BIT_TOM               DEF_BIT_03
#define  STM32F_FS_DIEPMSK_BIT_EPDM              DEF_BIT_01
#define  STM32F_FS_DIEPMSK_BIT_XFRCM             DEF_BIT_00

#define  STM32F_FS_DOEPMSK_BIT_OTEPDM            DEF_BIT_04
#define  STM32F_FS_DOEPMSK_BIT_STUPM             DEF_BIT_03
#define  STM32F_FS_DOEPMSK_BIT_EPDM              DEF_BIT_01
#define  STM32F_FS_DOEPMSK_BIT_XFRCM             DEF_BIT_00

#define  STM32F_FS_DAINT_BIT_OEPINT_EP0          DEF_BIT_16
#define  STM32F_FS_DAINT_BIT_OEPINT_EP1          DEF_BIT_17
#define  STM32F_FS_DAINT_BIT_OEPINT_EP2          DEF_BIT_18
#define  STM32F_FS_DAINT_BIT_OEPINT_EP3          DEF_BIT_19
#define  STM32F_FS_DAINT_BIT_IEPINT_EP0          DEF_BIT_00
#define  STM32F_FS_DAINT_BIT_IEPINT_EP1          DEF_BIT_01
#define  STM32F_FS_DAINT_BIT_IEPINT_EP2          DEF_BIT_02
#define  STM32F_FS_DAINT_BIT_IEPINT_EP3          DEF_BIT_03

#define  STM32F_FS_DAINTMSK_BIT_OEPINT_EP0       DEF_BIT_16
#define  STM32F_FS_DAINTMSK_BIT_OEPINT_EP1       DEF_BIT_17
#define  STM32F_FS_DAINMSKT_BIT_OEPINT_EP2       DEF_BIT_18
#define  STM32F_FS_DAINTMSK_BIT_OEPINT_EP3       DEF_BIT_19
#define  STM32F_FS_DAINTMSK_OEPINT_ALL          (STM32F_FS_DAINTMSK_BIT_OEPINT_EP0 | STM32F_FS_DAINTMSK_BIT_OEPINT_EP1 |               \
                                                 STM32F_FS_DAINTMSK_BIT_OEPINT_EP2 | STM32F_FS_DAINTMSK_BIT_OEPINT_EP3)
#define  STM32F_FS_DAINTMSK_BIT_IEPINT_EP0       DEF_BIT_00
#define  STM32F_FS_DAINTMSK_BIT_IEPINT_EP1       DEF_BIT_01
#define  STM32F_FS_DAINTMSK_BIT_IEPINT_EP2       DEF_BIT_02
#define  STM32F_FS_DAINTMSK_BIT_IEPINT_EP3       DEF_BIT_03
#define  STM32F_FS_DAINTMSK_IEPINT_ALL          (STM32F_FS_DAINTMSK_BIT_IEPINT_EP0 | STM32F_FS_DAINTMSK_BIT_IEPINT_EP1 |               \
                                                 STM32F_FS_DAINTMSK_BIT_IEPINT_EP2 | STM32F_FS_DAINTMSK_BIT_IEPINT_EP3)

#define  STM32F_FS_DIEPEMPMSK_BIT_INEPTXFEM_EP0  DEF_BIT_00
#define  STM32F_FS_DIEPEMPMSK_BIT_INEPTXFEM_EP1  DEF_BIT_01
#define  STM32F_FS_DIEPEMPMSK_BIT_INEPTXFEM_EP2  DEF_BIT_02
#define  STM32F_FS_DIEPEMPMSK_BIT_INEPTXFEM_EP3  DEF_BIT_03

#define  STM32F_FS_DxEPCTLx_BIT_EPENA            DEF_BIT_31
#define  STM32F_FS_DxEPCTLx_BIT_EPDIS            DEF_BIT_30
#define  STM32F_FS_DxEPCTLx_BIT_SODDFRM          DEF_BIT_29
#define  STM32F_FS_DxEPCTLx_BIT_SD0PID           DEF_BIT_28
#define  STM32F_FS_DxEPCTLx_BIT_SEVNFRM          DEF_BIT_28
#define  STM32F_FS_DxEPCTLx_BIT_SNAK             DEF_BIT_27
#define  STM32F_FS_DxEPCTLx_BIT_CNAK             DEF_BIT_26
#define  STM32F_FS_DxEPCTLx_BIT_STALL            DEF_BIT_21
#define  STM32F_FS_DxEPCTLx_EPTYPE_CTRL          DEF_BIT_MASK(0u, 18u)
#define  STM32F_FS_DxEPCTLx_EPTYPE_ISO           DEF_BIT_MASK(1u, 18u)
#define  STM32F_FS_DxEPCTLx_EPTYPE_BULK          DEF_BIT_MASK(2u, 18u)
#define  STM32F_FS_DxEPCTLx_EPTYPE_INTR          DEF_BIT_MASK(3u, 18u)
#define  STM32F_FS_DxEPCTLx_BIT_NAKSTS           DEF_BIT_17
#define  STM32F_FS_DxEPCTLx_BIT_EONUM            DEF_BIT_16
#define  STM32F_FS_DxEPCTLx_BIT_DPID             DEF_BIT_16
#define  STM32F_FS_DxEPCTLx_BIT_USBAEP           DEF_BIT_15
#define  STM32F_FS_DxEPCTLx_MPSIZ_MASK           DEF_BIT_FIELD(11u, 0u)

#define  STM32F_FS_DxEPCTL0_MPSIZ_64             DEF_BIT_MASK(0u, 0u)
#define  STM32F_FS_DxEPCTL0_MPSIZ_64_MSK         DEF_BIT_FIELD(2u, 0u)

#define  STM32F_FS_DOEPCTLx_BIT_SD1PID           DEF_BIT_29
#define  STM32F_FS_DOEPCTLx_BIT_SNPM             DEF_BIT_20

#define  STM32F_FS_DIEPINTx_BIT_TXFE             DEF_BIT_07
#define  STM32F_FS_DIEPINTx_BIT_INEPNE           DEF_BIT_06
#define  STM32F_FS_DIEPINTx_BIT_ITTXFE           DEF_BIT_04
#define  STM32F_FS_DIEPINTx_BIT_TOC              DEF_BIT_03
#define  STM32F_FS_DIEPINTx_BIT_EPDISD           DEF_BIT_01
#define  STM32F_FS_DIEPINTx_BIT_XFRC             DEF_BIT_00

#define  STM32F_FS_DOEPINTx_BIT_B2BSTUP          DEF_BIT_06
#define  STM32F_FS_DOEPINTx_BIT_OTEPDIS          DEF_BIT_04
#define  STM32F_FS_DOEPINTx_BIT_STUP             DEF_BIT_03
#define  STM32F_FS_DOEPINTx_BIT_EPDISD           DEF_BIT_01
#define  STM32F_FS_DOEPINTx_BIT_XFRC             DEF_BIT_00

#define  STM32F_FS_DOEPTSIZx_STUPCNT_1_PKT       DEF_BIT_MASK(1u, 29u)
#define  STM32F_FS_DOEPTSIZx_STUPCNT_2_PKT       DEF_BIT_MASK(2u, 29u)
#define  STM32F_FS_DOEPTSIZx_STUPCNT_3_PKT       DEF_BIT_MASK(3u, 29u)
#define  STM32F_FS_DOEPTSIZx_XFRSIZ_MSK          DEF_BIT_FIELD(19u, 0u)
#define  STM32F_FS_DOEPTSIZx_PKTCNT_MSK          DEF_BIT_FIELD(10u, 19u)

#define  STM32F_FS_DOEPTSIZ0_BIT_PKTCNT          DEF_BIT_19
#define  STM32F_FS_DOEPTSIZ0_XFRSIZ_MAX_64       DEF_BIT_MASK(64u, 0u)

#define  STM32F_FS_DIEPTSIZx_MCNT_1_PKT          DEF_BIT_MASK(1u, 29u)
#define  STM32F_FS_DIEPTSIZx_MCNT_2_PKT          DEF_BIT_MASK(2u, 29u)
#define  STM32F_FS_DIEPTSIZx_MCNT_3_PKT          DEF_BIT_MASK(3u, 29u)
#define  STM32F_FS_DIEPTSIZx_XFRSIZ_MSK          DEF_BIT_FIELD(19u, 0u)
#define  STM32F_FS_DIEPTSIZx_PKTCNT_MSK          DEF_BIT_FIELD(10u, 19u)
#define  STM32F_FS_DIEPTSIZx_PKTCNT_01           DEF_BIT_MASK(1u, 19u)

#define  STM32F_FS_DTXFSTSx_EP_FIFO_FULL         DEF_BIT_NONE
#define  STM32F_FS_DTXFSTSx_EP_FIFO_WAVAIL_01    DEF_BIT_MASK(1u, 0u)
#define  STM32F_FS_DTXFSTSx_EP_FIFO_WAVAIL_02    DEF_BIT_MASK(2u, 0u)

#define  STM32F_FS_DOEPTSIZx_RXDPID_DATA0        DEF_BIT_MASK(0u, 29u)
#define  STM32F_FS_DOEPTSIZx_RXDPID_DATA1        DEF_BIT_MASK(2u, 29u)
#define  STM32F_FS_DOEPTSIZx_RXDPID_DATA2        DEF_BIT_MASK(1u, 29u)
#define  STM32F_FS_DOEPTSIZx_RXDPID_MDATA        DEF_BIT_MASK(3u, 29u)

#define  STM32F_FS_PCGCCTL_BIT_PHYSUSP           DEF_BIT_04
#define  STM32F_FS_PCGCCTL_BIT_GATEHCLK          DEF_BIT_01
#define  STM32F_FS_PCGCCTL_BIT_STPPCLK           DEF_BIT_00

/********************  Bit definition for USB_OTG_DTHRCTL register  ********************/
#define STM32F_FS_DTHRCTL_NONISOTHREN             0x00000001U            /*!< Nonisochronous IN endpoints threshold enable */
#define STM32F_FS_DTHRCTL_ISOTHREN                0x00000002U            /*!< ISO IN endpoint threshold enable */

#define STM32F_FS_DTHRCTL_TXTHRLEN                0x000007FCU            /*!< Transmit threshold length */
#define STM32F_FS_DTHRCTL_TXTHRLEN_0              0x00000004U            /*!<Bit 0 */
#define STM32F_FS_DTHRCTL_TXTHRLEN_1              0x00000008U            /*!<Bit 1 */
#define STM32F_FS_DTHRCTL_TXTHRLEN_2              0x00000010U            /*!<Bit 2 */
#define STM32F_FS_DTHRCTL_TXTHRLEN_3              0x00000020U            /*!<Bit 3 */
#define STM32F_FS_DTHRCTL_TXTHRLEN_4              0x00000040U            /*!<Bit 4 */
#define STM32F_FS_DTHRCTL_TXTHRLEN_5              0x00000080U            /*!<Bit 5 */
#define STM32F_FS_DTHRCTL_TXTHRLEN_6              0x00000100U            /*!<Bit 6 */
#define STM32F_FS_DTHRCTL_TXTHRLEN_7              0x00000200U            /*!<Bit 7 */
#define STM32F_FS_DTHRCTL_TXTHRLEN_8              0x00000400U            /*!<Bit 8 */
#define STM32F_FS_DTHRCTL_RXTHREN                 0x00010000U            /*!< Receive threshold enable */

#define STM32F_FS_DTHRCTL_RXTHRLEN                0x03FE0000U            /*!< Receive threshold length */
#define STM32F_FS_DTHRCTL_RXTHRLEN_0              0x00020000U            /*!<Bit 0 */
#define STM32F_FS_DTHRCTL_RXTHRLEN_1              0x00040000U            /*!<Bit 1 */
#define STM32F_FS_DTHRCTL_RXTHRLEN_2              0x00080000U            /*!<Bit 2 */
#define STM32F_FS_DTHRCTL_RXTHRLEN_3              0x00100000U            /*!<Bit 3 */
#define STM32F_FS_DTHRCTL_RXTHRLEN_4              0x00200000U            /*!<Bit 4 */
#define STM32F_FS_DTHRCTL_RXTHRLEN_5              0x00400000U            /*!<Bit 5 */
#define STM32F_FS_DTHRCTL_RXTHRLEN_6              0x00800000U            /*!<Bit 6 */
#define STM32F_FS_DTHRCTL_RXTHRLEN_7              0x01000000U            /*!<Bit 7 */
#define STM32F_FS_DTHRCTL_RXTHRLEN_8              0x02000000U            /*!<Bit 8 */
#define STM32F_FS_DTHRCTL_ARPEN                   0x08000000U            /*!< Arbiter parking enable */


/*
*********************************************************************************************************
*                                           LOCAL CONSTANTS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                            LOCAL MACROS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                          LOCAL DATA TYPES
*********************************************************************************************************
*/

typedef  struct  usbd_stm32f_fs_in_ep_reg {                     /* -------------- IN-ENDPOINT REGISTERS --------------- */
    CPU_REG32                  CTLx;                            /* Device control IN endpoint-x                         */
    CPU_REG32                  RSVD0;
    CPU_REG32                  INTx;                            /* Device IN endpoint-x interrupt                       */
    CPU_REG32                  RSVD1;
    CPU_REG32                  TSIZx;                           /* Device IN endpoint-x transfer size                   */
    CPU_REG32                  DMAx;                            /* OTG_HS device IN endpoint-x DMA address register     */
    CPU_REG32                  DTXFSTSx;                        /* Device IN endpoint-x transmit FIFO status            */
    CPU_REG32                  RSVD2;
} USBD_STM32F_FS_IN_EP_REG;


typedef  struct  usbd_stm32f_fs_out_ep_reg {                    /* ------------- OUT ENDPOINT REGISTERS --------------- */
    CPU_REG32                  CTLx;                            /* Device control OUT endpoint-x                        */
    CPU_REG32                  RSVD0;
    CPU_REG32                  INTx;                            /* Device OUT endpoint-x interrupt                      */
    CPU_REG32                  RSVD1;
    CPU_REG32                  TSIZx;                           /* Device OUT endpoint-x transfer size                  */
    CPU_REG32                  DMAx;                            /* OTG_HS device OUT endpoint-x DMA address register    */
    CPU_REG32                  RSVD2[2u];
} USBD_STM32F_FS_OUT_EP_REG;

typedef  struct  usbd_stm32f_fs_dfifo_reg {                     /* ---------- DATA FIFO ACCESS REGISTERS -------------- */
    CPU_REG32                  DATA[STM32F_FS_DFIFO_SIZE];      /* 4K bytes per endpoint                                */
} USBD_STM32F_FS_DFIFO_REG;

typedef  struct  usbd_stm32f_fs_reg {
                                                                /* ----- CORE GLOBAL CONTROL AND STATUS REGISTERS ----- */
    CPU_REG32                  GOTGCTL;                         /* Core control and status                              */
    CPU_REG32                  GOTGINT;                         /* Core interrupt                                       */
    CPU_REG32                  GAHBCFG;                         /* Core AHB configuration                               */
    CPU_REG32                  GUSBCFG;                         /* Core USB configuration                               */
    CPU_REG32                  GRSTCTL;                         /* Core reset                                           */
    CPU_REG32                  GINTSTS;                         /* Core interrupt                                       */
    CPU_REG32                  GINTMSK;                         /* Core interrupt mask                                  */
    CPU_REG32                  GRXSTSR;                         /* Core receive status debug read                       */
    CPU_REG32                  GRXSTSP;                         /* Core status read and pop                             */
    CPU_REG32                  GRXFSIZ;                         /* Core receive FIFO size                               */
    CPU_REG32                  DIEPTXF0;                        /* Endpoint 0 transmit FIFO size                        */
    CPU_REG32                  HNPTXSTS;                        /* Core Non Periodic Tx FIFO/Queue status               */
    CPU_REG32                  GI2CCTL;                         /* OTG_HS I2C access register                           */
    CPU_REG32                  RSVD0;
    CPU_REG32                  GCCFG;                           /* General core configuration                           */
    CPU_REG32                  CID;                             /* Core ID register                                     */

    CPU_REG32                  RSVD1[48u];

    CPU_REG32                  HPTXFSIZ;                        /* Core Host Periodic Tx FIFO size                      */
    CPU_REG32                  DIEPTXFx[STM32F_FS_MAX_NBR_EPS]; /* Device IN endpoint transmit FIFO size                */

    CPU_REG32                  RSVD2[432u];
                                                                /* ----- DEVICE MODE CONTROL AND STATUS REGISTERS ----- */
    CPU_REG32                  DCFG;                            /* Device configuration                                 */
    CPU_REG32                  DCTL;                            /* Device control                                       */
    CPU_REG32                  DSTS;                            /* Device status                                        */
    CPU_REG32                  RSVD3;
    CPU_REG32                  DIEPMSK;                         /* Device IN endpoint common interrupt mask             */
    CPU_REG32                  DOEPMSK;                         /* Device OUT endpoint common interrupt mask            */
    CPU_REG32                  DAINT;                           /* Device All endpoints interrupt                       */
    CPU_REG32                  DAINTMSK;                        /* Device All endpoints interrupt mask                  */

    CPU_REG32                  RSVD4[2u];

    CPU_REG32                  DVBUSDIS;                        /* Device VBUS discharge time                           */
    CPU_REG32                  DVBUSPULSE;                      /* Device VBUS pulsing time                             */

    CPU_REG32                  DTHRCTL;							/*!< dev threshold                830h */

    CPU_REG32                  DIEPEMPMSK;                      /* Device IN ep FIFO empty interrupt mask               */

    CPU_REG32                  EACHHINT;                        /* OTG_HS device each endpoint interrupt register       */
    CPU_REG32                  EACHHINTMSK;                     /* OTG_HS device each endpoint interrupt register mask  */
    CPU_REG32                  DIEPEACHMSK1;                    /* OTG_HS device each IN endpoint-1 interrupt register  */
    CPU_REG32                  RSVD6[15u];
    CPU_REG32                  DOEPEACHMSK1;                    /* OTG_HS device each OUT endpoint-1 interrupt register */

    CPU_REG32                  RSVD7[31u];

    USBD_STM32F_FS_IN_EP_REG   DIEP[STM32F_FS_MAX_NBR_EPS];         /* Device IN EP registers                               */
    CPU_REG32					RSVD7_1[8];			// Filler so that out ep will start at 0xb00
    USBD_STM32F_FS_OUT_EP_REG  DOEP[STM32F_FS_MAX_NBR_EPS];         /* Device OUT EP registers                              */
    CPU_REG32					RSVD7_2[8];			// Filler so that DFIFO will start at 0x1000

    CPU_REG32                  RSVD8[64u];

    CPU_REG32                  PCGCR;                           /* Power and clock gating control                       */

    CPU_REG32                  RSVD9[127u];

    USBD_STM32F_FS_DFIFO_REG   DFIFO[STM32F_FS_MAX_NBR_EPS];        /* Data FIFO access registers                           */
} USBD_STM32F_FS_REG;


/*
*********************************************************************************************************
*                                          DRIVER DATA TYPE
*********************************************************************************************************
*/

typedef  struct  usbd_drv_data_ep {                             /* ---------- DEVICE ENDPOINT DATA STRUCTURE ---------- */
    CPU_INT32U   DataBuf[STM32F_FS_MAX_PKT_SIZE / 4u];          /* Drv internal aligned buffer.                         */
    CPU_INT16U   EP_MaxPktSize[STM32F_FS_NBR_CHANNEL];          /* Max pkt size of opened EPs.                          */
    CPU_INT16U   EP_PktXferLen[STM32F_FS_NBR_CHANNEL];          /* EPs current xfer len.                                */
    CPU_INT08U  *EP_AppBufPtr[STM32F_FS_NBR_CHANNEL];           /* Ptr to app buffer.                                   */
    CPU_INT16U   EP_AppBufLen[STM32F_FS_NBR_CHANNEL];           /* Len of app buffer.                                   */
    CPU_INT16U   EP_xfer_count[STM32F_FS_NBR_CHANNEL];     		/*!< Partial transfer length in case of multi packet transfer   */
    CPU_INT32U   EP_SetupBuf[2u];                               /* Buffer that contains setup pkt.                      */
} USBD_DRV_DATA_EP;


/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                             USB DEVICE CONTROLLER DRIVER API PROTOTYPES
*********************************************************************************************************
*/

                                                                /* ------------ FULL-SPEED MODE DRIVER API ------------ */
static  void         USBD_DrvInit            (USBD_DRV     *p_drv,
                                              USBD_ERR     *p_err);

static  void         USBD_DrvStart           (USBD_DRV     *p_drv,
                                              USBD_ERR     *p_err);

static  void         USBD_DrvStop            (USBD_DRV     *p_drv);

static  CPU_BOOLEAN  USBD_DrvAddrSet         (USBD_DRV     *p_drv,
                                              CPU_INT08U    dev_addr);

static  CPU_INT16U   USBD_DrvFrameNbrGet     (USBD_DRV     *p_drv);

static  void         USBD_DrvEP_Open         (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr,
                                              CPU_INT08U    ep_type,
                                              CPU_INT16U    max_pkt_size,
                                              CPU_INT08U    transaction_frame,
                                              USBD_ERR     *p_err);

static  void         USBD_DrvEP_Close        (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr);

static  CPU_INT32U   USBD_DrvEP_RxStart      (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr,
                                              CPU_INT08U   *p_buf,
                                              CPU_INT32U    buf_len,
                                              USBD_ERR     *p_err);

static  CPU_INT32U   USBD_DrvEP_Rx           (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr,
                                              CPU_INT08U   *p_buf,
                                              CPU_INT32U    buf_len,
                                              USBD_ERR     *p_err);

static  void         USBD_DrvEP_RxZLP        (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr,
                                              USBD_ERR     *p_err);

static  CPU_INT32U   USBD_DrvEP_Tx           (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr,
                                              CPU_INT08U   *p_buf,
                                              CPU_INT32U    buf_len,
                                              USBD_ERR     *p_err);

static  void         USBD_DrvEP_TxStart      (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr,
                                              CPU_INT08U   *p_buf,
                                              CPU_INT32U    buf_len,
                                              USBD_ERR     *p_err);


static  void         USBD_DrvEP_TxZLP        (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr,
                                              USBD_ERR     *p_err);

static  CPU_BOOLEAN  USBD_DrvEP_Abort        (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr);

static  CPU_BOOLEAN  USBD_DrvEP_Stall        (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_addr,
                                              CPU_BOOLEAN   state);

static  void         USBD_DrvISR_Handler     (USBD_DRV     *p_drv);


/*
*********************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void         STM32_RxFIFO_Rd         (USBD_DRV     *p_drv);

static  void         STM32_TxFIFO_Wr         (USBD_DRV     *p_drv,
                                              CPU_INT08U    ep_log_nbr,
                                              CPU_INT08U   *p_buf,
                                              CPU_INT16U    ep_pkt_len);

static  void         STM32_EP_OutProcess     (USBD_DRV     *p_drv);

static  void         STM32_EP_InProcess      (USBD_DRV     *p_drv);

static void 		STM32_USBD_LL_Reset		(USBD_DRV     *p_drv);
static  CPU_BOOLEAN  STM32_TxFifoFlush 		(USBD_STM32F_FS_REG  *p_reg, CPU_INT32U num );
static  CPU_BOOLEAN  STM32_RxFifoFlush		(USBD_STM32F_FS_REG* p_reg);

static  CPU_BOOLEAN  STM32_EP0_OutStart		(USBD_STM32F_FS_REG* p_reg, CPU_BOOLEAN dma, CPU_INT32U* psetup);
static CPU_BOOLEAN 	STM32_ActivateSetup 	(USBD_STM32F_FS_REG *p_reg);

static CPU_BOOLEAN  STM32_SetRxFiFo			(USBD_STM32F_FS_REG *p_reg, CPU_INT16U size);
static CPU_BOOLEAN 	STM32_SetTxFiFo			(USBD_STM32F_FS_REG *p_reg, CPU_INT08U fifo, CPU_INT16U size);
/*
*********************************************************************************************************
*                                     LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                  USB DEVICE CONTROLLER DRIVER API
*********************************************************************************************************
*/

USBD_DRV_API  USBD_DrvAPI_STM32F_FS = { USBD_DrvInit,
                                        USBD_DrvStart,
                                        USBD_DrvStop,
                                        USBD_DrvAddrSet,
                                        DEF_NULL,
                                        DEF_NULL,
                                        DEF_NULL,
                                        USBD_DrvFrameNbrGet,
                                        USBD_DrvEP_Open,
                                        USBD_DrvEP_Close,
                                        USBD_DrvEP_RxStart,
                                        USBD_DrvEP_Rx,
                                        USBD_DrvEP_RxZLP,
                                        USBD_DrvEP_Tx,
                                        USBD_DrvEP_TxStart,
                                        USBD_DrvEP_TxZLP,
                                        USBD_DrvEP_Abort,
                                        USBD_DrvEP_Stall,
                                        USBD_DrvISR_Handler,
                                      };


/*
*********************************************************************************************************
*********************************************************************************************************
*                                     DRIVER INTERFACE FUNCTIONS
*********************************************************************************************************
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                           USBD_DrvInit()
*
* Description : Initialize the Full-speed device.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               p_err       Pointer to variable that will receive the return error code from this function :
*
*                               USBD_ERR_NONE       Device successfully initialized.
*                               USBD_ERR_ALLOC      Memory allocation failed.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_DevInit() via 'p_drv_api->Init()'.
*
* Note(s)     : (1) Since the CPU frequency could be higher than OTG module clock, a timeout is needed
*                   to reset the OTG controller successfully.
*********************************************************************************************************
*/

static  void  USBD_DrvInit (USBD_DRV  *p_drv,
                            USBD_ERR  *p_err)
{
    CPU_INT08U           ep_nbr;
    CPU_INT32U           reg_to;
    CPU_REG32            ctrl_reg;
    CPU_SIZE_T           reqd_octets;
    LIB_ERR              lib_mem_err;
    USBD_STM32F_FS_REG  *p_reg;
    USBD_DRV_BSP_API    *p_bsp_api;
    USBD_DRV_DATA_EP    *p_drv_data;

    int i;

//    CPU_REG32 stam = STM32F_FS_DIEPTSIZx_PKTCNT_MSK;
                                                                /* Alloc drv internal data.                             */
    p_drv_data = (USBD_DRV_DATA_EP *)Mem_HeapAlloc(
    											sizeof(USBD_DRV_DATA_EP),
                                                sizeof(CPU_ALIGN),
                                                &reqd_octets,
                                                &lib_mem_err);
    if (p_drv_data == (void *)0)
    {
       *p_err = USBD_ERR_ALLOC;
        return;
    }

    Mem_Clr(p_drv_data, sizeof(USBD_DRV_DATA_EP));

    p_drv->DataPtr = p_drv_data;                                /* Store drv internal data ptr.                         */

    p_bsp_api =  p_drv->BSP_API_Ptr;                            /* Get driver BSP API reference.                        */
    p_reg     = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;  /* Get USB ctrl reg ref.                                */

    if (p_bsp_api->Init != (void *)0) {
        p_bsp_api->Init(p_drv);                                 /* Call board/chip specific device controller ...       */
                                                                /* ... initialization function.                         */
    }


    /* Disable the global interrupt                         */
    DEF_BIT_CLR(p_reg->GAHBCFG, STM32F_FS_GAHBCFG_BIT_GINTMSK);

                                                                /* --------------------- PHY CFG ---------------------- */
    DEF_BIT_SET(p_reg->GUSBCFG, STM32F_FS_GUSBCFG_BIT_PHYSEL);  /* Full Speed serial transceivers                       */

//                                                                /* -------------------- CORE RESET -------------------- */
	reg_to = STM32F_FS_REG_VAL_TO;                              /* Check AHB master state machine is in IDLE condition  */
	while ((DEF_BIT_IS_CLR(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_AHBIDL)) && (reg_to > 0u))
	{
		reg_to--;
	}

	DEF_BIT_SET(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_CSRST);   /* Resets the HCLK and PCLK domains                     */

//    p_reg->GCCFG = STM32F_FS_GCCFG_BIT_PWRDWN  |
//                   STM32F_FS_GCCFG_BIT_VBUSAEN |                /* Vbus sensing B and A device enable                   */
//                   STM32F_FS_GCCFG_BIT_VBUSBEN;                 /* Transceiver active                                   */
//

//    DEF_BIT_SET(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_HSRST);   /* Clear the control logic in the AHB Clock domain      */
    reg_to = STM32F_FS_REG_VAL_TO;                              /* Check all necessary logic is reset in the core       */
    while ((DEF_BIT_IS_SET(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_CSRST)) && (reg_to > 0u))
    {
        reg_to--;
    }

    /* Deactivate the power down*/
    p_reg->GCCFG = STM32F_FS_GCCFG_BIT_PWRDWN;

    // Enable DMA
     if (STM32F_FS_USE_DMA == DEF_ENABLED)
     {
     	p_reg->GAHBCFG |= STM32F_FS_GAHBCFG_HBSTLEN_INCR4;
        p_reg->GAHBCFG |= STM32F_FS_GAHBCFG_BIT_DMAEN;
     }



       p_reg->GUSBCFG &= ~(STM32F_FS_GUSBCFG_BIT_FHMOD | STM32F_FS_GUSBCFG_BIT_FDMOD);

        DEF_BIT_SET(p_reg->GUSBCFG, STM32F_FS_GUSBCFG_BIT_FDMOD);   /* Force the core to device mode                        */

        HAL_Delay(50U);												 /* Wait at least 50ms before the change takes effect    */
//        reg_to = STM32F_FS_REG_FMOD_TO;                             /* Wait at least 25ms before the change takes effect    */
//        while (reg_to > 0u)
//        {
//            reg_to--;
//        }



     															/* Init endpoints TX fifo */
     for (i=0; i<STM32F_FS_MAX_NBR_EPS; i++)
     {
     	p_reg->DIEPTXFx[i] = 0U;
     }

     /* ------------------- DEVICE INIT -------------------- */
     p_reg->GCCFG |= STM32F_FS_GCCFG_BIT_VBDEN;
   	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 /* Deactivate VBUS Sensing B */
    p_reg->GCCFG &= ~STM32F_FS_GCCFG_BIT_VBDEN;

     /* B-peripheral session valid override enable*/
    p_reg->GOTGCTL |= STM32F_FS_GOTGCTL_BVALOEN;
    p_reg->GOTGCTL |= STM32F_FS_GOTGCTL_BVALOVAL;

    //Zeev check its the same &USBx_PCGCCTL	volatile uint32_t *	0x50000e00

    p_reg->PCGCR = DEF_BIT_NONE;                                /* Reset the PHY clock                                  */

    DEF_BIT_SET(p_reg->DCFG, STM32F_FS_DCFG_PFIVL_80);
    DEF_BIT_SET(p_reg->DCFG, STM32F_FS_DCFG_DSPD_FULLSPEED);

//    // Special addition
//     p_reg->DCFG |=0X00004000;  //Bit 14 XCVRDLY: Transceiver delay

	/* Flush all TX FIFO                               */
    STM32_TxFifoFlush(p_reg, 0x10U);
	/* Flush the receive FIFO                               */
    STM32_RxFifoFlush(p_reg);

//    p_reg->GRSTCTL = STM32F_FS_GRSTCTL_BIT_TXFFLSH |            /* Flush all transmit FIFOs                             */
//                     STM32F_FS_GRSTCTL_FLUSH_TXFIFO_ALL;
//
//    reg_to = STM32F_FS_REG_VAL_TO;                              /* Wait for the flush completion                        */
//    while ((DEF_BIT_IS_SET(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_TXFFLSH)) &&
//           (reg_to > 0u)) {
//        reg_to--;
//    }
//
//                                                                /* Flush the receive FIFO                               */
//    DEF_BIT_SET(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_RXFFLSH);
//
//    reg_to = STM32F_FS_REG_VAL_TO;                              /* Wait for the flush completion                        */
//    while ((DEF_BIT_IS_SET(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_RXFFLSH)) &&
//           (reg_to > 0u)) {
//        reg_to--;
//    }
                                                                /* Clear all pending Device interrupts                  */
    p_reg->DIEPMSK  = DEF_BIT_NONE;                             /* Dis. interrupts for the Device IN Endpoints          */
    p_reg->DOEPMSK  = DEF_BIT_NONE;                             /* Dis. interrupts for the Device OUT Endpoints         */
    p_reg->DAINT = 0xFFFFFFFFU;
    p_reg->DAINTMSK = DEF_BIT_NONE;                             /* Dis. interrupts foo all Device Endpoints             */

    for (ep_nbr = 0u; ep_nbr < STM32F_FS_NBR_EPS; ep_nbr++)
    {
                                                                /* ----------------- IN ENDPOINT RESET ---------------- */
        ctrl_reg = p_reg->DIEP[ep_nbr].CTLx;
        if (DEF_BIT_IS_SET(ctrl_reg, STM32F_FS_DxEPCTLx_BIT_EPENA))
        {
 //           ctrl_reg = DEF_BIT_NONE;
            ctrl_reg = STM32F_FS_DxEPCTLx_BIT_EPDIS | STM32F_FS_DxEPCTLx_BIT_SNAK;
        }
        else
        {
// Zeev check the address        	&(USBx_INEP(i)->DIEPCTL)	volatile uint32_t *	0x50000900, 920

            ctrl_reg = DEF_BIT_NONE;
        }

        p_reg->DIEP[ep_nbr].CTLx  = ctrl_reg;
        p_reg->DIEP[ep_nbr].TSIZx = DEF_BIT_NONE;
        p_reg->DIEP[ep_nbr].INTx  = 0x000000FFu;                /* Clear any pending Interrupt                          */
    }

    for (ep_nbr = 0u; ep_nbr < STM32F_FS_NBR_EPS; ep_nbr++)
    {
                                                                /* ---------------- OUT ENDPOINT RESET ---------------- */
        ctrl_reg = p_reg->DOEP[ep_nbr].CTLx;
        if (DEF_BIT_IS_SET(ctrl_reg, STM32F_FS_DxEPCTLx_BIT_EPENA))
        {
//            ctrl_reg = DEF_BIT_NONE;
            ctrl_reg = STM32F_FS_DxEPCTLx_BIT_EPDIS | STM32F_FS_DxEPCTLx_BIT_SNAK;
        }
        else
        {
            ctrl_reg = DEF_BIT_NONE;
        }

        p_reg->DOEP[ep_nbr].CTLx  = ctrl_reg;
        p_reg->DOEP[ep_nbr].TSIZx = DEF_BIT_NONE;
        p_reg->DOEP[ep_nbr].INTx  = 0x000000FFu;                /* Clear any pending Interrupt                          */
    }

    p_reg->DIEPMSK &= ~(STM32F_FS_DIEPMSK_BIT_TXFURM);

    /* Disable the global interrupt                         */
//    DEF_BIT_CLR(p_reg->GAHBCFG, STM32F_FS_GAHBCFG_BIT_GINTMSK);

     if (STM32F_FS_USE_DMA == DEF_ENABLED)
     {
 		/*Set threshold parameters */
 		p_reg->DTHRCTL = (STM32F_FS_DTHRCTL_TXTHRLEN_6 | STM32F_FS_DTHRCTL_RXTHRLEN_6);
 		p_reg->DTHRCTL |= (STM32F_FS_DTHRCTL_RXTHREN | STM32F_FS_DTHRCTL_ISOTHREN | STM32F_FS_DTHRCTL_NONISOTHREN);
 		CPU_INT32U temp = p_reg->DTHRCTL;
     }

    p_reg->GINTMSK = DEF_BIT_NONE;                              /* Disable all interrupts                               */

    /* Clear any pending interrupts */
    p_reg->GINTSTS = 0xBFFFFFFFU;

    /* Enable the common interrupts */
    if (STM32F_FS_USE_DMA == DEF_DISABLED)
    {
    	p_reg->GINTMSK |= STM32F_FS_GINTMSK_BIT_RXFLVLM;
    }

    /* Enable interrupts                                    */
	p_reg->GINTMSK |= STM32F_FS_GINTMSK_BIT_USBSUSPM |
					STM32F_FS_GINTMSK_BIT_USBRST   |
					STM32F_FS_GINTMSK_BIT_ENUMDNEM |
					STM32F_FS_GINTMSK_BIT_WUIM		|
					STM32F_FS_GINTMSK_BIT_IEPINT   |
					STM32F_FS_GINTMSK_BIT_OEPINT	|
					STM32F_FS_GINTMSK_BIT_IISOIXFRM|
					STM32F_FS_GINTMSK_BIT_IISOOXFRM ;

#if 0
    DEF_BIT_CLR(p_reg->DCFG, STM32F_FS_DCFG_DAD_MASK);          /* Set Device Address to zero                           */

    p_reg->GRXFSIZ     =  STM32F_FS_RXFIFO_SIZE;            /* Set Rx FIFO depth                                    */
                                                              /* Set EP0 to EP3 Tx FIFO depth                         */
     p_reg->DIEPTXF0    = (STM32F_FS_TXFIFO_EP0_SIZE << 16u) |  STM32F_FS_RXFIFO_SIZE;
     p_reg->DIEPTXFx[0] = (STM32F_FS_TXFIFO_EP1_SIZE << 16u) | (STM32F_FS_RXFIFO_SIZE +
                                                                 STM32F_FS_TXFIFO_EP0_SIZE);

//     p_reg->DIEPTXFx[1] = (STM32F_FS_TXFIFO_EP2_SIZE << 16u) | (STM32F_FS_RXFIFO_SIZE +
//                                                                 STM32F_FS_TXFIFO_EP0_SIZE +
//                                                                 STM32F_FS_TXFIFO_EP1_SIZE);
//     p_reg->DIEPTXFx[2] = (STM32F_FS_TXFIFO_EP3_SIZE << 16u) | (STM32F_FS_RXFIFO_SIZE +
//                                                                 STM32F_FS_TXFIFO_EP0_SIZE +
//                                                                 STM32F_FS_TXFIFO_EP1_SIZE +
//                                                                 STM32F_FS_TXFIFO_EP2_SIZE);
#endif
     p_reg->DCTL |= STM32F_FS_DCTL_BIT_SDIS ;		/* Generate Device disconnect event to the USB host        */

     reg_to = STM32F_FS_REG_FMOD_TO;                             /* Wait at least 25ms before the change takes effect    */
     while (reg_to > 0u)
     {
         reg_to--;
     }

     STM32_SetRxFiFo(p_reg, STM32F_FS_RXFIFO_SIZE);
     STM32_SetTxFiFo(p_reg, 0, STM32F_FS_TXFIFO_EP0_SIZE);
     STM32_SetTxFiFo(p_reg, 1, STM32F_FS_TXFIFO_EP1_SIZE);

//     p_reg->GUSBCFG &=  ~STM32F_FS_GUSBCFG_BIT_PHYLPCS;

   *p_err = USBD_ERR_NONE;
}


/*
*********************************************************************************************************
*                                           USBD_DrvStart()
*
* Description : Start device operation :
*
*                   (1) Enable device controller bus state interrupts.
*                   (2) Call board/chip specific connect function.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               p_err       Pointer to variable that will receive the return error code from this function :
*
*                               USBD_ERR_NONE    Device successfully connected.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_DevStart() via 'p_drv_api->Start()'.
*
* Note(s)     : Typically, the start function activates the pull-down on the D- pin to simulate
*               attachment to host. Some MCUs/MPUs have an internal pull-down that is activated by a
*               device controller register; for others, this may be a GPIO pin. Additionally, interrupts
*               for reset and suspend are activated.
*********************************************************************************************************
*/

static  void  USBD_DrvStart (USBD_DRV  *p_drv,
                             USBD_ERR  *p_err)
{
    USBD_DRV_BSP_API    *p_bsp_api;
    USBD_STM32F_FS_REG  *p_reg;


    p_bsp_api =  p_drv->BSP_API_Ptr;                            /* Get driver BSP API reference.                        */
    p_reg     = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;  /* Get USB ctrl reg ref.                                */
#if 0
    p_reg->GINTSTS = 0xFFFFFFFFu;                               /* Clear any pending interrupt                          */
                                                                /* Enable interrupts                                    */
//    STM32F_FS_GINTMSK_BIT_OTGINT   |
//                     STM32F_FS_GINTMSK_BIT_SRQIM    |
    p_reg->GINTMSK |= STM32F_FS_GINTMSK_BIT_USBSUSPM |
                     STM32F_FS_GINTMSK_BIT_USBRST   |
                     STM32F_FS_GINTMSK_BIT_ENUMDNEM |
                     STM32F_FS_GINTMSK_BIT_WUIM		|
					 STM32F_FS_GINTMSK_BIT_IEPINT   |
					 STM32F_FS_GINTMSK_BIT_OEPINT	|
					 STM32F_FS_GINTMSK_BIT_IISOIXFRM|
					 STM32F_FS_GINTMSK_BIT_IISOOXFRM ;
#endif
    if (p_bsp_api->Conn != (void *)0) {
        p_bsp_api->Conn();                                      /* Call board/chip specific connect function.           */
    }

    p_reg->DCTL &= ~STM32F_FS_DCTL_BIT_SDIS ;	         /* Generate Device connect event to the USB host        */

    HAL_Delay(3U);

    // because we dont have OTG we have t intiate the process ourselves
    USBD_EventConn(p_drv);                                  /* Notify connect event.                                */

//   CPU_REG32 delay = 50000u;                             /* Wait at least 2ms before the change takes effect    */
//   while (delay > 0u)
//   {
//	delay--;
//   }

   /* Enable Global Interrupt                              */
   DEF_BIT_SET(p_reg->GAHBCFG, STM32F_FS_GAHBCFG_BIT_GINTMSK);

   // ***************************************************************************
   // Emulate a wakeup interrupt
//   DEF_BIT_CLR(p_reg->PCGCR, (STM32F_FS_PCGCCTL_BIT_GATEHCLK |
//                              STM32F_FS_PCGCCTL_BIT_STPPCLK));
//
//   DEF_BIT_CLR(p_reg->DCTL, STM32F_FS_DCTL_BIT_RWUSIG);    /* Clear Remote wakeup signaling                        */
//                                                           /* Clear Remote wakeup interrupt                        */

   // ***************************************************************************

   // Generate Session request
//   DEF_BIT_SET(p_reg->GOTGCTL, STM32F_FS_GOTGCTL_BIT_SRQ);

   *p_err = USBD_ERR_NONE;
}


/*
*********************************************************************************************************
*                                           USBD_DrvStop()
*
* Description : Stop Full-speed device operation.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_DevStop() via 'p_drv_api->Stop()'.
*
* Note(s)     : Typically, the stop function performs the following operations:
*               (1) Clear and disable USB interrupts.
*               (2) Disconnect from the USB host (e.g, reset the pull-down on the D- pin).
*********************************************************************************************************
*/

static  void  USBD_DrvStop (USBD_DRV  *p_drv)
{
    USBD_DRV_BSP_API    *p_bsp_api;
    USBD_STM32F_FS_REG  *p_reg;


    p_bsp_api =  p_drv->BSP_API_Ptr;                            /* Get driver BSP API reference.                        */
    p_reg     = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;  /* Get USB ctrl reg ref.                                */

    if (p_bsp_api->Disconn != (void *)0) {
        p_bsp_api->Disconn();
    }

    p_reg->GINTMSK = DEF_BIT_NONE;                              /* Disable all interrupts                               */
    p_reg->GINTSTS = 0xFFFFFFFF;                                /* Clear any pending interrupt                          */
                                                                /* Disable the global interrupt                         */
    DEF_BIT_CLR(p_reg->GAHBCFG, STM32F_FS_GAHBCFG_BIT_GINTMSK);

    DEF_BIT_SET(p_reg->DCTL, STM32F_FS_DCTL_BIT_SDIS);          /* Generate Device Disconnect event to the USB host     */
}


/*
*********************************************************************************************************
*                                          USBD_DrvAddrSet()
*
* Description : Assign an address to device.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               dev_addr    Device address assigned by the host.
*
* Return(s)   : DEF_OK,   if NO error(s).
*
*               DEF_FAIL, otherwise.
*
* Caller(s)   : USBD_StdReqDev() via 'p_drv_api->AddrSet()'.
*
* Note(s)     : (1) For device controllers that have hardware assistance to enable the device address after
*                   the status stage has completed, the assignment of the device address can also be
*                   combined with enabling the device address mode.
*
*               (2) For device controllers that change the device address immediately, without waiting the
*                   status phase to complete, see USBD_DrvAddrEn().
*********************************************************************************************************
*/

static  CPU_BOOLEAN  USBD_DrvAddrSet (USBD_DRV    *p_drv,
                                      CPU_INT08U   dev_addr)
{
    USBD_STM32F_FS_REG  *p_reg;


    p_reg = (USBD_STM32F_FS_REG *)(p_drv->CfgPtr->BaseAddr);

    DEF_BIT_CLR(p_reg->DCFG, STM32F_FS_DCFG_DAD_MASK);
    p_reg->DCFG |= (dev_addr << 4u);                            /* Set Device Address                                   */

    return (DEF_OK);
}


/*
*********************************************************************************************************
*                                        USBD_DrvFrameNbrGet()
*
* Description : Retrieve current frame number.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
* Return(s)   : Frame number.
*
* Caller(s)   : USBD_EP_Open() via 'p_drv_api->GetFrameNbr()'.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  CPU_INT16U  USBD_DrvFrameNbrGet (USBD_DRV  *p_drv)
{
    CPU_INT16U           frame_nbr;
    USBD_STM32F_FS_REG  *p_reg;


    p_reg     = (USBD_STM32F_FS_REG *)(p_drv->CfgPtr->BaseAddr);
    frame_nbr = (p_reg->DSTS & STM32F_FS_DSTS_FNSOF_MASK) >> 8u;

    return (frame_nbr);
}


/*
*********************************************************************************************************
*                                          USBD_DrvEP_Open()
*
* Description : Open and configure a device endpoint, given its characteristics (e.g., endpoint type,
*               endpoint address, maximum packet size, etc).
*
* Argument(s) : p_drv               Pointer to device driver structure.
*
*               ep_addr             Endpoint address.
*
*               ep_type             Endpoint type :
*
*                                       USBD_EP_TYPE_CTRL,
*                                       USBD_EP_TYPE_ISOC,
*                                       USBD_EP_TYPE_BULK,
*                                       USBD_EP_TYPE_INTR.
*
*               max_pkt_size        Maximum packet size.
*
*               transaction_frame   Endpoint transactions per frame.
*
*               p_err       Pointer to variable that will receive the return error code from this function :
*
*                               USBD_ERR_NONE               Endpoint successfully opened.
*                               USBD_ERR_EP_INVALID_ADDR    Invalid endpoint address.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_EP_Open() via 'p_drv_api->EP_Open()',
*               USBD_CtrlOpen().
*
* Note(s)     : (1) Typically, the endpoint open function performs the following operations:
*
*                   (a) Validate endpoint address, type and maximum packet size.
*                   (b) Configure endpoint information in the device controller. This may include not
*                       only assigning the type and maximum packet size, but also making certain that
*                       the endpoint is successfully configured (or  realized  or  mapped ). For some
*                       device controllers, this may not be necessary.
*
*               (2) If the endpoint address is valid, then the endpoint open function should validate
*                   the attributes allowed by the hardware endpoint :
*
*                   (a) The maximum packet size 'max_pkt_size' should be validated to match hardware
*                       capabilities.
*********************************************************************************************************
*/

static  void  USBD_DrvEP_Open (USBD_DRV    *p_drv,
                               CPU_INT08U   ep_addr,
                               CPU_INT08U   ep_type,
                               CPU_INT16U   max_pkt_size,
                               CPU_INT08U   transaction_frame,
                               USBD_ERR    *p_err)
{

    CPU_INT08U           ep_log_nbr;
    CPU_INT08U           ep_phy_nbr;
    CPU_INT32U           reg_val;
    USBD_DRV_DATA_EP    *p_drv_data;
    USBD_STM32F_FS_REG  *p_reg;


    (void)&transaction_frame;

    p_reg      = (USBD_STM32F_FS_REG *)(p_drv->CfgPtr->BaseAddr);
    p_drv_data = (USBD_DRV_DATA_EP   *)(p_drv->DataPtr);
    ep_log_nbr =  USBD_EP_ADDR_TO_LOG(ep_addr);
    ep_phy_nbr =  USBD_EP_ADDR_TO_PHY(ep_addr);
    reg_val    =  0u;

    if (STM32F_FS_USE_DMA == DEF_DISABLED)
     {
     	USBD_DBG_DRV_EP("  Drv EP FIFO Open %d", ep_addr);
     }
     else
     {
     	USBD_DBG_DRV_EP("  Drv EP DMA Open %d", ep_addr);
     }

    switch (ep_type) {
        case USBD_EP_TYPE_CTRL:
             if (USBD_EP_IS_IN(ep_addr) == DEF_YES) {
                                                                /* En. Device EP0 IN interrupt                          */
                 DEF_BIT_SET(p_reg->DAINTMSK, STM32F_FS_DAINTMSK_BIT_IEPINT_EP0);
                                                                /* En. common IN EP Transfer complete interrupt         */
                 DEF_BIT_SET(p_reg->DIEPMSK, STM32F_FS_DIEPMSK_BIT_XFRCM);

             } else {
                                                                /* En. EP0 OUT interrupt                                */
                 DEF_BIT_SET(p_reg->DAINTMSK, STM32F_FS_DAINTMSK_BIT_OEPINT_EP0);
                                                                /* En. common OUT EP Setup $ Xfer complete interrupt    */
                 DEF_BIT_SET(p_reg->DOEPMSK, (STM32F_FS_DOEPMSK_BIT_STUPM | STM32F_FS_DOEPMSK_BIT_XFRCM));
                 p_reg->DOEP[ep_log_nbr].TSIZx = (STM32F_FS_DOEPTSIZx_STUPCNT_3_PKT |
                                                  STM32F_FS_DOEPTSIZ0_BIT_PKTCNT    |
                                                  STM32F_FS_DOEPTSIZ0_XFRSIZ_MAX_64);

                 DEF_BIT_SET(p_reg->DOEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_SNAK);
             }

             break;



        case USBD_EP_TYPE_INTR:
        case USBD_EP_TYPE_BULK:
             if (USBD_EP_IS_IN(ep_addr)) {
                 reg_val = (max_pkt_size                 |      /* cfg EP max packet size                               */
                           (ep_type    << 18u)           |      /* cfg EP type                                          */
                           (ep_log_nbr << 22u)           |      /* Tx FIFO number                                       */
                           STM32F_FS_DxEPCTLx_BIT_SD0PID |      /* EP start data toggle                                 */
                           STM32F_FS_DxEPCTLx_BIT_USBAEP);      /* USB active EP                                        */

                 p_reg->DIEP[ep_log_nbr].CTLx = reg_val;
                                                                /* En. Device EPx IN Interrupt                          */
                 DEF_BIT_SET(p_reg->DAINTMSK, DEF_BIT(ep_log_nbr));
             } else {
                 reg_val = (max_pkt_size                 |      /* cfg EP max packet size                               */
                           (ep_type << 18u)              |      /* cfg EP type                                          */
                           STM32F_FS_DxEPCTLx_BIT_SD0PID |      /* EP start data toggle                                 */
                           STM32F_FS_DxEPCTLx_BIT_USBAEP);      /* USB active EP                                        */

                 p_reg->DOEP[ep_log_nbr].CTLx = reg_val;

                 reg_val = DEF_BIT(ep_log_nbr) << 16u;
                 DEF_BIT_SET(p_reg->DAINTMSK, reg_val);         /* En. Device EPx OUT Interrupt                         */
             }
             break;


        case USBD_EP_TYPE_ISOC:
        default:
            *p_err = USBD_ERR_INVALID_ARG;
             return;
    }

    p_drv_data->EP_MaxPktSize[ep_phy_nbr] = max_pkt_size;

   *p_err = USBD_ERR_NONE;
}


/*
*********************************************************************************************************
*                                         USBD_DrvEP_Close()
*
* Description : Close a device endpoint, and uninitialize/clear endpoint configuration in hardware.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               ep_addr     Endpoint address.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_EP_Close() via 'p_drv_api->EP_Close()',
*               USBD_CtrlOpen().
*
* Note(s)     : Typically, the endpoint close function clears the endpoint information in the device
*               controller. For some controllers, this may not be necessary.
*********************************************************************************************************
*/

static  void  USBD_DrvEP_Close (USBD_DRV    *p_drv,
                                CPU_INT08U   ep_addr)
{
    CPU_INT08U           ep_log_nbr;
    CPU_INT32U           reg_val;
    USBD_STM32F_FS_REG  *p_reg;


    p_reg      = (USBD_STM32F_FS_REG *)(p_drv->CfgPtr->BaseAddr);
    ep_log_nbr =  USBD_EP_ADDR_TO_LOG(ep_addr);
    reg_val    =  DEF_BIT_NONE;


    USBD_DBG_DRV_EP("  Drv EP Closed", ep_addr);

    if (USBD_EP_IS_IN(ep_addr)) {                               /* ------------------- IN ENDPOINTS ------------------- */
                                                                /* Deactive the Endpoint                                */
        DEF_BIT_CLR(p_reg->DIEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_USBAEP);
        reg_val = DEF_BIT(ep_log_nbr);
    } else {                                                    /* ------------------ OUT ENDPOINTS ------------------- */
                                                                /* Deactive the Endpoint                                */
        DEF_BIT_CLR(p_reg->DOEP[ep_log_nbr].CTLx ,STM32F_FS_DxEPCTLx_BIT_USBAEP);
        reg_val = (DEF_BIT(ep_log_nbr)  << 16u);
    }

    DEF_BIT_CLR(p_reg->DAINTMSK, reg_val);                      /* Dis. EP interrupt                                    */
}


/*
*********************************************************************************************************
*                                        USBD_DrvEP_RxStart()
*
* Description : Configure endpoint with buffer to receive data.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               ep_addr     Endpoint address.
*
*               p_buf       Pointer to data buffer.
*
*               buf_len     Length of the buffer.
*
*               p_err       Pointer to variable that will receive the return error code from this function :
*
*                               USBD_ERR_NONE   Receive successfully configured.
*
* Return(s)   : Number of bytes that can be handled in one transfer.
*
* Caller(s)   : USBD_EP_Rx() via 'p_drv_api->EP_Rx()',
*               USBD_EP_Process().
*
* Note(s)     : none.
*********************************************************************************************************
*/
static  CPU_INT32U  USBD_DrvEP0_RxStart (USBD_DRV    *p_drv,
                                        CPU_INT08U   ep_addr,
                                        CPU_INT08U  *p_buf,
                                        CPU_INT32U   buf_len,
                                        USBD_ERR    *p_err)
{

    CPU_INT08U           ep_log_nbr;
    CPU_INT08U           ep_phy_nbr;
    CPU_INT16U           ep_pkt_len;
    CPU_INT16U           pkt_cnt;
    CPU_INT32U           ctl_reg;
    CPU_INT32U           tsiz_reg;
    USBD_DRV_DATA_EP    *p_drv_data;
    USBD_STM32F_FS_REG  *p_reg;


    p_reg      = (USBD_STM32F_FS_REG *)(p_drv->CfgPtr->BaseAddr);
    p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
    ep_log_nbr =  USBD_EP_ADDR_TO_LOG(ep_addr);
    ep_phy_nbr =  USBD_EP_ADDR_TO_PHY(ep_addr);

    if (buf_len > 0)
    {
    	ep_pkt_len = p_drv_data->EP_MaxPktSize[ep_phy_nbr];
    }
    else
    {
    	ep_pkt_len = 0;
    }

    USBD_DBG_DRV_EP("  Drv EP0 FIFO RxStart", ep_addr);

    ctl_reg  = p_reg->DOEP[ep_log_nbr].CTLx;                    /* Read Control EP reg                                  */
    tsiz_reg = p_reg->DOEP[ep_log_nbr].TSIZx;                   /* Read Transer EP reg                                  */

                                                                /* Clear EP transfer size and packet count              */
    DEF_BIT_CLR(tsiz_reg, (STM32F_FS_DOEPTSIZx_XFRSIZ_MSK | STM32F_FS_DOEPTSIZx_PKTCNT_MSK));


	tsiz_reg |=  p_drv_data->EP_MaxPktSize[ep_phy_nbr];     /* Set transfer size to max pkt size                    */
	tsiz_reg |= (1u << 19u);                                /* Set packet count                                     */

    /* DMA Case 8, Checked */
    if (STM32F_FS_USE_DMA == DEF_ENABLED)
	{
		p_reg->DOEP[ep_log_nbr].DMAx = (CPU_REG32)p_buf;
	}

    p_drv_data->EP_AppBufPtr[ep_phy_nbr] = p_buf;
    p_drv_data->EP_AppBufLen[ep_phy_nbr] = buf_len;

                                                                /* Clear EP NAK and Enable EP for receiving             */
    ctl_reg |= STM32F_FS_DxEPCTLx_BIT_CNAK | STM32F_FS_DxEPCTLx_BIT_EPENA;

    p_drv_data->EP_PktXferLen[ep_phy_nbr] = ep_pkt_len;
    p_reg->DOEP[ep_log_nbr].TSIZx         = tsiz_reg;
    p_reg->DOEP[ep_log_nbr].CTLx          = ctl_reg;

   *p_err = USBD_ERR_NONE;

    return (ep_pkt_len);
}


static  CPU_INT32U  USBD_DrvEP_RxStart (USBD_DRV    *p_drv,
                                        CPU_INT08U   ep_addr,
                                        CPU_INT08U  *p_buf,
                                        CPU_INT32U   buf_len,
                                        USBD_ERR    *p_err)
{

    CPU_INT08U           ep_log_nbr;
    CPU_INT08U           ep_phy_nbr;
    CPU_INT16U           ep_pkt_len;
    CPU_INT16U           pkt_cnt;
    CPU_INT32U           ctl_reg;
    CPU_INT32U           tsiz_reg;
    USBD_DRV_DATA_EP    *p_drv_data;
    USBD_STM32F_FS_REG  *p_reg;


    p_reg      = (USBD_STM32F_FS_REG *)(p_drv->CfgPtr->BaseAddr);
    p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
    ep_log_nbr =  USBD_EP_ADDR_TO_LOG(ep_addr);
    ep_phy_nbr =  USBD_EP_ADDR_TO_PHY(ep_addr);
    ep_pkt_len = (CPU_INT16U)DEF_MIN(buf_len, p_drv_data->EP_MaxPktSize[ep_phy_nbr]);

    p_drv_data->EP_xfer_count[ep_phy_nbr] = 0;

    /* DMA Case 6, Checked */
    if (STM32F_FS_USE_DMA == DEF_ENABLED)
	{
		p_reg->DOEP[ep_log_nbr].DMAx = (CPU_REG32)p_buf;
	}

  	if (ep_log_nbr == 0)
   	{
   		return USBD_DrvEP0_RxStart(p_drv, ep_addr, p_buf, buf_len, p_err);
   	}

    USBD_DBG_DRV_EP("  Drv EP FIFO RxStart", ep_addr);

    ctl_reg  = p_reg->DOEP[ep_log_nbr].CTLx;                    /* Read Control EP reg                                  */
    tsiz_reg = p_reg->DOEP[ep_log_nbr].TSIZx;                   /* Read Transer EP reg                                  */

                                                                /* Clear EP transfer size and packet count              */
    DEF_BIT_CLR(tsiz_reg, (STM32F_FS_DOEPTSIZx_XFRSIZ_MSK | STM32F_FS_DOEPTSIZx_PKTCNT_MSK));

    if (buf_len == 0u)
    {
        tsiz_reg |=  p_drv_data->EP_MaxPktSize[ep_phy_nbr];     /* Set transfer size to max pkt size                    */
        tsiz_reg |= (1u << 19u);                                /* Set packet count                                     */
    } else
    {
        pkt_cnt   = (ep_pkt_len + (p_drv_data->EP_MaxPktSize[ep_phy_nbr] - 1u))
                  / p_drv_data->EP_MaxPktSize[ep_phy_nbr];
        tsiz_reg |= (pkt_cnt << 19u);                           /* Set packet count                                     */
                                                                /* Set transfer size                                    */
        tsiz_reg |= pkt_cnt * p_drv_data->EP_MaxPktSize[ep_phy_nbr];
    }

    p_drv_data->EP_AppBufPtr[ep_phy_nbr] = p_buf;
    p_drv_data->EP_AppBufLen[ep_phy_nbr] = ep_pkt_len;

                                                                /* Clear EP NAK and Enable EP for receiving             */
    ctl_reg |= STM32F_FS_DxEPCTLx_BIT_CNAK | STM32F_FS_DxEPCTLx_BIT_EPENA;

    p_drv_data->EP_PktXferLen[ep_phy_nbr] = 0u;
    p_reg->DOEP[ep_log_nbr].TSIZx         = tsiz_reg;
    p_reg->DOEP[ep_log_nbr].CTLx          = ctl_reg;

   *p_err = USBD_ERR_NONE;

    return (ep_pkt_len);
}


/*
*********************************************************************************************************
*                                           USBD_DrvEP_Rx()
*
* Description : Receive the specified amount of data from device endpoint.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               ep_addr     Endpoint address.
*
*               p_buf       Pointer to data buffer.
*
*               buf_len     Length of the buffer.
*
*               p_err       Pointer to variable that will receive the return error code from this function :
*
*                               USBD_ERR_NONE   Data successfully received.
*
* Return(s)   : Number of octets received, if NO error(s).
*
*               0,                         otherwise.
*
* Caller(s)   : USBD_EP_Rx() via 'p_drv_api->EP_Rx()',
*               USBD_EP_Process().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  CPU_INT32U  USBD_DrvEP_Rx (USBD_DRV    *p_drv,
                                   CPU_INT08U   ep_addr,
                                   CPU_INT08U  *p_buf,
                                   CPU_INT32U   buf_len,
                                   USBD_ERR    *p_err)
{
    CPU_INT08U         ep_phy_nbr;
    CPU_INT16U         xfer_len;
    USBD_DRV_DATA_EP  *p_drv_data;


    (void)&p_buf;

    p_drv_data = (USBD_DRV_DATA_EP *)p_drv->DataPtr;
    ep_phy_nbr =  USBD_EP_ADDR_TO_PHY(ep_addr);
    xfer_len   =  p_drv_data->EP_PktXferLen[ep_phy_nbr];

    USBD_DBG_DRV_EP("  Drv EP FIFO Rx", ep_addr);

    if (xfer_len > buf_len)
    {
       *p_err = USBD_ERR_RX;
    }
    else
    {
       *p_err = USBD_ERR_NONE;
    }

    return (DEF_MIN(xfer_len, buf_len));
}


/*
*********************************************************************************************************
*                                         USBD_DrvEP_RxZLP()
*
* Description : Receive zero-length packet from endpoint.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               ep_addr     Endpoint address.
*
*               p_err       Pointer to variable that will receive the return error code from this function :
*
*                               USBD_ERR_NONE   Zero-length packet successfully received.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_EP_RxZLP() via 'p_drv_api->EP_RxZLP()'.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  USBD_DrvEP_RxZLP (USBD_DRV    *p_drv,
                                CPU_INT08U   ep_addr,
                                USBD_ERR    *p_err)
{
    (void)&p_drv;
    (void)&ep_addr;


    USBD_DBG_DRV_EP("  Drv EP RxZLP", ep_addr);

   *p_err = USBD_ERR_NONE;
}


/*
*********************************************************************************************************
*                                           USBD_DrvEP_Tx()
*
* Description : Configure endpoint with buffer to transmit data.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               ep_addr     Endpoint address.
*
*               p_buf       Pointer to buffer of data that will be transmitted.
*
*               buf_len     Number of octets to transmit.
*
*               p_err       Pointer to variable that will receive the return error code from this function :
*
*                               USBD_ERR_NONE   Transmit successfully configured.
*
* Return(s)   : Number of octets transmitted, if NO error(s).
*
*               0,                            otherwise.
*
* Caller(s)   : USBD_EP_Tx() via 'p_drv_api->EP_Tx()',
*               USBD_EP_Process().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  CPU_INT32U  USBD_DrvEP_Tx (USBD_DRV    *p_drv,
                                   CPU_INT08U   ep_addr,
                                   CPU_INT08U  *p_buf,
                                   CPU_INT32U   buf_len,
                                   USBD_ERR    *p_err)
{
    CPU_INT08U         ep_phy_nbr;
    CPU_INT16U         ep_pkt_len;
    USBD_DRV_DATA_EP  *p_drv_data;


    (void)&p_buf;

    p_drv_data = (USBD_DRV_DATA_EP *)p_drv->DataPtr;
    ep_phy_nbr =  USBD_EP_ADDR_TO_PHY(ep_addr);
    ep_pkt_len = (CPU_INT16U)DEF_MIN(p_drv_data->EP_MaxPktSize[ep_phy_nbr], buf_len);

   *p_err = USBD_ERR_NONE;

    return (ep_pkt_len);
}


/*
*********************************************************************************************************
*                                        USBD_DrvEP_TxStart()
*
* Description : Transmit the specified amount of data to device endpoint.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               ep_addr     Endpoint address.
*
*               p_buf       Pointer to buffer of data that will be transmitted.
*
*               buf_len     Number of octets to transmit.
*
*               p_err       Pointer to variable that will receive the return error code from this function :
*
*                               USBD_ERR_NONE   Data successfully transmitted.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_EP_Tx() via 'p_drv_api->EP_TxStart()',
*               USBD_EP_Process().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  USBD_DrvEP0_TxStart (USBD_DRV    *p_drv,
                                  CPU_INT08U   ep_addr,
                                  CPU_INT08U  *p_buf,
                                  CPU_INT32U   buf_len,
                                  USBD_ERR    *p_err)
{
    CPU_INT08U           ep_log_nbr;
    CPU_INT32U           ctl_reg;
    CPU_INT32U           tsiz_reg;
    USBD_STM32F_FS_REG  *p_reg;
    USBD_DRV_DATA_EP    *p_drv_data;
    CPU_INT08U  		ep_phy_nbr ;

    p_reg      = (USBD_STM32F_FS_REG *)(p_drv->CfgPtr->BaseAddr);
    ep_log_nbr =  USBD_EP_ADDR_TO_LOG(ep_addr);
    p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
	ep_phy_nbr = USBD_EP_ADDR_TO_PHY(USBD_EP_LOG_TO_ADDR_OUT(ep_log_nbr));


    ctl_reg  = p_reg->DIEP[ep_log_nbr].CTLx;                    /* Read EP control reg.                                 */
    tsiz_reg = p_reg->DIEP[ep_log_nbr].TSIZx;                   /* Read EP transfer reg                                 */

    /* Zero Length Packet? */
	if (buf_len == 0U)
	{
		tsiz_reg &= ~(STM32F_FS_DIEPTSIZx_PKTCNT_MSK);
		tsiz_reg |= (STM32F_FS_DIEPTSIZx_PKTCNT_MSK & (1U << 19U)) ;
		tsiz_reg &= ~(STM32F_FS_DIEPTSIZx_XFRSIZ_MSK);
	}
	else
	{
		/* Program the transfer size and packet count
		* as follows: xfersize = N * maxpacket +
		* short_packet pktcnt = N + (short_packet
		* exist ? 1 : 0)
		*/
		tsiz_reg &= ~(STM32F_FS_DIEPTSIZx_XFRSIZ_MSK);
		tsiz_reg &= ~(STM32F_FS_DIEPTSIZx_PKTCNT_MSK);
		if (buf_len > p_drv_data->EP_MaxPktSize[ep_phy_nbr])
		{
			buf_len = p_drv_data->EP_MaxPktSize[ep_phy_nbr];
		}

		tsiz_reg |= (STM32F_FS_DIEPTSIZx_PKTCNT_MSK & (1u << 19U)) ;
		tsiz_reg |= (STM32F_FS_DIEPTSIZx_XFRSIZ_MSK & buf_len);
	}

	p_reg->DIEP[ep_log_nbr].TSIZx = tsiz_reg;

    /* Clear EP NAK mode & Enable EP transmitting.          */
	ctl_reg |= STM32F_FS_DxEPCTLx_BIT_CNAK | STM32F_FS_DxEPCTLx_BIT_EPENA;
    p_reg->DIEP[ep_log_nbr].CTLx  = ctl_reg;

	  /* DMA Case 9, Checked */
    /* DMA Case 10, Checked */
    /* DMA Case 11, not Checked */
    /* DMA Case 12, not Checked */
    if (STM32F_FS_USE_DMA == DEF_ENABLED)
    {
		p_drv_data->EP_AppBufPtr[ep_phy_nbr] = p_buf;
		p_drv_data->EP_AppBufLen[ep_phy_nbr] = buf_len;

    	p_reg->DIEP[ep_log_nbr].DMAx = (CPU_REG32)p_buf;
    }


    if (STM32F_FS_USE_DMA == DEF_DISABLED)
    {
    	STM32_TxFIFO_Wr(p_drv, ep_log_nbr, p_buf, buf_len);         /* Write to Tx FIFO of associated EP                    */
    }

     USBD_DBG_DRV_EP_ARG("  Drv EP0 FIFO Tx Len:", ep_addr, buf_len);

    *p_err = USBD_ERR_NONE;
}

static  void  USBD_DrvEP_TxStart (USBD_DRV    *p_drv,
                                  CPU_INT08U   ep_addr,
                                  CPU_INT08U  *p_buf,
                                  CPU_INT32U   buf_len,
                                  USBD_ERR    *p_err)
{
    CPU_INT08U           ep_log_nbr;
    CPU_INT32U           ctl_reg;
    CPU_INT32U           tsiz_reg;
    USBD_STM32F_FS_REG  *p_reg;
    CPU_INT08U  		ep_phy_nbr ;
    USBD_DRV_DATA_EP    *p_drv_data;


    p_reg      = (USBD_STM32F_FS_REG *)(p_drv->CfgPtr->BaseAddr);
    ep_log_nbr =  USBD_EP_ADDR_TO_LOG(ep_addr);
    p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
	ep_phy_nbr = USBD_EP_ADDR_TO_PHY(USBD_EP_LOG_TO_ADDR_OUT(ep_log_nbr));

    ctl_reg  = p_reg->DIEP[ep_log_nbr].CTLx;                    /* Read EP control reg.                                 */
    tsiz_reg = p_reg->DIEP[ep_log_nbr].TSIZx;                   /* Read EP transfer reg                                 */

    /* Zero Length Packet? */
 	if (buf_len == 0U)
 	{
 		tsiz_reg &= ~(STM32F_FS_DIEPTSIZx_PKTCNT_MSK);
 		tsiz_reg |= (STM32F_FS_DIEPTSIZx_PKTCNT_MSK & (1U << 19U)) ;
 		tsiz_reg &= ~(STM32F_FS_DIEPTSIZx_XFRSIZ_MSK);

 		p_reg->DIEP[ep_log_nbr].TSIZx = tsiz_reg;
 	}
 	else
 	{
		tsiz_reg &= ~(STM32F_FS_DIEPTSIZx_XFRSIZ_MSK);
		tsiz_reg &= ~(STM32F_FS_DIEPTSIZx_PKTCNT_MSK);
		tsiz_reg |= (STM32F_FS_DIEPTSIZx_PKTCNT_MSK & (((buf_len + p_drv_data->EP_MaxPktSize[ep_phy_nbr]  -1U)/ p_drv_data->EP_MaxPktSize[ep_phy_nbr]) << 19U)) ;
		tsiz_reg |= (STM32F_FS_DIEPTSIZx_XFRSIZ_MSK & buf_len);

//		tsiz_reg |=  buf_len;                                       /* Transfer size                                        */
//		tsiz_reg |= (1u << 19u);                                    /* Packet count  */
		p_reg->DIEP[ep_log_nbr].TSIZx = tsiz_reg;
	}

    if (STM32F_FS_USE_DMA == DEF_ENABLED)
    {
		p_drv_data->EP_AppBufPtr[ep_phy_nbr] = p_buf;
		p_drv_data->EP_AppBufLen[ep_phy_nbr] = buf_len;

    	p_reg->DIEP[ep_log_nbr].DMAx = (CPU_REG32)p_buf;
    }

                                                                /* Clear EP NAK mode & Enable EP transmitting.          */
    ctl_reg |= STM32F_FS_DxEPCTLx_BIT_CNAK | STM32F_FS_DxEPCTLx_BIT_EPENA;

     p_reg->DIEP[ep_log_nbr].CTLx  = ctl_reg;

    if (STM32F_FS_USE_DMA == DEF_DISABLED)
    {
    	STM32_TxFIFO_Wr(p_drv, ep_log_nbr, p_buf, buf_len);         /* Write to Tx FIFO of associated EP                    */
    }

    USBD_DBG_DRV_EP_ARG("  Drv EP FIFO Tx Len:", ep_addr, buf_len);

    *p_err = USBD_ERR_NONE;
}


/*
*********************************************************************************************************
*                                         USBD_DrvEP_TxZLP()
*
* Description : Transmit zero-length packet from endpoint.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               ep_addr     Endpoint address.
*
*               p_err       Pointer to variable that will receive the return error code from this function :
*
*                               USBD_ERR_NONE   Zero-length packet successfully transmitted.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_EP_Tx() via 'p_drv_api->EP_TxZLP()',
*               USBD_EP_TxZLP(),
*               USBD_EP_Process().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  USBD_DrvEP_TxZLP (USBD_DRV    *p_drv,
                                CPU_INT08U   ep_addr,
                                USBD_ERR    *p_err)
{
    USBD_DBG_DRV_EP("  Drv EP TxZLP", ep_addr);

    USBD_DrvEP_TxStart(              p_drv,
                                     ep_addr,
                       (CPU_INT08U *)0,
                                     0u,
                                     p_err);
}


/*
*********************************************************************************************************
*                                         USBD_DrvEP_Abort()
*
* Description : Abort any pending transfer on endpoint.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               ep_addr     Endpoint Address.
*
* Return(s)   : DEF_OK,   if NO error(s).
*
*               DEF_FAIL, otherwise.
*
* Caller(s)   : USBD_URB_Abort() via 'p_drv_api->EP_Abort()'.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  CPU_BOOLEAN  USBD_DrvEP_Abort (USBD_DRV    *p_drv,
                                       CPU_INT08U   ep_addr)
{
    CPU_INT08U           ep_log_nbr;
    CPU_INT32U           reg_to;
    USBD_STM32F_FS_REG  *p_reg;


    p_reg      = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;
    ep_log_nbr =  USBD_EP_ADDR_TO_LOG(ep_addr);

    if (USBD_EP_IS_IN(ep_addr) == DEF_YES) {                    /* ------------------- IN ENDPOINTS ------------------- */
                                                                /* Set Endpoint to NAK mode                             */
        DEF_BIT_SET(p_reg->DIEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_SNAK);

        reg_to = STM32F_FS_REG_VAL_TO;                          /* Wait for IN EP NAK effective                         */
        while ((DEF_BIT_IS_CLR(p_reg->DIEP[ep_log_nbr].INTx, STM32F_FS_DIEPINTx_BIT_INEPNE) == DEF_YES) &&
               (reg_to > 0u)) {
            reg_to--;
        }

        DEF_BIT_SET(p_reg->DIEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_EPDIS);

        reg_to = STM32F_FS_REG_VAL_TO;                          /* Wait for EP disable                                  */
        while ((DEF_BIT_IS_SET(p_reg->DIEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_EPDIS) == DEF_YES) &&
               (reg_to > 0u)) {
            reg_to--;
        }
                                                                /* Clear EP Disable interrupt                           */
        DEF_BIT_SET(p_reg->DIEP[ep_log_nbr].INTx, STM32F_FS_DIEPINTx_BIT_EPDISD);

                                                                /* Flush EPx TX FIFO                                    */
        p_reg->GRSTCTL = STM32F_FS_GRSTCTL_BIT_TXFFLSH |  (ep_log_nbr << 6u);

        reg_to = STM32F_FS_REG_VAL_TO;                          /* Wait for the flush completion                        */
        while ((DEF_BIT_IS_SET(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_TXFFLSH) == DEF_YES) &&
               (reg_to > 0u)) {
            reg_to--;
        }

    } else {                                                    /* ------------------ OUT ENDPOINTS ------------------- */

        DEF_BIT_SET(p_reg->DOEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_SNAK);

                                                                /* Flush EPx RX FIFO                                    */
        DEF_BIT_SET(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_RXFFLSH);

        reg_to = STM32F_FS_REG_VAL_TO;                          /* Wait for the flush completion                        */
        while ((DEF_BIT_IS_SET(p_reg->GRSTCTL, STM32F_FS_GRSTCTL_BIT_RXFFLSH) == DEF_YES) &&
               (reg_to > 0u)) {
            reg_to--;
        }
    }

    USBD_DBG_DRV_EP("  Drv EP Abort", ep_addr);

    return (DEF_OK);
}


/*
*********************************************************************************************************
*                                         USBD_DrvEP_Stall()
*
* Description : Set or clear stall condition on endpoint.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
*               ep_addr     Endpoint address.
*
*               state       Endpoint stall state.
*
* Return(s)   : DEF_OK,   if NO error(s).
*
*               DEF_FAIL, otherwise.
*
* Caller(s)   : USBD_EP_Stall() via 'p_drv_api->EP_Stall()',
*               USBD_CtrlStall().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  CPU_BOOLEAN  USBD_DrvEP_Stall (USBD_DRV     *p_drv,
                                       CPU_INT08U    ep_addr,
                                       CPU_BOOLEAN   state)
{
    CPU_INT08U           ep_log_nbr;
    CPU_INT32U           ep_type;
    USBD_STM32F_FS_REG  *p_reg;


    p_reg      = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;
    ep_log_nbr =  USBD_EP_ADDR_TO_LOG(ep_addr);

    USBD_DBG_DRV_EP("  Drv EP Stall", ep_addr);

    if (USBD_EP_IS_IN(ep_addr) == DEF_YES) {                    /* ------------------- IN ENDPOINTS ------------------- */
        if (state == DEF_SET) {
                                                                /* Disable Endpoint if enable for transmit              */
            if (DEF_BIT_IS_SET(p_reg->DIEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_EPENA) == DEF_YES) {
                DEF_BIT_SET(p_reg->DIEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_EPDIS);
            }
                                                                /* Set Stall bit                                        */
            DEF_BIT_SET(p_reg->DIEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_STALL);

        } else {
            ep_type = (p_reg->DIEP[ep_log_nbr].CTLx >> 18u) & DEF_BIT_FIELD(2u, 0u);
                                                                /* Clear Stall Bit                                      */
            DEF_BIT_CLR(p_reg->DIEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_STALL);

            if ((ep_type == USBD_EP_TYPE_INTR) ||
                (ep_type == USBD_EP_TYPE_BULK)) {
                                                                /* Set DATA0 PID                                        */
                DEF_BIT_SET(p_reg->DIEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_SD0PID);
            }
        }

    } else {                                                    /* ------------------- OUT ENDPOINTS ------------------ */
        if (state == DEF_SET) {
                                                                /* Set Stall bit                                        */
            DEF_BIT_SET(p_reg->DOEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_STALL);

        } else {
            ep_type = (p_reg->DOEP[ep_log_nbr].CTLx >> 18u) & DEF_BIT_FIELD(2u, 0u);
                                                                /* Clear Stall Bit                                      */
            DEF_BIT_CLR(p_reg->DOEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_STALL);

            if ((ep_type == USBD_EP_TYPE_INTR) || (ep_type == USBD_EP_TYPE_BULK)) {
                                                                /* Set DATA0 PID                                        */
                DEF_BIT_SET(p_reg->DOEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_SD0PID);
            }
        }
    }

    /* DMA Case12, Checked */
    if(ep_log_nbr == 0)
    {
  	  /* DMA Case 7, Checked */
    	USBD_DRV_DATA_EP* p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
    	CPU_INT32U*	p_data_buf = &(p_drv_data->EP_SetupBuf[0u]);

    	STM32_EP0_OutStart(p_reg, STM32F_FS_USE_DMA, p_data_buf);
    }
    return (DEF_OK);
}


/*
*********************************************************************************************************
*                                        USBD_DrvISR_Handler()
*
* Description : USB device Interrupt Service Routine (ISR) handler.
*
* Argument(s) : p_drv       Pointer to device driver structure.
*
* Return(s)   : none.
*
* Caller(s)   : This is an ISR.
*
* Note(s)     : none.
*********************************************************************************************************
*/

CPU_INT32U fs_firstUsbInts[10];
int fs_numUsbInts = 0;


static  void  USBD_DrvISR_Handler (USBD_DRV  *p_drv)
{
    CPU_INT32U          int_stat;
    CPU_INT32U          otgint_stat;
    USBD_STM32F_FS_REG*	p_reg;

    CPU_INT32U 			hclk = 180000000;

    p_reg    = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;
    int_stat =  p_reg->GINTSTS;                                 /* Read global interrrupt status register               */

    int_stat &= p_reg->GINTMSK;									/* Mask out undesired interrupts (they are masked but still apear in the interrupt register */

    if (fs_numUsbInts<10)
    {
  	  fs_firstUsbInts[fs_numUsbInts++] = int_stat;
    }

    															/* ------------ RX FIFO NON-EMPTY DETECTION ----------- */
	if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_RXFLVL) == DEF_YES)
	{
		STM32_RxFIFO_Rd(p_drv);
	}

		/* --------------- IN ENDPOINT INTERRUPT -------------- */
	if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_IEPINT) == DEF_YES)
	{
		STM32_EP_InProcess(p_drv);
	}

	/* -------------- OUT ENDPOINT INTERRUPT -------------- */
	if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_OEPINT) == DEF_YES)
	{
		STM32_EP_OutProcess(p_drv);
	}


    if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_USBRST) == DEF_YES)
    {
        USBD_EventReset(p_drv);                                 /* Notify bus reset event.                              */
                                                                 /* Enable Global OUT/IN EP interrupt...                 */
                                                                 /* ...and RX FIFO non-empty interrupt.                  */
         DEF_BIT_SET(p_reg->GINTMSK, (STM32F_FS_GINTMSK_BIT_OEPINT   |
                                      STM32F_FS_GINTMSK_BIT_IEPINT   |
                                      STM32F_FS_GINTMSK_BIT_RXFLVLM));
                                                                 /* Clear USB bus reset interrupt                        */
         DEF_BIT_SET(p_reg->GINTSTS, STM32F_FS_GINTSTS_BIT_USBRST);

         USBD_DrvAddrSet(p_drv, 0u);

         // Special
         p_reg->DCFG |=0X00004000;  //Bit 14 XCVRDLY: Transceiver delay

			STM32_SetRxFiFo(p_reg, STM32F_FS_RXFIFO_SIZE);
			STM32_SetTxFiFo(p_reg, 0, STM32F_FS_TXFIFO_EP0_SIZE);
			STM32_SetTxFiFo(p_reg, 1, STM32F_FS_TXFIFO_EP1_SIZE);
			p_reg->GRXFSIZ     =  STM32F_FS_RXFIFO_SIZE;            /* Set Rx FIFO depth                                    */
																  /* Set EP0 to EP3 Tx FIFO depth                         */
			p_reg->DIEPTXF0    = (STM32F_FS_TXFIFO_EP0_SIZE << 16u) |  STM32F_FS_RXFIFO_SIZE;
			p_reg->DIEPTXFx[0] = (STM32F_FS_TXFIFO_EP1_SIZE << 16u) | (STM32F_FS_RXFIFO_SIZE +
																	 STM32F_FS_TXFIFO_EP0_SIZE);

			p_reg->DIEPTXFx[1] = (STM32F_FS_TXFIFO_EP2_SIZE << 16u) | (STM32F_FS_RXFIFO_SIZE +
																	 STM32F_FS_TXFIFO_EP0_SIZE +
																	 STM32F_FS_TXFIFO_EP1_SIZE);
			p_reg->DIEPTXFx[2] = (STM32F_FS_TXFIFO_EP3_SIZE << 16u) | (STM32F_FS_RXFIFO_SIZE +
																	 STM32F_FS_TXFIFO_EP0_SIZE +
																	 STM32F_FS_TXFIFO_EP1_SIZE +
																	 STM32F_FS_TXFIFO_EP2_SIZE);

         /* setup EP0 to receive SETUP packets */
 			USBD_DRV_DATA_EP* p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
			CPU_INT32U*	p_data_buf = &(p_drv_data->EP_SetupBuf[0u]);

			/* DMA Case 5, Checked */
			STM32_EP0_OutStart(p_reg, STM32F_FS_USE_DMA, p_data_buf);

//        USBD_EventReset(p_drv);                                 /* Notify bus reset event.                              */
//
//        p_reg->DCTL &= ~STM32F_FS_DCTL_BIT_RWUSIG;
//
//        STM32_TxFifoFlush(p_reg, 0u);
//
//        int i;
//        for (i = 0U; i < STM32F_FS_NBR_EPS; i++)
//        {
//          p_reg->DIEP[i].INTx = 0xFFU;
//          p_reg->DOEP[i].INTx = 0xFFU;
//        }
//
//        p_reg->DAINT = 0xFFFFFFFFU;
//        p_reg->DAINTMSK |= 0x10001U;
//
//        p_reg->DOEPMSK |= (STM32F_FS_DOEPMSK_BIT_STUPM | STM32F_FS_DOEPMSK_BIT_XFRCM | STM32F_FS_DOEPMSK_BIT_EPDM | STM32F_FS_DOEPMSK_BIT_OTEPSPRM);
//        p_reg->DIEPMSK |= (STM32F_FS_DIEPMSK_BIT_TOM | STM32F_FS_DIEPMSK_BIT_XFRCM | STM32F_FS_DIEPMSK_BIT_EPDM);
//
//        USBD_DrvAddrSet(p_drv, 0u);
//
//        /* setup EP0 to receive SETUP packets */
//        USBD_DRV_DATA_EP* p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
//        CPU_INT32U*	p_data_buf = &(p_drv_data->EP_SetupBuf[0u]);
//
//        STM32_EP0_OutStart(p_reg, STM32F_FS_USE_DMA, p_data_buf);
//
//        /* Clear USB bus reset interrupt                        */
//        DEF_BIT_SET(p_reg->GINTSTS, STM32F_FS_GINTSTS_BIT_USBRST);
   }

    /* ---------------- USB RESET DETECTION --------------- */

                                                               /* ------------ ENUMERATION DONE DETECTION ------------ */
    if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_ENUMDNE) == DEF_YES)
    {
    	STM32_ActivateSetup(p_reg);

         	p_reg->GUSBCFG &= ~STM32F_FS_GUSBCFG_TRDT_MASK;
         	p_reg->GUSBCFG |= (CPU_INT32U)((STM32F_FS_TRDT_VALUE << 10U) & STM32F_FS_GUSBCFG_TRDT_MASK);

         	p_reg->GUSBCFG |= (CPU_INT32U)((1) & STM32F_FS_GUSBCFG_TOCAL_MASK);
//         	p_reg->GUSBCFG |= (CPU_INT32U)((7) & STM32F_FS_GUSBCFG_TOCAL_MASK);

           /* The USBTRD is configured according to the tables below, depending on AHB frequency
           used by application. In the low AHB frequency range it is used to stretch enough the USB response
           time to IN tokens, the USB turnaround time, so to compensate for the longer AHB read access
           latency to the Data FIFO */

            /* Get hclk frequency value */
             hclk = HAL_RCC_GetHCLKFreq();

           if((hclk >= 14200000)&&(hclk < 15000000))
           {
             /* hclk Clock Range between 14.2-15 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0xF << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           else if((hclk >= 15000000)&&(hclk < 16000000))
           {
             /* hclk Clock Range between 15-16 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0xE << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           else if((hclk >= 16000000)&&(hclk < 17200000))
           {
             /* hclk Clock Range between 16-17.2 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0xD << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           else if((hclk >= 17200000)&&(hclk < 18500000))
           {
             /* hclk Clock Range between 17.2-18.5 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0xC << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           else if((hclk >= 18500000)&&(hclk < 20000000))
           {
             /* hclk Clock Range between 18.5-20 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0xB << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           else if((hclk >= 20000000)&&(hclk < 21800000))
           {
             /* hclk Clock Range between 20-21.8 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0xA << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           else if((hclk >= 21800000)&&(hclk < 24000000))
           {
             /* hclk Clock Range between 21.8-24 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0x9 << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           else if((hclk >= 24000000)&&(hclk < 27700000))
           {
             /* hclk Clock Range between 24-27.7 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0x8 << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           else if((hclk >= 27700000)&&(hclk < 32000000))
           {
             /* hclk Clock Range between 27.7-32 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0x7 << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           else /* if(hclk >= 32000000) */
           {
             /* hclk Clock Range between 32-180 MHz */
         	  p_reg->GUSBCFG |= (CPU_INT32U)((0x6 << 10) & STM32F_FS_GUSBCFG_TRDT_MASK);
           }

           /*Reset Device*/
//         STM32_USBD_LL_Reset(p_drv);
                                                              /* Clear Enumeration done interrupt                     */
        DEF_BIT_SET(p_reg->GINTSTS, STM32F_FS_GINTSTS_BIT_ENUMDNE);
    }

																/* ------------------ MODE MISMATCH ------------------- */
    if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_MMIS) == DEF_YES)
    {
    	DEF_BIT_SET(p_reg->GINTSTS, STM32F_FS_GINTSTS_BIT_MMIS);
    }

                                                                /* -------------- SESSION REQ DETECTION --------------- */
    if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_SRQINT) == DEF_YES) {

        USBD_EventConn(p_drv);                                  /* Notify connect event.                                */
                                                                /* Clear all OTG interrupt sources.                     */
        DEF_BIT_SET(p_reg->GINTSTS, STM32F_FS_GINTSTS_BIT_SRQINT);
    }

                                                                /* ------------------ OTG INTERRUPT ------------------- */
    if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_OTGINT) == DEF_YES) {

        otgint_stat = p_reg->GOTGINT;
        if (DEF_BIT_IS_SET(otgint_stat, STM32F_FS_GOTGINT_BIT_SEDET) == DEF_YES) {
            USBD_EventDisconn(p_drv);                           /* Notify disconnect event.                             */
        }
                                                                /* Clear all OTG interrupt sources.                     */
        DEF_BIT_SET(p_reg->GOTGINT, (STM32F_FS_GOTGINT_BIT_SEDET   |
                                     STM32F_FS_GOTGINT_BIT_SRSSCHG |
                                     STM32F_FS_GOTGINT_BIT_HNSSCHG |
                                     STM32F_FS_GOTGINT_BIT_HNGDET  |
                                     STM32F_FS_GOTGINT_BIT_ADTOCHG |
                                     STM32F_FS_GOTGINT_BIT_DBCDNE));
    }

                                                                /* ------------- EARLY SUSPEND DETECTION -------------- */
    if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_ESUSP) == DEF_YES) {
                                                                /* Clear the Early Suspend interrupt                    */
        DEF_BIT_SET(p_reg->GINTSTS, STM32F_FS_GINTSTS_BIT_ESUSP);
    }

                                                                /* --------------- USB SUSPEND DETECTION -------------- */
    if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_USBSUSP) == DEF_YES) {
        USBD_EventSuspend(p_drv);                               /* Notify Suspend Event                                 */
                                                                /* Clear USB Suspend interrupt                          */
        DEF_BIT_SET(p_reg->GINTSTS, STM32F_FS_GINTSTS_BIT_USBSUSP);
    }

                                                                /* ----------------- WAKE-UP DETECTION ---------------- */
    if (DEF_BIT_IS_SET(int_stat, STM32F_FS_GINTSTS_BIT_WKUPINT) == DEF_YES) {
        USBD_EventResume(p_drv);                                /* Notify Resume Event                                  */

        DEF_BIT_CLR(p_reg->PCGCR, (STM32F_FS_PCGCCTL_BIT_GATEHCLK |
                                   STM32F_FS_PCGCCTL_BIT_STPPCLK));

        DEF_BIT_CLR(p_reg->DCTL, STM32F_FS_DCTL_BIT_RWUSIG);    /* Clear Remote wakeup signaling                        */
                                                                /* Clear Remote wakeup interrupt                        */
        DEF_BIT_SET(p_reg->GINTSTS, STM32F_FS_GINTSTS_BIT_WKUPINT);
    }
}


/*
*********************************************************************************************************
*********************************************************************************************************
*                                           LOCAL FUNCTIONS
*********************************************************************************************************
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                          STM32_RxFIFO_Rd()
*
* Description : Handle Rx FIFO non-empty interrupt generated when a data OUT packet has been received.
*
* Arguments   : p_drv       Pointer to device driver structure.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_DrvISR_Handler().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  STM32_RxFIFO_Rd (USBD_DRV  *p_drv)
{
    CPU_INT08U           ep_log_nbr;
    CPU_INT08U           ep_phy_nbr;
    CPU_INT08U           pkt_stat;
    CPU_INT08U           byte_rem;
    CPU_INT08U           word_cnt;
    CPU_INT16U           byte_cnt;
    CPU_INT32U          *p_data_buf;
    CPU_INT32U           reg_val;
    CPU_INT32U           i;
    CPU_INT32U           data;
    USBD_DRV_DATA_EP    *p_drv_data;
    USBD_STM32F_FS_REG  *p_reg;


    p_reg      = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;
    p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
    reg_val    =  p_reg->GRXSTSP;
    ep_log_nbr = (reg_val & STM32F_FS_GRXSTSx_EPNUM_MASK)  >>  0u;
    ep_phy_nbr =  USBD_EP_ADDR_TO_PHY(USBD_EP_LOG_TO_ADDR_OUT(ep_log_nbr));
    byte_cnt   = (reg_val & STM32F_FS_GRXSTSx_BCNT_MASK)   >>  4u;
    pkt_stat   = (reg_val & STM32F_FS_GRXSTSx_PKTSTS_MASK) >> 17u;


    p_drv_data->EP_PktXferLen[ep_phy_nbr] += byte_cnt;

                                                                /* Disable Rx FIFO non-empty                            */
    DEF_BIT_CLR(p_reg->GINTMSK, STM32F_FS_GINTMSK_BIT_RXFLVLM);

    switch (pkt_stat) {
        case STM32F_FS_GRXSTSx_PKTSTS_SETUP_RX:
             p_data_buf = &p_drv_data->EP_SetupBuf[0u];
             if (byte_cnt == 8u) {                              /* Read Setup packet from Rx FIFO                        */
                 p_data_buf[0u] = *p_reg->DFIFO[ep_log_nbr].DATA;
                 p_data_buf[1u] = *p_reg->DFIFO[ep_log_nbr].DATA;
             }

             DEF_BIT_SET(p_reg->DOEP[0u].CTLx, STM32F_FS_DxEPCTLx_BIT_SNAK);
             break;


        case STM32F_FS_GRXSTSx_PKTSTS_OUT_RX:
             DEF_BIT_SET(p_reg->DOEP[ep_log_nbr].CTLx,          /* Put endpoint in NACK mode.                           */
                         STM32F_FS_DxEPCTLx_BIT_SNAK);

             if ((byte_cnt                             >   0u) &&
                 (p_drv_data->EP_AppBufPtr[ep_phy_nbr] != (CPU_INT08U *)0)) {

                 byte_cnt =  DEF_MIN(byte_cnt, p_drv_data->EP_AppBufLen[ep_phy_nbr]);
                 word_cnt = (byte_cnt / 4u);

                                                                /* Check app buffer alignement.                         */
                 if ((((CPU_INT32U)p_drv_data->EP_AppBufPtr[ep_phy_nbr]) % 4u) == 0u) {
                     p_data_buf = (CPU_INT32U *)p_drv_data->EP_AppBufPtr[ep_phy_nbr];
                 } else {
                     p_data_buf = &p_drv_data->DataBuf[0u];     /* Use drv internal aligned buf if app buf not aligned. */
                 }

                                                                /* Read OUT packet from Rx FIFO                         */
                 for (i = 0u; i < word_cnt; i++) {
                     *p_data_buf = *p_reg->DFIFO[ep_log_nbr].DATA;
                      p_data_buf++;
                 }

                 byte_rem = (byte_cnt - (word_cnt * 4u));
                 if (byte_rem > 0u) {                           /* Rd remaining data if byte_cnt not multiple of 4.     */
                     data = *p_reg->DFIFO[ep_log_nbr].DATA;

                     for (i = 0u; i < byte_rem; i++) {
                         ((CPU_INT08U *)p_data_buf)[i] = (CPU_INT08U)((data >> (8u * i)) & 0xFFu);
                     }
                 }

                                                                /* Copy data to app buf if not aligned.                 */
                 if ((((CPU_INT32U)p_drv_data->EP_AppBufPtr[ep_phy_nbr]) % 4u) != 0u) {
                     p_data_buf = &p_drv_data->DataBuf[0u];
                     for (i = 0u; i < byte_cnt; i++) {
                         p_drv_data->EP_AppBufPtr[ep_phy_nbr][i] = ((CPU_INT08U *)p_data_buf)[i];
                     }
                 }

                 p_drv_data->EP_AppBufPtr[ep_phy_nbr] = (CPU_INT08U *)0;
             }
             break;


        case STM32F_FS_GRXSTSx_PKTSTS_OUT_COMPL:
        case STM32F_FS_GRXSTSx_PKTSTS_SETUP_COMPL:
        case STM32F_FS_GRXSTSx_PKTSTS_OUT_NAK:
        default:
             break;
    }
                                                                /* Enable Rx FIFO non-empty interrupt                   */
    DEF_BIT_SET(p_reg->GINTMSK, STM32F_FS_GINTMSK_BIT_RXFLVLM);

}

/*
*********************************************************************************************************
*                                          STM32_TxFIFO_Wr()
*
* Description : Writes a packet into the Tx FIFO associated with the Endpoint.
*
* Arguments   : p_drv       Pointer to device driver structure.
*
*               ep_log_nbr  Endpoint logical number.
*
*               p_buf       Pointer to buffer of data that will be transmitted.
*
*               ep_pkt_len  Number of octets to transmit.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_DrvEP_TxStart().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  STM32_TxFIFO_Wr (USBD_DRV    *p_drv,
                               CPU_INT08U   ep_log_nbr,
                               CPU_INT08U  *p_buf,
                               CPU_INT16U   ep_pkt_len)
{
    CPU_INT32U           nbr_words;
    CPU_INT32U           i;
    CPU_INT32U           words_avail;
    CPU_INT32U          *p_data_buf;
    USBD_STM32F_FS_REG  *p_reg;
    USBD_DRV_DATA_EP    *p_drv_data;
    CPU_SR_ALLOC();


    p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
    p_reg      = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;
    nbr_words  = (ep_pkt_len + 3u) / 4u;

    do {
                                                                /* Read available words in the EP Tx FIFO               */
        words_avail = p_reg->DIEP[ep_log_nbr].DTXFSTSx & 0x0000FFFFu;
    } while (words_avail < nbr_words);                          /* Check if there are enough words to write into FIFO   */

    CPU_CRITICAL_ENTER();
    if (((CPU_INT32U)p_buf % 4u) == 0u) {                       /* Check app buffer alignement.                         */
        p_data_buf = (CPU_INT32U *)p_buf;
    } else {
        p_data_buf = &p_drv_data->DataBuf[0u];                  /* Use drv internal aligned buf if app buf not aligned. */

        for (i = 0u; i < ep_pkt_len; i++) {
            ((CPU_INT08U *)p_data_buf)[i] = p_buf[i];
        }
    }

    for (i = 0u; i < nbr_words; i++) {
       *p_reg->DFIFO[ep_log_nbr].DATA = *p_data_buf;
        p_data_buf++;
    }
    CPU_CRITICAL_EXIT();
}

/*
*********************************************************************************************************
*                                        STM32_EP_OutProcess()
*
* Description : Process OUT interrupts on associated EP.
*
* Arguments   : p_drv       Pointer to device driver structure.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_DrvISR_Handler().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  STM32_EP_OutProcess (USBD_DRV  *p_drv)
{

    CPU_INT32U           ep_int_stat;
    CPU_INT32U           dev_ep_int;
    CPU_INT32U           ep_log_nbr;
    USBD_DRV_DATA_EP    *p_drv_data;
    USBD_STM32F_FS_REG  *p_reg;


    p_reg      = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;
    p_drv_data = (USBD_DRV_DATA_EP   *)p_drv->DataPtr;
    dev_ep_int =  p_reg->DAINT >> 16u;                          /* Read all Device OUT Endpoint interrupt               */

    while (dev_ep_int != DEF_BIT_NONE)
    {
        ep_log_nbr  = (CPU_INT08U)(31u - CPU_CntLeadZeros32(dev_ep_int & 0x0000FFFFu));
        ep_int_stat =  p_reg->DOEP[ep_log_nbr].INTx;            /* Read OUT EP interrupt status                         */

                                                                /* Check if EP transfer completed occurred              */
        if (DEF_BIT_IS_SET(ep_int_stat, STM32F_FS_DOEPINTx_BIT_XFRC))
        {
			CPU_INT08U  ep_phy_nbr = USBD_EP_ADDR_TO_PHY(USBD_EP_LOG_TO_ADDR_OUT(ep_log_nbr));
			/* DMA Case 1, Checked */
			if(STM32F_FS_USE_DMA == DEF_ENABLED)
			{
				p_drv_data->EP_PktXferLen[ep_phy_nbr] =  p_drv_data->EP_MaxPktSize[ep_phy_nbr] - (p_reg->DOEP[ep_log_nbr].TSIZx & STM32F_FS_DOEPTSIZx_XFRSIZ_MSK);
				p_drv_data->EP_AppBufPtr[ep_phy_nbr] += p_drv_data->EP_MaxPktSize[ep_phy_nbr];
			}

			USBD_EP_RxCmpl(p_drv, ep_log_nbr);

			/* DMA Case 2, Checked */
			if(STM32F_FS_USE_DMA == DEF_ENABLED)
			{
               if((ep_log_nbr == 0U) && (p_drv_data->EP_PktXferLen[ep_phy_nbr] == 0U))
               {
                  /* this is ZLP, so prepare EP0 for next setup */
            	  STM32_EP0_OutStart(p_reg, 1, &p_drv_data->EP_SetupBuf[0u]);
               }
             }

                                                                    /* Clear EP transfer complete interrupt                 */
            DEF_BIT_SET(p_reg->DOEP[ep_log_nbr].INTx, STM32F_FS_DOEPINTx_BIT_XFRC);
        }
                                                                /* Check if EP Setup phase done interrupt occurred      */
        if (DEF_BIT_IS_SET(ep_int_stat, STM32F_FS_DOEPINTx_BIT_STUP))
        {
            USBD_EventSetup(p_drv, (void *)&p_drv_data->EP_SetupBuf[0u]);
                                                                /* Clear EP0 SETUP complete interrupt                   */
            DEF_BIT_SET(p_reg->DOEP[ep_log_nbr].INTx, STM32F_FS_DOEPINTx_BIT_STUP);

                                                                /* Re-enable EP for next setup pkt.                     */
            DEF_BIT_SET(p_reg->DOEP[0u].CTLx, STM32F_FS_DxEPCTLx_BIT_EPENA);
        }

        DEF_BIT_SET(p_reg->DOEP[ep_log_nbr].CTLx, STM32F_FS_DxEPCTLx_BIT_SNAK);

        dev_ep_int = p_reg->DAINT >> 16u;                       /* Read all Device OUT Endpoint interrupt               */
    }
}

/*
*********************************************************************************************************
*                                        STM32_EP_InProcess()
*
* Description : Process IN interrupts on associated EP.
*
* Arguments   : p_drv       Pointer to device driver structure.
*
* Return(s)   : none.
*
* Caller(s)   : USBD_DrvISR_Handler().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  STM32_EP_InProcess (USBD_DRV  *p_drv)
{
    CPU_INT08U           ep_log_nbr;
    CPU_INT32U           ep_int_stat;
    CPU_INT32U           dev_ep_int;
    USBD_STM32F_FS_REG  *p_reg;
    USBD_DRV_DATA_EP    *p_drv_data;
    CPU_INT08U           ep_phy_nbr;


    p_reg      = (USBD_STM32F_FS_REG *)p_drv->CfgPtr->BaseAddr;
    dev_ep_int = (p_reg->DAINT & 0x0000FFFFu);                   /* Read all Device IN Endpoint interrupt                */

    while (dev_ep_int != DEF_BIT_NONE)
    {
        ep_log_nbr  = (CPU_INT08U)(31u - CPU_CntLeadZeros32(dev_ep_int & 0x0000FFFFu));
        ep_int_stat =  p_reg->DIEP[ep_log_nbr].INTx;             /* Read IN EP interrupt status                          */

                                                                /* Check if EP transfer completed interrupt occurred    */
        if (DEF_BIT_IS_SET(ep_int_stat, STM32F_FS_DIEPINTx_BIT_XFRC))
        {
          	/* DMA Case 2, Checked */
			if(STM32F_FS_USE_DMA == DEF_ENABLED)
			{
				p_drv_data->EP_AppBufPtr[ep_phy_nbr] += p_drv_data->EP_MaxPktSize[ep_phy_nbr];
			}

        	USBD_EP_TxCmpl(p_drv, ep_log_nbr);

        	/* DMA Case 4, Checked */
    		if(STM32F_FS_USE_DMA == DEF_ENABLED)
    		{
               if( (ep_log_nbr == 0U) && (p_drv_data->EP_PktXferLen[ep_phy_nbr] == 0U) )
               {
                  /* this is ZLP, so prepare EP0 for next setup */
            	  STM32_EP0_OutStart(p_reg, 1, &p_drv_data->EP_SetupBuf[0u]);
               }
            }

                                                                /* Clear EP transfer complete interrupt                 */
            DEF_BIT_SET(p_reg->DIEP[ep_log_nbr].INTx, STM32F_FS_DIEPINTx_BIT_XFRC);
        }

        dev_ep_int = (p_reg->DAINT & 0x0000FFFFu);             /* Read all Device IN Endpoint interrupt                */
    }
}

static void STM32_USBD_LL_Reset(USBD_DRV     *p_drv)
{
  /* Open EP0 OUT */
	USBD_ERR err;
	USBD_DrvEP_Open(p_drv, 0x00, USBD_EP_TYPE_CTRL, STM32F_FS_TXFIFO_EP0_SIZE, 0, &err);

  /* Open EP0 IN */
	USBD_DrvEP_Open(p_drv, 0x80, USBD_EP_TYPE_CTRL, 64u /*STM32F_FS_RXFIFO_SIZE*/, 0, &err);

}

static CPU_BOOLEAN STM32_ActivateSetup (USBD_STM32F_FS_REG *p_reg)
{
  /* Set the MPS of the IN EP based on the enumeration speed */
	p_reg->DIEP[0].CTLx &= ~STM32F_FS_DxEPCTLx_MPSIZ_MASK;


  if((p_reg->DSTS & STM32F_FS_DSTS_ENUMSPD_MASK) == STM32F_FS_DSTS_ENUMSPD_LS_PHY_6MHZ)
  {
	  p_reg->DIEP[0].CTLx |= 3U;
  }

  p_reg->DCTL |= STM32F_FS_DCTL_BIT_CGINAK;


  return DEF_OK;

}


static CPU_BOOLEAN STM32_SetRxFiFo(USBD_STM32F_FS_REG *p_reg, CPU_INT16U size)
{
  p_reg->GRXFSIZ = size;

  return DEF_OK;
}

static CPU_BOOLEAN STM32_SetTxFiFo(USBD_STM32F_FS_REG *p_reg, CPU_INT08U fifo, CPU_INT16U size)
{
	CPU_INT08U i = 0U;
	CPU_INT32U Tx_Offset = 0U;

  /*  TXn min size = 16 words. (n  : Transmit FIFO index)
      When a TxFIFO is not used, the Configuration should be as follows:
          case 1 :  n > m    and Txn is not used    (n,m  : Transmit FIFO indexes)
         --> Txm can use the space allocated for Txn.
         case2  :  n < m    and Txn is not used    (n,m  : Transmit FIFO indexes)
         --> Txn should be configured with the minimum space of 16 words
     The FIFO is used optimally when used TxFIFOs are allocated in the top
         of the FIFO.Ex: use EP1 and EP2 as IN instead of EP1 and EP3 as IN ones.
     When DMA is used 3n * FIFO locations should be reserved for internal DMA registers */

  Tx_Offset = p_reg->GRXFSIZ;

  if(fifo == 0U)
  {
	  p_reg->DIEPTXF0 = (CPU_INT32U)(((CPU_INT32U)size << 16U) | Tx_Offset);
  }
  else
  {
    Tx_Offset += (p_reg->DIEPTXF0) >> 16U;
    for (i = 0U; i < (fifo - 1U); i++)
    {
      Tx_Offset += (p_reg->DIEPTXFx[i] >> 16U);
    }

    /* Multiply Tx_Size by 2 to get higher performance */
    p_reg->DIEPTXFx[fifo - 1U] = (CPU_INT32U)(((CPU_INT32U)size << 16U) | Tx_Offset);
  }

  return DEF_OK;
}

static CPU_BOOLEAN STM32_TxFifoFlush (USBD_STM32F_FS_REG*	p_reg, CPU_INT32U num )
{
	CPU_INT32U count = 0U;

  p_reg->GRSTCTL = ( STM32F_FS_GRSTCTL_BIT_TXFFLSH |(CPU_INT32U)( num << 6));

  do
  {
    if (++count > 200000U)
    {
      return DEF_FAIL;
    }
  }
  while ((p_reg->GRSTCTL & STM32F_FS_GRSTCTL_BIT_TXFFLSH) == STM32F_FS_GRSTCTL_BIT_TXFFLSH);

  return DEF_OK;
}

static CPU_BOOLEAN STM32_RxFifoFlush(USBD_STM32F_FS_REG* p_reg)
{
	CPU_INT32U count = 0U;

	p_reg->GRSTCTL = STM32F_FS_GRSTCTL_BIT_RXFFLSH;

  do
  {
    if (++count > 200000U)
    {
      return DEF_FAIL;
    }
  }
  while ((p_reg->GRSTCTL & STM32F_FS_GRSTCTL_BIT_RXFFLSH) == STM32F_FS_GRSTCTL_BIT_RXFFLSH);

  return DEF_OK;
}



static CPU_BOOLEAN STM32_EP0_OutStart(USBD_STM32F_FS_REG* p_reg, CPU_BOOLEAN dma, CPU_INT32U* psetup)
{
	p_reg->DOEP[0].TSIZx = 0u;
	p_reg->DOEP[0].TSIZx  |= (STM32F_FS_DOEPTSIZx_PKTCNT_MSK & (1U << 19U)) ;
	p_reg->DOEP[0].TSIZx  |= (3U * 8U);
	p_reg->DOEP[0].TSIZx  |=  STM32F_FS_DOEPTSIZx_STUPCNT_3_PKT;

  if (dma == DEF_ENABLED)
  {
	  p_reg->DOEP[0].DMAx = (CPU_REG32)psetup;
	  /* EP enable */
	  p_reg->DOEP[0].CTLx = 0x80008000U;
  }

  return DEF_OK;
}

