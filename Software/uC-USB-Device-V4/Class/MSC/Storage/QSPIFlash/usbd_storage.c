/*
*********************************************************************************************************
*                                            uC/USB-Device
*                                       The Embedded USB Stack
*
*                         (c) Copyright 2004-2014; Micrium, Inc.; Weston, FL
*
*                  All rights reserved.  Protected by international copyright laws.
*
*                  uC/USB-Device is provided in source form to registered licensees ONLY.  It is
*                  illegal to distribute this source code to any third party unless you receive
*                  written permission by an authorized Micrium representative.  Knowledge of
*                  the source code may NOT be used to develop a similar product.
*
*                  Please help us continue to provide the Embedded community with the finest
*                  software available.  Your honesty is greatly appreciated.
*
*                  You can find our product's user manual, API reference, release notes and
*                  more information at: https://doc.micrium.com
*
*                  You can contact us at: http://www.micrium.com
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                 USB DEVICE MSC CLASS STORAGE DRIVER
*
*                                              QSPI_FLASH
*
* File           : usbd_storage.c
* Version        : V4.05.00
* Programmer(s)  :
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                              INCLUDE FILES
*********************************************************************************************************
*/
#ifdef QSPIFLASH_STORAGE
#define    MICRIUM_SOURCE
#include   "usbd_storage.h"
#include "stm32f4xx_hal.h"
#include "../../../bsp/bsp.h"
#include "string.h"
#include "bsp.h"
#include "stdbool.h"

/*********************************************************************************************************
*                                              LOCAL DEFINES
*********************************************************************************************************
*/

#define MAX_WRITE_RETRIES 1

#define  USBD_QSPI_FLASH_SIZE     (USBD_QSPI_FLASH_CFG_BLK_SIZE * \
                                  USBD_QSPI_FLASH_CFG_NBR_BLKS)

#define USBD_QSPI_FLASH_CFG_NBR_UNITS	1

#define MAX_WAIT_FOR_QSPI_OP			10000000

#define QSPI_FLASH_SUBSECTOR_SIZE		0x1000

#define USE_DMA

#ifdef STM32446E_EVAL
#define MEMORY_TYPE_N25Q256
#endif

#ifdef SUBWAY_BREADBOARD
#define MEMORY_TYPE_IS25LP128
#endif // SUBWAY_BREADBOARD

#ifdef SUBWAY_PRODUCT
#define MEMORY_TYPE_IS25LP128
#endif


/* Definition for QSPI Pins */
#ifdef MEMORY_TYPE_N25Q256

/* N25Q256A13EF840E Micron memory */

#define QSPI_FLASH_SIZE                      24
#define  USBD_QSPI_FLASH_CFG_NBR_BLKS      32768u

/* Definition for QSPI clock resources */
#define QSPI_CLK_ENABLE()          __HAL_RCC_QSPI_CLK_ENABLE()
#define QSPI_CLK_DISABLE()         __HAL_RCC_QSPI_CLK_DISABLE()
#define QSPI_CS_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOG_CLK_ENABLE()
#define QSPI_CLK_GPIO_CLK_ENABLE() __HAL_RCC_GPIOD_CLK_ENABLE()
#define QSPI_D0_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOF_CLK_ENABLE()
#define QSPI_D1_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOF_CLK_ENABLE()
#define QSPI_D2_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOF_CLK_ENABLE()
#define QSPI_D3_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOF_CLK_ENABLE()
#define QSPI_DMA_CLK_ENABLE()      __HAL_RCC_DMA2_CLK_ENABLE()

#define QSPI_FORCE_RESET()         __HAL_RCC_QSPI_FORCE_RESET()
#define QSPI_RELEASE_RESET()       __HAL_RCC_QSPI_RELEASE_RESET()

#define QSPI_FLASH_ID			   QSPI_FLASH_ID_1
#define QSPI_CS_PIN                GPIO_PIN_6
#define QSPI_CS_GPIO_PORT          GPIOG
#define QSPI_CS_AF				   GPIO_AF10_QSPI
#define QSPI_CLK_PIN               GPIO_PIN_3
#define QSPI_CLK_GPIO_PORT         GPIOD
#define QSPI_CLK_AF				   GPIO_AF9_QSPI
#define QSPI_D0_PIN                GPIO_PIN_8
#define QSPI_D0_GPIO_PORT          GPIOF
#define QSPI_D0_AF				   GPIO_AF10_QSPI
#define QSPI_D1_PIN                GPIO_PIN_9
#define QSPI_D1_GPIO_PORT          GPIOF
#define QSPI_D1_AF				   GPIO_AF10_QSPI
#define QSPI_D2_PIN                GPIO_PIN_7
#define QSPI_D2_GPIO_PORT          GPIOF
#define QSPI_D2_AF				   GPIO_AF9_QSPI
#define QSPI_D3_PIN                GPIO_PIN_6
#define QSPI_D3_GPIO_PORT          GPIOF
#define QSPI_D3_AF				   GPIO_AF9_QSPI


/* Definition for QSPI DMA */
#define QSPI_DMA_INSTANCE          DMA2_Stream7
#define QSPI_DMA_CHANNEL           DMA_CHANNEL_3
#define QSPI_DMA_IRQ               DMA2_Stream7_IRQn
#define QSPI_DMA_IRQ_HANDLER       DMA2_Stream7_IRQHandler

#define QSPI_MEM_READY_TIMEOUT		5000

#endif

#ifdef MEMORY_TYPE_N25Q128

#define QSPI_FLASH_SIZE                      23
#define  USBD_QSPI_FLASH_CFG_NBR_BLKS      32768u


	  /*
	   *     PC11     ------> QUADSPI_BK2_NCS
    PG9     ------> QUADSPI_BK2_IO2
    PA7     ------> QUADSPI_CLK
    PE8     ------> QUADSPI_BK2_IO1
    PH2     ------> QUADSPI_BK2_IO0
    PE10     ------> QUADSPI_BK2_IO3
	   */


#define QSPI_FLASH_ID			   QSPI_FLASH_ID_2

/* Definition for QSPI clock resources */
#define QSPI_CLK_ENABLE()          __HAL_RCC_QSPI_CLK_ENABLE()
#define QSPI_CLK_DISABLE()         __HAL_RCC_QSPI_CLK_DISABLE()
#define QSPI_CS_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOC_CLK_ENABLE()
#define QSPI_CLK_GPIO_CLK_ENABLE() __HAL_RCC_GPIOA_CLK_ENABLE()
#define QSPI_D0_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOH_CLK_ENABLE()
#define QSPI_D1_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOE_CLK_ENABLE()
#define QSPI_D2_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOG_CLK_ENABLE()
#define QSPI_D3_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOE_CLK_ENABLE()

#define QSPI_DMA_CLK_ENABLE()      __HAL_RCC_DMA2_CLK_ENABLE()

#define QSPI_FORCE_RESET()         __HAL_RCC_QSPI_FORCE_RESET()
#define QSPI_RELEASE_RESET()       __HAL_RCC_QSPI_RELEASE_RESET()


#define QSPI_CS_PIN                GPIO_PIN_11
#define QSPI_CS_GPIO_PORT          GPIOC
#define QSPI_CS_AF				   GPIO_AF9_QSPI
#define QSPI_CLK_PIN               GPIO_PIN_7
#define QSPI_CLK_GPIO_PORT         GPIOA
#define QSPI_CLK_AF				   GPIO_AF10_QSPI
#define QSPI_D0_PIN                GPIO_PIN_2
#define QSPI_D0_GPIO_PORT          GPIOH
#define QSPI_D0_AF				   GPIO_AF9_QSPI
#define QSPI_D1_PIN                GPIO_PIN_8
#define QSPI_D1_GPIO_PORT          GPIOE
#define QSPI_D1_AF				   GPIO_AF10_QSPI
#define QSPI_D2_PIN                GPIO_PIN_9
#define QSPI_D2_GPIO_PORT          GPIOG
#define QSPI_D2_AF				   GPIO_AF9_QSPI
#define QSPI_D3_PIN                GPIO_PIN_10
#define QSPI_D3_GPIO_PORT          GPIOE
#define QSPI_D3_AF				   GPIO_AF10_QSPI

/* Definition for QSPI DMA */
#define QSPI_DMA_INSTANCE          DMA2_Stream7
#define QSPI_DMA_CHANNEL           DMA_CHANNEL_3
#define QSPI_DMA_IRQ               DMA2_Stream7_IRQn
#define QSPI_DMA_IRQ_HANDLER       DMA2_Stream7_IRQHandler

#define QSPI_MEM_READY_TIMEOUT		1000000

#endif

#ifdef MEMORY_TYPE_IS25LP128

#define QSPI_FLASH_SIZE                      23
#define  USBD_QSPI_FLASH_CFG_NBR_BLKS      32768u


	  /*
	   *     PC11     ------> QUADSPI_BK2_NCS
    PG9     ------> QUADSPI_BK2_IO2
    PA7     ------> QUADSPI_CLK
    PE8     ------> QUADSPI_BK2_IO1
    PH2     ------> QUADSPI_BK2_IO0
    PE10     ------> QUADSPI_BK2_IO3
	   */


#define QSPI_FLASH_ID			   QSPI_FLASH_ID_2

/* Definition for QSPI clock resources */
#define QSPI_CLK_ENABLE()          __HAL_RCC_QSPI_CLK_ENABLE()
#define QSPI_CLK_DISABLE()         __HAL_RCC_QSPI_CLK_DISABLE()
#define QSPI_CS_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOC_CLK_ENABLE()
#define QSPI_CLK_GPIO_CLK_ENABLE() __HAL_RCC_GPIOA_CLK_ENABLE()
#define QSPI_D0_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOH_CLK_ENABLE()
#define QSPI_D1_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOE_CLK_ENABLE()
#define QSPI_D2_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOG_CLK_ENABLE()
#define QSPI_D3_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOE_CLK_ENABLE()

#define QSPI_DMA_CLK_ENABLE()      __HAL_RCC_DMA2_CLK_ENABLE()

#define QSPI_FORCE_RESET()         __HAL_RCC_QSPI_FORCE_RESET()
#define QSPI_RELEASE_RESET()       __HAL_RCC_QSPI_RELEASE_RESET()


#define QSPI_CS_PIN                GPIO_PIN_11
#define QSPI_CS_GPIO_PORT          GPIOC
#define QSPI_CS_AF				   GPIO_AF9_QSPI
#define QSPI_CLK_PIN               GPIO_PIN_7
#define QSPI_CLK_GPIO_PORT         GPIOA
#define QSPI_CLK_AF				   GPIO_AF10_QSPI
#define QSPI_D0_PIN                GPIO_PIN_2
#define QSPI_D0_GPIO_PORT          GPIOH
#define QSPI_D0_AF				   GPIO_AF9_QSPI
#define QSPI_D1_PIN                GPIO_PIN_8
#define QSPI_D1_GPIO_PORT          GPIOE
#define QSPI_D1_AF				   GPIO_AF10_QSPI
#define QSPI_D2_PIN                GPIO_PIN_9
#define QSPI_D2_GPIO_PORT          GPIOG
#define QSPI_D2_AF				   GPIO_AF9_QSPI
#define QSPI_D3_PIN                GPIO_PIN_10
#define QSPI_D3_GPIO_PORT          GPIOE
#define QSPI_D3_AF				   GPIO_AF10_QSPI

/* Definition for QSPI DMA */
#define QSPI_DMA_INSTANCE          DMA2_Stream7
#define QSPI_DMA_CHANNEL           DMA_CHANNEL_3
#define QSPI_DMA_IRQ               DMA2_Stream7_IRQn
#define QSPI_DMA_IRQ_HANDLER       DMA2_Stream7_IRQHandler

#define QSPI_MEM_READY_TIMEOUT		1000000

#endif

#ifdef MEMORY_TYPE_IS25LP128
#define QSPI_FLASH_PAGE_SIZE                 256

#define  USBD_QSPI_FLASH_CFG_BLK_SIZE		512u

#define  USBD_QSPI_FLASH_CFG_BASE_ADDR        0u

/* Reset Operations */
#define RESET_ENABLE_CMD                     0x66
#define RESET_MEMORY_CMD                     0x99

/* Identification Operations */
#define READ_ID_CMD                          0x9E
#define READ_ID_CMD2                         0x9F
#define MULTIPLE_IO_READ_ID_CMD              0xAF
#define READ_SERIAL_FLASH_DISCO_PARAM_CMD    0x5A

/* Read Operations */
#define READ_CMD                             0x03
#define READ_4_BYTE_ADDR_CMD                 0x13

#define FAST_READ_CMD                        0x0B
#define FAST_READ_DTR_CMD                    0x0D
#define FAST_READ_4_BYTE_ADDR_CMD            0x0C

#define DUAL_OUT_FAST_READ_CMD               0x3B
#define DUAL_OUT_FAST_READ_DTR_CMD           0x3D
#define DUAL_OUT_FAST_READ_4_BYTE_ADDR_CMD   0x3C

#define DUAL_INOUT_FAST_READ_CMD             0xBB
#define DUAL_INOUT_FAST_READ_DTR_CMD         0xBD
#define DUAL_INOUT_FAST_READ_4_BYTE_ADDR_CMD 0xBC

#define QUAD_OUT_FAST_READ_CMD               0x6B
#define QUAD_OUT_FAST_READ_DTR_CMD           0x6D
#define QUAD_OUT_FAST_READ_4_BYTE_ADDR_CMD   0x6C

#define QUAD_INOUT_FAST_READ_CMD             0xEB
#define QUAD_INOUT_FAST_READ_DTR_CMD         0xED
#define QUAD_INOUT_FAST_READ_4_BYTE_ADDR_CMD 0xEC

/* Write Operations */
#define WRITE_ENABLE_CMD                     0x06
#define WRITE_DISABLE_CMD                    0x04

/* Register Operations */
#define READ_STATUS_REG_CMD                  0x05
#define WRITE_STATUS_REG_CMD                 0x01

#define READ_LOCK_REG_CMD                    0xE8
#define WRITE_LOCK_REG_CMD                   0xE5

#define READ_FLAG_STATUS_REG_CMD             0x70
#define CLEAR_FLAG_STATUS_REG_CMD            0x50

#define READ_NONVOL_CFG_REG_CMD              0xB5
#define WRITE_NONVOL_CFG_REG_CMD             0xB1

#define READ_VOL_CFG_REG_CMD                 0x85
#define WRITE_VOL_CFG_REG_CMD                0x81

#define READ_ENHANCED_VOL_CFG_REG_CMD        0x65
#define WRITE_ENHANCED_VOL_CFG_REG_CMD       0x61

#define READ_EXT_ADDR_REG_CMD                0xC8
#define WRITE_EXT_ADDR_REG_CMD               0xC5

/* Program Operations */
#define PAGE_PROG_CMD                        0x02
#define PAGE_PROG_4_BYTE_ADDR_CMD            0x12

#define DUAL_IN_FAST_PROG_CMD                0xA2
#define EXT_DUAL_IN_FAST_PROG_CMD            0xD2

#define QUAD_IN_FAST_PROG_CMD                0x32
#define EXT_QUAD_IN_FAST_PROG_CMD            0x02 /*0x38*/
#define QUAD_IN_FAST_PROG_4_BYTE_ADDR_CMD    0x34

/* Erase Operations */
#define SUBSECTOR_ERASE_CMD                  0x20
#define SUBSECTOR_ERASE_4_BYTE_ADDR_CMD      0x21

#define SECTOR_ERASE_CMD                     0xD8
#define SECTOR_ERASE_4_BYTE_ADDR_CMD         0xDC

#define BULK_ERASE_CMD                       0xC7

#define PROG_ERASE_RESUME_CMD                0x7A
#define PROG_ERASE_SUSPEND_CMD               0x75

/* One-Time Programmable Operations */
#define READ_OTP_ARRAY_CMD                   0x4B
#define PROG_OTP_ARRAY_CMD                   0x42

/* 4-byte Address Mode Operations */
#define ENTER_4_BYTE_ADDR_MODE_CMD           0xB7
#define EXIT_4_BYTE_ADDR_MODE_CMD            0xE9

/* Quad Operations */
#define ENTER_QUAD_CMD                       0x35
#define EXIT_QUAD_CMD                        0xF5

/* Default dummy clocks cycles */
#define DUMMY_CLOCK_CYCLES_READ              8
#define DUMMY_CLOCK_CYCLES_READ_QUAD         6

#define DUMMY_CLOCK_CYCLES_READ_DTR          6
#define DUMMY_CLOCK_CYCLES_READ_QUAD_DTR     8
#endif

#ifdef MEMORY_TYPE_N25Q128
#define QSPI_FLASH_PAGE_SIZE                 256

#define  USBD_QSPI_FLASH_CFG_BLK_SIZE		512u

#define  USBD_QSPI_FLASH_CFG_BASE_ADDR        0u

/* Reset Operations */
#define RESET_ENABLE_CMD                     0x66
#define RESET_MEMORY_CMD                     0x99

/* Identification Operations */
#define READ_ID_CMD                          0x9E
#define READ_ID_CMD2                         0x9F
#define MULTIPLE_IO_READ_ID_CMD              0xAF
#define READ_SERIAL_FLASH_DISCO_PARAM_CMD    0x5A

/* Read Operations */
#define READ_CMD                             0x03
#define READ_4_BYTE_ADDR_CMD                 0x13

/* Write Operations */
#define WRITE_ENABLE_CMD                     0x06
#define WRITE_DISABLE_CMD                    0x04

/* Register Operations */
#define READ_STATUS_REG_CMD                  0x05
#define WRITE_STATUS_REG_CMD                 0x01

#define READ_VOL_CFG_REG_CMD                 0x85
#define WRITE_VOL_CFG_REG_CMD                0x81

/* N25Q128A11E Micron memory */

#define SECTOR_ERASE_CMD                     0xD8
#define SUBSECTOR_ERASE_CMD					 0x20
#define EXT_QUAD_IN_FAST_PROG_CMD            0x12 /*0x38*/
#define QUAD_INOUT_FAST_READ_CMD             0xEB

/* Default dummy clocks cycles */
#define DUMMY_CLOCK_CYCLES_READ              8
#define DUMMY_CLOCK_CYCLES_READ_QUAD         10

#define DUMMY_CLOCK_CYCLES_READ_DTR          6
#define DUMMY_CLOCK_CYCLES_READ_QUAD_DTR     8

/* Quad Operations */
#define ENTER_QUAD_CMD                       0x35
#define EXIT_QUAD_CMD                        0xF5

#endif
/* End address of the QSPI memory */
#define QSPI_END_ADDR              (1 << QSPI_FLASH_SIZE)


/*
*********************************************************************************************************
*                                             LOCAL CONSTANTS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                            LOCAL DATA TYPES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                              LOCAL TABLES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                         LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/
bool G_NVM_in_write_op = false;
QSPI_HandleTypeDef 	QSPIHandle;
#ifdef USE_DMA
DMA_HandleTypeDef hdma;
#endif

QSPI_CommandTypeDef sCommand;
CPU_INT08U 			StatusMatch, CmdCplt, RxCplt, TxCplt;

CPU_INT08U			subSectorCopy[QSPI_FLASH_SUBSECTOR_SIZE];
/*
*********************************************************************************************************
*                                        LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/
static	CPU_BOOLEAN QSPI_DummyCyclesCfg			(QSPI_HandleTypeDef *hqspi);
		void		HAL_QSPI_MspInit			(QSPI_HandleTypeDef *hqspi);
		void		HAL_QSPI_MspDeInit			(QSPI_HandleTypeDef *hqspi);
static 	CPU_BOOLEAN QSPI_WriteEnable			(QSPI_HandleTypeDef *hqspi, CPU_BOOLEAN use_inst_single_line);
static 	void 		QUADSPI_IRQHandler			(void);
static	void		QUADSPI_DMA_IRQ_HANDLER		(void);
static 	CPU_BOOLEAN	QSPI_AutoPollingMemReady	(QSPI_HandleTypeDef *hqspi);
		CPU_BOOLEAN QSPI_Wait_For_Flag(CPU_INT08U* flag);

static	void  		USBD_StorageRdSubSector (USBD_STORAGE_LUN  *p_storage_lun,
		                      CPU_INT32U         subSectorNum,
		                      CPU_INT08U        *p_data_buf,
		                      USBD_ERR          *p_err);
		void 		MX_GPIO_Init(void);
		void		MX_DMA_Init(void);
static 	CPU_BOOLEAN ENABLE_ENTER_QUAD_PERIPHERAL_INTERFACE(QSPI_HandleTypeDef *hqspi);
static 	CPU_BOOLEAN SINGLE_SOFTWARE_RESET_RESET_ENABLE(QSPI_HandleTypeDef *hqspi);


/*
*********************************************************************************************************
*                                       LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*********************************************************************************************************
*                                          GLOBAL FUNCTIONS
*********************************************************************************************************
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                              USBD_StorageInit()
*
* Description : Initialize the storage medium.
*
* Argument(s) : p_err       Pointer to variable that will receive error code from this function.
*
*                                USBD_ERR_NONE                  Storage successfully initialized.
*                                USBD_ERR_SCSI_LOG_UNIT_NOTRDY  Initiliazing RAM area failed.
*
* Return(s)   : None.
*
* Caller(s)   : USBD_SCSI_Init().
*
* Note(s)     : None.
*********************************************************************************************************
*/
#define BUFFERSIZE     256
uint8_t aTxBuffer[] = " ****QSPI communication based on DMA****  ****QSPI communication based on DMA****  ****QSPI communication based on DMA****  ****QSPI communication based on DMA****  ****QSPI communication based on DMA****  ****QSPI communication based on DMA**** ";

/* Buffer used for reception */
uint8_t aRxBuffer[BUFFERSIZE];

void  USBD_StorageInit (USBD_ERR  *p_err)
{
	  MX_GPIO_Init();
	  MX_DMA_Init();

	  // zeev instd of MX_QUADSPI_Init
	  /* Initialize QuadSPI ------------------------------------------------------ */
	  QSPIHandle.Instance = QUADSPI;
//	  HAL_QSPI_DeInit(&QSPIHandle);

	  QSPIHandle.Init.ClockPrescaler     = 1;
	  QSPIHandle.Init.FifoThreshold      = 4;
	  QSPIHandle.Init.SampleShifting     = QSPI_SAMPLE_SHIFTING_HALFCYCLE;
	  QSPIHandle.Init.FlashSize          = QSPI_FLASH_SIZE;
	  QSPIHandle.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE;
	  QSPIHandle.Init.ClockMode          = QSPI_CLOCK_MODE_0;
	  QSPIHandle.Timeout				 = QSPI_MEM_READY_TIMEOUT;
#ifdef SUBWAY_BREADBOARD
	  QSPIHandle.Init.FlashID 			 = QSPI_FLASH_ID;
	  QSPIHandle.Init.DualFlash 		 = QSPI_DUALFLASH_DISABLE;
#endif // SUBWAY_BREADBOARD
#ifdef SUBWAY_PRODUCT
	  QSPIHandle.Init.FlashID 			 = QSPI_FLASH_ID;
	  QSPIHandle.Init.DualFlash 		 = QSPI_DUALFLASH_DISABLE;
#endif // SUBWAY_PRODUCT
	  if (HAL_QSPI_Init(&QSPIHandle) != HAL_OK)
	  {
		 // APP_TRACE_DBG(("Failed HAL_QSPI_Init \n\r"));
		 *p_err = USBD_ERR_FAIL;
		 return;
	  }

	  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
	  sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
	  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
	  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;
	  //      sCommand.AddressMode 		 = QSPI_ADDRESS_4_LINES;
	  //      sCommand.DataMode    		 = QSPI_DATA_4_LINES;

#ifdef MEMORY_TYPE_IS25LP128
		QSPI_AutoPollingTypeDef sConfig;

		// Enable write operations ------------------------------------------
		sCommand.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
		sCommand.Instruction       = EXIT_QUAD_CMD;
		sCommand.AddressMode       = QSPI_ADDRESS_NONE;
		sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
		sCommand.DataMode          = QSPI_DATA_NONE;
		sCommand.DummyCycles       = 0;
		sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
		sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
		sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

		if (HAL_QSPI_Command_IT(&QSPIHandle, &sCommand) != HAL_OK)
		{
		return;
		}

		//Configure automatic polling mode to wait for write enabling ----
		sConfig.Match           = 0x00;
		sConfig.Mask            = 0x40;
		sConfig.MatchMode       = QSPI_MATCH_MODE_AND;
		sConfig.StatusBytesSize = 1;
		sConfig.Interval        = 0x10;
		sConfig.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

		sCommand.Instruction    = READ_STATUS_REG_CMD;
		sCommand.DataMode       = QSPI_DATA_4_LINES;

		if (HAL_QSPI_AutoPolling_IT(&QSPIHandle, &sCommand, &sConfig) != HAL_OK)
		{
		  return;
		}

        /* Enable write operations ------------------------------------------- */
        QSPI_WriteEnable(&QSPIHandle, 1);

        // Set quad mode
        sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
   	  sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
   	  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
   	  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
   	  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
   	  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

   	  sCommand.Instruction = WRITE_STATUS_REG_CMD;
   	  sCommand.AddressMode = QSPI_ADDRESS_NONE;
   	  sCommand.DataMode    = QSPI_DATA_1_LINE;
   	  sCommand.NbData      = 1;

   	 if (HAL_QSPI_Command( &QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
   	 {
   		 return;
   	 }

   	 CPU_INT08U val = 0x40;

   	 if (HAL_QSPI_Transmit_DMA(&QSPIHandle, &val) != HAL_OK)
   	 {
   		 return;
   	 }

   	 // Read status reg to make sure we wrote )x40 correctly
      sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
	  sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
	  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
	  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	  sCommand.Instruction = READ_STATUS_REG_CMD;
	  sCommand.AddressMode = QSPI_ADDRESS_NONE;
	  sCommand.DataMode    = QSPI_DATA_1_LINE;
	  sCommand.NbData      = 1;

	 if (HAL_QSPI_Command( &QSPIHandle, &sCommand,HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  	 {
   		 return;
   	 }

	 val = 1;

	 if (HAL_QSPI_Transmit_DMA(&QSPIHandle, &val) != HAL_OK)
   	 {
   		 return;
   	 }

	 CPU_INT08U RecieveRegStatus;

	 if (HAL_QSPI_Receive_DMA(&QSPIHandle, &RecieveRegStatus) != HAL_OK)
   	 {
   		 return;
   	 }

	if (!SINGLE_SOFTWARE_RESET_RESET_ENABLE(&QSPIHandle))
	 {
		 return;
	 }

	if (!ENABLE_ENTER_QUAD_PERIPHERAL_INTERFACE(&QSPIHandle))
  	 {
  		 return;
  	 }

#endif

   	 #if 0
	  CPU_INT32U address = 0;
	  CPU_INT16U index;
	   CPU_INT08U step = 0;


	  while(1)
	  {
		  HAL_Delay(1000);
	    switch(step)
	    {
	      case 0:
	        CmdCplt = 0;

	        /* Initialize Reception buffer --------------------------------------- */
	        for (index = 0; index < BUFFERSIZE; index++)
	        {
	          aRxBuffer[index] = 0;
	        }

	        /* Enable write operations ------------------------------------------- */
	        QSPI_WriteEnable(&QSPIHandle, 1);

	        /* Erasing Sequence -------------------------------------------------- */
	        sCommand.Instruction = SUBSECTOR_ERASE_CMD;
	        sCommand.AddressMode = QSPI_ADDRESS_1_LINE;
	        sCommand.Address     = address;
	        sCommand.DataMode    = QSPI_DATA_NONE;
	        sCommand.DummyCycles = 0;

	        if (HAL_QSPI_Command_IT(&QSPIHandle, &sCommand) != HAL_OK)
	        {
	        	return;
	        }

	        step++;
	        break;

	      case 1:
	        if(CmdCplt != 0)
	        {
	          CmdCplt = 0;
	          StatusMatch = 0;

	          /* Configure automatic polling mode to wait for end of erase ------- */
	          QSPI_AutoPollingMemReady(&QSPIHandle);

	          step++;
	        }
	        break;

	      case 2:
	        if(StatusMatch != 0)
	        {
	          StatusMatch = 0;
	          TxCplt = 0;

	          /* Enable write operations ----------------------------------------- */
	          QSPI_WriteEnable(&QSPIHandle, 1);

	          /* Writing Sequence ------------------------------------------------ */
	          sCommand.Instruction = EXT_QUAD_IN_FAST_PROG_CMD;
	          sCommand.AddressMode = QSPI_ADDRESS_4_LINES;
	          sCommand.DataMode    = QSPI_DATA_4_LINES;
	          sCommand.NbData      = BUFFERSIZE;

	          if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	          {
	        	  return;
	          }

	          if (HAL_QSPI_Transmit_DMA(&QSPIHandle, aTxBuffer) != HAL_OK)
	          {
	        	  return;
	          }

	          step++;
	        }
	        break;

	      case 3:
	        if(TxCplt != 0)
	        {
	          TxCplt = 0;
	          StatusMatch = 0;

	          /* Configure automatic polling mode to wait for end of program ----- */
	          QSPI_AutoPollingMemReady(&QSPIHandle);

	          step++;
	        }
	        break;

	      case 4:
	        if(StatusMatch != 0)
	        {
	          StatusMatch = 0;
	          RxCplt = 0;

	          /* Configure Volatile Configuration register (with new dummy cycles) */
	          QSPI_DummyCyclesCfg(&QSPIHandle);

	          /* Reading Sequence ------------------------------------------------ */
	          sCommand.Instruction = QUAD_INOUT_FAST_READ_CMD;
	          sCommand.DummyCycles = DUMMY_CLOCK_CYCLES_READ_QUAD;

	          if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	          {
	        	  return;
	          }

	          if (HAL_QSPI_Receive_DMA(&QSPIHandle, aRxBuffer) != HAL_OK)
	          {
	        	  return;
	          }
	          step++;
	        }
	        break;

	      case 5:
	        if (RxCplt != 0)
	        {
	          RxCplt = 0;
	          int fail = 0;

	          /* Result comparison ----------------------------------------------- */
	          for (index = 0; index < BUFFERSIZE; index++)
	          {
	            if (aRxBuffer[index] != aTxBuffer[index])
	            {
	                // APP_TRACE_DBG(("Fail QSPI read/write \n\r"));
	                fail = 1;
	            }
	          }

	          if (!fail)
	        	  // APP_TRACE_DBG(("QSPI read/write OK \n\r"));

	          address += QSPI_FLASH_PAGE_SIZE;
	          if(address >= QSPI_END_ADDR)
	          {
	            address = 0;
	          }
	          step = 0;
	        }
	        break;

	      default :
	    	  return;
	    }
	  }
#endif
	  *p_err = USBD_ERR_NONE;


}

/*
*********************************************************************************************************
*                                              USBD_StorageAdd()
*
* Description : Initialize storage medium.
*
* Argument(s) : p_storage_lun    Pointer to the logical unit storage structure.
*
*               p_err       Pointer to variable that will receive error code from this function.
*
*                                USBD_ERR_NONE                  Storage successfully initialized.
*                                USBD_ERR_SCSI_LOG_UNIT_NOTRDY  Initiliazing RAM area failed.
*
* Return(s)   : None.
*
* Caller(s)   : USBD_SCSI_LunAdd().
*
* Note(s)     : None.
*********************************************************************************************************
*/

void  USBD_StorageAdd (USBD_STORAGE_LUN  *p_storage_lun,
                       USBD_ERR          *p_err)
{
    CPU_INT08U  lun_nbr;

    lun_nbr = p_storage_lun->LunNbr;
    p_storage_lun->MediumPresent = DEF_TRUE;                    /* QSPI FLASH medium is initially always present.          */
   *p_err                        = USBD_ERR_NONE;
}

/*
*********************************************************************************************************
*                                          USBD_StorageCapacityGet()
*
* Description : Get the capacity of the storage medium.
*
* Argument(s) : p_storage_lun    Pointer to the logical unit storage structure.
*
*               p_nbr_blks       Pointer to variable that will receive the number of logical blocks.
*
*               p_blk_size       Pointer to variable that will receive the size of each block, in bytes.
*
*               p_err       Pointer to variable that will receive error code from this function.
*
*                                USBD_ERR_NONE      Medium capacity successfully gotten.
*
* Return(s)   : None.
*
* Caller(s)   : USBD_SCSI_ProcessCmd().
*
* Note(s)     : None.
*********************************************************************************************************
*/

void  USBD_StorageCapacityGet (USBD_STORAGE_LUN  *p_storage_lun,
                               CPU_INT64U        *p_nbr_blks,
                               CPU_INT32U        *p_blk_size,
                               USBD_ERR          *p_err)
{
	(void)&p_storage_lun;

	*p_nbr_blks = USBD_QSPI_FLASH_CFG_NBR_BLKS;
	*p_blk_size = USBD_QSPI_FLASH_CFG_BLK_SIZE;
	*p_err      = USBD_ERR_NONE;
}


/*
*********************************************************************************************************
*                                               USBD_StorageRd()
*
* Description : Read the data from the storage medium.
*
* Argument(s) : p_storage_lun    Pointer to the logical unit storage structure.
*
*               blk_addr         Logical Block Address (LBA) of starting read block.
*
*               nbr_blks         Number of logical blocks to read.
*
*               p_data_buf      Pointer to buffer in which data will be stored.
*
*               p_err       Pointer to variable that will receive error code from this function.
*
*                               USBD_ERR_NONE                           Medium successfully read.
*                               USBD_ERR_SCSI_LOG_UNIT_NOTSUPPORTED     Logical unit not supported.
*                               USBD_ERR_SCSI_LOG_UNIT_NOTRDY           Logical unit cannot perform
*                                                                           operations.
*
* Return(s)   : None.
*
* Caller(s)   : USBD_SCSI_RdData().
*
* Note(s)     : None.
*********************************************************************************************************
*/
CPU_INT08U testBuff[512];

static int err_point = 0;
void  USBD_StorageRd (USBD_STORAGE_LUN  *p_storage_lun,
                      CPU_INT64U         blk_addr,
                      CPU_INT32U         nbr_blks,
                      CPU_INT08U        *p_data_buf,
                      USBD_ERR          *p_err)
{

    CPU_INT08U  lun;
    CPU_INT64U  mem_area_size;
    CPU_INT32U  mem_size_read;

 	CPU_INT08U bn = blk_addr;
    // APP_TRACE_DBG(("USBD_StorageRd blk_addr=%d\n\r", bn));

    lun = (*p_storage_lun).LunNbr;

     if (lun > USBD_QSPI_FLASH_CFG_NBR_UNITS)
     {
        *p_err = USBD_ERR_SCSI_LU_NOTSUPPORTED;
        err_point = 1;
        // APP_TRACE_DBG(("USBD_ERR_SCSI_LU_NOTSUPPORTED in USBD_StorageRd \n\r"));
         return;
     }

     mem_area_size = blk_addr + (nbr_blks * USBD_QSPI_FLASH_CFG_BLK_SIZE);
     if (mem_area_size > USBD_QSPI_FLASH_SIZE)
     {
         err_point = 1;
        *p_err = USBD_ERR_SCSI_LU_NOTRDY;
        // APP_TRACE_DBG(("USBD_ERR_SCSI_LU_NOTRDY in USBD_StorageRd \n\r"));
         return;
     }

     mem_size_read = nbr_blks * USBD_QSPI_FLASH_CFG_BLK_SIZE;
     /* Configure automatic polling mode to wait for end of program ----- */
     if (!QSPI_AutoPollingMemReady(&QSPIHandle))
     {
         err_point = 3;
		 *p_err = USBD_ERR_FAIL;
	        // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd 1\n\r"));
		 return;
     }

//     size_t i=0;
//     size_t num_of_pages = nbr_blks * USBD_QSPI_FLASH_CFG_BLK_SIZE/QSPI_FLASH_PAGE_SIZE;
     size_t address = blk_addr * USBD_QSPI_FLASH_CFG_BLK_SIZE;

	 RxCplt = 0;


	 /* Configure Volatile Configuration register (with new dummy cycles) */
#ifdef MICRON_QSPIFLASH_STORAGE
	 if (!QSPI_DummyCyclesCfg(&QSPIHandle))
	 {
	        err_point = 4;
		 *p_err = USBD_ERR_FAIL;
			// APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd 2\n\r"));
		 return;
	 }
#endif
	 /* Reading Sequence ------------------------------------------------ */
#ifdef MEMORY_TYPE_IS25LP128
	 sCommand.Instruction = FAST_READ_CMD;
	 sCommand.InstructionMode = QSPI_INSTRUCTION_4_LINES;
#else
	 sCommand.Instruction = QUAD_INOUT_FAST_READ_CMD;
#endif
	 sCommand.DummyCycles = DUMMY_CLOCK_CYCLES_READ_QUAD;
	 sCommand.NbData      = mem_size_read;
	 sCommand.Address     = address;
	 sCommand.AddressMode = QSPI_ADDRESS_4_LINES;
#ifdef USE_DMA
		 sCommand.DataMode    = QSPI_DATA_4_LINES;
#else
		 sCommand.DataMode    = QSPI_DATA_4_LINES;
#endif

	 if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	 {
	        err_point = 5;
		 *p_err = USBD_ERR_FAIL;
			// APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd 3\n\r"));
		 return;
	 }

#ifdef USE_DMA
	 if (HAL_QSPI_Receive_DMA(&QSPIHandle, p_data_buf) != HAL_OK)
#else
	 if (HAL_QSPI_Receive_IT(&QSPIHandle, p_data_buf) != HAL_OK)
#endif
	 {
	        err_point = 6;
		 *p_err = USBD_ERR_FAIL;
			// APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd 4\n\r"));
		 return;
	 }

	 // Wait for read command to complete
	 if (!QSPI_Wait_For_Flag(&RxCplt))
	 {
	        err_point = 7;
		 *p_err = USBD_ERR_FAIL;
			// APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd 5\n\r"));
		 return;
	 }

     *p_err = USBD_ERR_NONE;

//     if (!afterFirstWrite) return;
//	for (y=0; y<mem_size_read; y++)
//	{
//		// APP_TRACE_DBG(("%d ", p_data_buf[y]));
//	}
//	// APP_TRACE_DBG(("\n\r "));



}


/*
*********************************************************************************************************
*                                               USBD_StorageWr()
*
* Description : Write the data to the storage medium.
*
* Argument(s) : p_storage_lun    Pointer to the logical unit storage structure.
*
*               blk_addr         Logical Block Address (LBA) of starting write block.
*
*               nbr_blks         Number of logical blocks to write.
*
*               p-data_buf       Pointer to buffer in which data is stored.
*
*               p_err       Pointer to variable that will receive error code from this function.
*
*                               USBD_ERR_NONE                           Medium successfully written.
*                               USBD_ERR_SCSI_LOG_UNIT_NOTSUPPORTED     Logical unit not supported.
*                               USBD_ERR_SCSI_LOG_UNIT_NOTRDY           Logical unit cannot perform
*                                                                           operations.
*
* Return(s)   : None.
*
* Caller(s)   : USBD_SCSI_WrData().
*
* Note(s)     : None.
*********************************************************************************************************
*/


void  USBD_StorageWr_Exec (USBD_STORAGE_LUN  *p_storage_lun,
                      CPU_INT64U         blk_addr,
                      CPU_INT32U         nbr_blks,
                      CPU_INT08U        *p_data_buf,
                      USBD_ERR          *p_err)
{

	CPU_INT08U  lun;
    CPU_INT64U  mem_area_size;
    CPU_INT32U  mem_size_write;
// 	CPU_INT32U bn = blk_addr;
    // APP_TRACE_DBG(("USBD_StorageWr blk_addr=%d\n\r", bn));

    lun = (*p_storage_lun).LunNbr;

    if (lun > USBD_QSPI_FLASH_CFG_NBR_UNITS)
    {
       *p_err = USBD_ERR_SCSI_LU_NOTSUPPORTED;
       // APP_TRACE_DBG(("USBD_ERR_SCSI_LU_NOTSUPPORTED in USBD_StorageWr \n\r"));

        return;
    }

    // Validate address input
    mem_area_size = (blk_addr + nbr_blks) * USBD_QSPI_FLASH_CFG_BLK_SIZE;
    if (mem_area_size > USBD_QSPI_FLASH_SIZE)
    {
       *p_err = USBD_ERR_SCSI_LU_NOTRDY;
       // APP_TRACE_DBG(("USBD_ERR_SCSI_LU_NOTRDY in USBD_StorageWr \n\r"));
        return;
    }

    mem_size_write = nbr_blks * USBD_QSPI_FLASH_CFG_BLK_SIZE;

    // Find the relevant subsector
    size_t address 				= blk_addr * USBD_QSPI_FLASH_CFG_BLK_SIZE;
    CPU_INT16U subSectorNum 	= (address >> 12) & 0x1fff; // up to sub sector 8191
	CPU_INT32U subSectorAddress = subSectorNum*QSPI_FLASH_SUBSECTOR_SIZE;

	CPU_INT32U pageWithinSectorAddress = address & (QSPI_FLASH_SUBSECTOR_SIZE-1);

	// Read sub-sector
	USBD_StorageRdSubSector (p_storage_lun, subSectorNum, subSectorCopy, p_err);
	if (*p_err != USBD_ERR_NONE)
	{
     // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRdSubSector \n\r"));
         return;
	}

//	size_t num_of_blocks = QSPI_FLASH_SUBSECTOR_SIZE/ USBD_QSPI_FLASH_CFG_BLK_SIZE;

	// Enter block into the sub_sector copy
	memcpy(subSectorCopy+pageWithinSectorAddress, p_data_buf, mem_size_write);

	// Write the copy into the sector
	size_t num_of_pages = QSPI_FLASH_SUBSECTOR_SIZE/ QSPI_FLASH_PAGE_SIZE;

    /* Enable write operations ------------------------------------------- */
#ifdef MEMORY_TYPE_IS25LP128
     if (!QSPI_WriteEnable(&QSPIHandle, 0))
#else
      if (!QSPI_WriteEnable(&QSPIHandle, 1))
#endif
     {
         *p_err = USBD_ERR_FAIL;
         // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageWr first QSPI_WriteEnable \n\r"));
             return;
     }

	  CmdCplt = 0;

	  /* Erasing Sequence -------------------------------------------------- */
	  sCommand.Instruction 	= SUBSECTOR_ERASE_CMD;
#ifdef MEMORY_TYPE_IS25LP128
	  sCommand.InstructionMode 	= QSPI_INSTRUCTION_4_LINES;
	  sCommand.AddressMode 	= QSPI_ADDRESS_4_LINES;
#else
	  sCommand.AddressMode 	= QSPI_ADDRESS_1_LINE;
#endif
	  sCommand.AddressSize 	= QSPI_ADDRESS_24_BITS;
	  sCommand.Address     	= subSectorAddress;
	  sCommand.DataMode    	= QSPI_DATA_NONE;
	  sCommand.NbData		= 0;
	  sCommand.DummyCycles 	= 0;


	  if (HAL_QSPI_Command_IT(&QSPIHandle, &sCommand) != HAL_OK)
	  {
		  *p_err = USBD_ERR_FAIL;
			 // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageWr SUBSECTOR_ERASE_CMD\n\r"));
		  return;
	  }

	  // Wait for erase command to complete
	  if (!QSPI_Wait_For_Flag(&CmdCplt))
		{
		 *p_err = USBD_ERR_FAIL;
		 // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageWr QSPI_Wait_For_Flag(&CmdCplt)\n\r"));
		 return;
		}

//	  HAL_Delay(100); // zeev temp

	  /* Configure automatic polling mode to wait for end of former command ------- */
	  QSPI_AutoPollingMemReady(&QSPIHandle);

	  int i;

	  address = subSectorAddress;

	  CPU_INT08U* buff = subSectorCopy;
	  for (i=0; i<num_of_pages; i++, address+=QSPI_FLASH_PAGE_SIZE, buff+=QSPI_FLASH_PAGE_SIZE)
	  {
		  /* Enable write operations ------------------------------------------- */
#ifdef MEMORY_TYPE_IS25LP128
		  if (!QSPI_WriteEnable(&QSPIHandle, 0))
#else
		  if (!QSPI_WriteEnable(&QSPIHandle, 1))
#endif
		  {
		         // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageWr second QSPI_WriteEnable\n\r"));
			  *p_err = USBD_ERR_FAIL;
			  return;
		  }

    	  /* Writing Sequence ------------------------------------------------ */
    	  TxCplt = 0;
    	 sCommand.Instruction = EXT_QUAD_IN_FAST_PROG_CMD;
    	 sCommand.Address     = address;
		 sCommand.AddressMode = QSPI_ADDRESS_4_LINES;
#ifdef USE_DMA
		 sCommand.DataMode    = QSPI_DATA_4_LINES;
#else
		 sCommand.DataMode    = QSPI_DATA_4_LINES;
#endif
		 sCommand.NbData      = QSPI_FLASH_PAGE_SIZE;

		 if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
		 {
	         // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageWr EXT_QUAD_IN_FAST_PROG_CMD\n\r"));
			  *p_err = USBD_ERR_FAIL;
			  return;
		 }

#ifdef USE_DMA
		 if (HAL_QSPI_Transmit_DMA(&QSPIHandle, buff) != HAL_OK)
#else
		 if (HAL_QSPI_Transmit_IT(&QSPIHandle, buff) != HAL_OK)
#endif
		 {
	         // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageWr HAL_QSPI_Transmit_DMA\n\r"));
			  *p_err = USBD_ERR_FAIL;
			  return;
		 }

		 // Wait for write command to complete
		 if (!QSPI_Wait_For_Flag(&TxCplt))
		 {
			  *p_err = USBD_ERR_FAIL;
			  return;
		 }

		  /* Configure automatic polling mode to wait for end of former command ------- */
		  QSPI_AutoPollingMemReady(&QSPIHandle);
	  }

     *p_err = USBD_ERR_NONE;
#if 0
 	int y;

 //   if (!afterFirstWrite) return;

 	   // APP_TRACE_DBG(("USBD_StorageWr blk_addr=%d, subsector=%d pageWithinSectorAddress=%d \n\r", bn, subSectorAddress, pageWithinSectorAddress));
// 		for (y=0; y<mem_size_write; y++)
// 		{
// 			// APP_TRACE_DBG(("%d ", p_data_buf[y]));
// 		}
//		// APP_TRACE_DBG(("\n\r "));

 	      // Check if done
 	      USBD_StorageRd (p_storage_lun,
 	                      blk_addr,
 	                      nbr_blks,
 	                      testBuff,
 	                      p_err);

 	      CPU_BOOLEAN allOk = 1;

 	      for (i=0; i<512; i++)
 	      {
 	    	  if (testBuff[i] != p_data_buf[i])
 	    	  {
 //	    		  // APP_TRACE_DBG(("Wrong value at block %d byte %d ", bn, i));
 	    		 // APP_TRACE_DBG(("Write ERROR \n\r ", bn, i));
 	    		  allOk = 0;
 	    		  break;
 	    	  }
 	      }

 	      if (allOk)
 	      {
 	    	  // APP_TRACE_DBG(("Write OK \n\r ", bn, i));
 	      }

#endif
}

CPU_INT64U err_blks[100];
int num_err_blks = 0;

void  USBD_StorageWr (USBD_STORAGE_LUN  *p_storage_lun,
                      CPU_INT64U         blk_addr,
                      CPU_INT32U         nbr_blks,
                      CPU_INT08U        *p_data_buf,
                      USBD_ERR          *p_err)
{

	if (nbr_blks == 0)
	{
		err_blks[num_err_blks] = blk_addr;
		++num_err_blks;
		*p_err = USBD_ERR_FAIL;
		return ;
	}

	G_NVM_in_write_op = true;

	bool write_ok = false;
	int num_retries = 0;
	while (!write_ok)
	{
		 num_retries++;

		 if (num_retries > MAX_WRITE_RETRIES)
		 {
			 *p_err = USBD_ERR_FAIL;
			 G_NVM_in_write_op = false;
			 // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageWr num_retries > MAX_WRITE_RETRIES\n\r"));
			 return;
		 }

		 USBD_StorageWr_Exec (p_storage_lun,
		                 blk_addr,
		                 nbr_blks,
		                 p_data_buf,
		                 p_err);

		 if (*p_err != USBD_ERR_NONE)
		 {
			 continue;
		 }

		 if (!SINGLE_SOFTWARE_RESET_RESET_ENABLE(&QSPIHandle))
		 {
			 continue;
		 }

		// Check if done
		 USBD_StorageRd (p_storage_lun,
						  blk_addr,
						  nbr_blks,
						  testBuff,
						  p_err);
		 int res = memcmp(testBuff, p_data_buf, QSPI_FLASH_PAGE_SIZE);
		 write_ok = (*p_err == USBD_ERR_NONE) && ( res == 0);
	 } // while !write_ok

	G_NVM_in_write_op = false;
	*p_err = USBD_ERR_NONE;
}
/*
*********************************************************************************************************
*                                            USBD_StorageStatusGet()
*
* Description : Get the status of the storage medium.
*
* Argument(s) : p_storage_lun    Pointer to the logical unit storage structure.
*
*               p_err       Pointer to variable that will receive error code from this function.
*
*                               USBD_ERR_NONE                          Medium present.
*                               USB_ERR_SCSI_MEDIUM_NOTPRESENT         Medium not present.
*                               USBD_ERR_SCSI_LOG_UNIT_NOTSUPPORTED    Logical unit not supported.
*
* Return(s)   : None.
*
* Caller(s)   : USBD_SCSI_IssueCmd().
*
* Note(s)     : None.
*********************************************************************************************************
*/

void  USBD_StorageStatusGet (USBD_STORAGE_LUN  *p_storage_lun,
                             USBD_ERR           *p_err)
{
    CPU_INT08U  lun;

//    // APP_TRACE_DBG(("USBD_StorageStatusGet called\n\r"));

    lun = p_storage_lun->LunNbr;

    if (lun > USBD_QSPI_FLASH_CFG_NBR_UNITS)
    {
       *p_err = USBD_ERR_SCSI_LU_NOTSUPPORTED;
        return;
    }

    if (p_storage_lun->MediumPresent == DEF_FALSE)
    {
       *p_err = USBD_ERR_SCSI_MEDIUM_NOTPRESENT;
    }
    else
    {
       *p_err = USBD_ERR_NONE;
    }

}


/*
*********************************************************************************************************
*                                               USBD_StorageLock()
*
* Description : Lock the storage medium.
*
* Argument(s) : p_storage_lun    Pointer to the logical unit storage structure.
*
* Argument(s) : p_storage_lun   Pointer to the logical unit storage structure.
*
*               timeout_ms      Timeout in milliseconds.
*
*               p_err       Pointer to variable that will receive error code from this function.
*
*                               USBD_ERR_NONE               Medium successfully locked.
*                               USBD_ERR_SCSI_LOCK_TIMEOUT  Medium lock timed out.
*                               USBD_ERR_SCSI_LOCK          Medium lock failed.
* Return(s)   : None.
*
* Caller(s)   : USBD_SCSI_ProcessCmd().
*
* Note(s)     : None.
*********************************************************************************************************
*/

void  USBD_StorageLock (USBD_STORAGE_LUN  *p_storage_lun,
                        CPU_INT32U         timeout_ms,
                        USBD_ERR          *p_err)
{
    // APP_TRACE_DBG(("USBD_StorageLock called\n\r"));

    (void)&timeout_ms;

    p_storage_lun->MediumPresent = DEF_TRUE;
    *p_err = USBD_ERR_NONE;
}


/*
*********************************************************************************************************
*                                               USBD_StorageUnlock()
*
* Description : Unlock the storage medium.
*
* Argument(s) : p_storage_lun    Pointer to the logical unit storage structure.
*
*               p_err       Pointer to variable that will receive error code from this function.
*
*                               USBD_ERR_NONE   Medium successfully unlocked.
*
* Return(s)   : None.
*
* Caller(s)   : USBD_SCSI_ProcessCmd().
*               USBD_SCSI_Unlock().
*
* Note(s)     : None.
*********************************************************************************************************
*/

void  USBD_StorageUnlock (USBD_STORAGE_LUN  *p_storage_lun,
                          USBD_ERR          *p_err)
{
    // APP_TRACE_DBG(("USBD_StorageUnlock called\n\r"));

	   p_storage_lun->MediumPresent = DEF_FALSE;
	   *p_err                        = USBD_ERR_NONE;

}

/*
*********************************************************************************************************
*********************************************************************************************************
*                                             LOCAL FUNCTIONS
*********************************************************************************************************
*********************************************************************************************************
*/
static CPU_BOOLEAN ENABLE_ENTER_QUAD_PERIPHERAL_INTERFACE(QSPI_HandleTypeDef *hqspi)
 {
	 QSPI_CommandTypeDef     sCommand;
	 QSPI_AutoPollingTypeDef sConfig;

	  // Enable write operations ------------------------------------------
	  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
	  sCommand.Instruction       = ENTER_QUAD_CMD;
	  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
	  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	  sCommand.DataMode          = QSPI_DATA_NONE;
	  sCommand.DummyCycles       = 0;
	  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
	  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	  CmdCplt = 0;
	  if (HAL_QSPI_Command_IT(hqspi,&sCommand) != HAL_OK)
	  {
	    return 0;
	  }

	  // Wait for erase command to complete
	  if (!QSPI_Wait_For_Flag(&CmdCplt))
	{
	 // APP_TRACE_DBG(("USBD_ERR_FAIL in ENABLE_ENTER_QUAD_PERIPHERAL_INTERFACE QSPI_Wait_For_Flag(&CmdCplt)\n\r"));
	 return 0;;
	}


	  return 1;
 }

static CPU_BOOLEAN QSPI_DummyCyclesCfg(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef sCommand;
  uint8_t reg;

  /* Read Volatile Configuration register --------------------------- */
  sCommand.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
  sCommand.Instruction       = READ_VOL_CFG_REG_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
//  sCommand.DataMode          = QSPI_DATA_1_LINE;
  sCommand.DataMode          = QSPI_DATA_4_LINES;
  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;
  sCommand.NbData            = 1;

  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
	  return 0;
  }

  if (HAL_QSPI_Receive(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
	  return 0;
  }

  /* Enable write operations ---------------------------------------- */
#ifdef MEMORY_TYPE_IS25LP128
	QSPI_WriteEnable(&QSPIHandle, 0);
#else
	QSPI_WriteEnable(&QSPIHandle, 1);
#endif

  /* Write Volatile Configuration register (with new dummy cycles) -- */
  sCommand.Instruction = WRITE_VOL_CFG_REG_CMD;
  MODIFY_REG(reg, 0xF0, (DUMMY_CLOCK_CYCLES_READ_QUAD << POSITION_VAL(0xF0)));

  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
	  return 0;
  }

  if (HAL_QSPI_Transmit(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
	  return 0;
  }

  return 1;
}

/**
  * @brief QSPI MSP Initialization
  *        This function configures the hardware resources used in this example:
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration
  *           - NVIC configuration for QSPI interrupt
  * @param hqspi: QSPI handle pointer
  * @retval None
  */

void HAL_QSPI_MspInit(QSPI_HandleTypeDef* qspiHandle)
{
  /* Configure QSPI device interrupt in interrupt controller (registering BSP ISR) */
  BSP_IntVectSet(BSP_INT_QUADSPI, &QUADSPI_IRQHandler);
#ifdef USE_DMA
  BSP_IntVectSet(BSP_INT_ID_DMA2_CH7, &QUADSPI_DMA_IRQ_HANDLER);
#endif


  GPIO_InitTypeDef GPIO_InitStruct;
  if(qspiHandle->Instance==QUADSPI)
  {
  /* USER CODE BEGIN QUADSPI_MspInit 0 */

  /* USER CODE END QUADSPI_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_QSPI_CLK_ENABLE();

    /**QUADSPI GPIO Configuration
    PC11     ------> QUADSPI_BK2_NCS
    PG9     ------> QUADSPI_BK2_IO2
    PA7     ------> QUADSPI_CLK
    PE8     ------> QUADSPI_BK2_IO1
    PH2     ------> QUADSPI_BK2_IO0
    PE10     ------> QUADSPI_BK2_IO3
    */
    // PC11     ------> QUADSPI_BK2_NCS
    GPIO_InitStruct.Pin = GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_QSPI;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    // PG9     ------> QUADSPI_BK2_IO2
    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_QSPI;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

    // PA7     ------> QUADSPI_CLK
    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF10_QSPI;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    // PE8     ------> QUADSPI_BK2_IO1
    // PE10     ------> QUADSPI_BK2_IO3
    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF10_QSPI;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

    // PH2     ------> QUADSPI_BK2_IO0
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_QSPI;
    HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

    /* QUADSPI DMA Init */
    /* QUADSPI Init */
    hdma.Instance = DMA2_Stream7;
    hdma.Init.Channel = DMA_CHANNEL_3;
    hdma.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma.Init.MemInc = DMA_MINC_ENABLE;
    hdma.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma.Init.Mode = DMA_NORMAL;
    hdma.Init.Priority = DMA_PRIORITY_LOW;
    hdma.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    HAL_DMA_Init(&hdma);

    __HAL_LINKDMA(qspiHandle, hdma, hdma);

    /* QUADSPI interrupt Init */
    HAL_NVIC_SetPriority(QUADSPI_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(QUADSPI_IRQn);
  /* USER CODE BEGIN QUADSPI_MspInit 1 */

  /* USER CODE END QUADSPI_MspInit 1 */
  }
}
#if 0
void HAL_QSPI_MspInit(QSPI_HandleTypeDef *hqspi)
{
	 __HAL_RCC_GPIOA_CLK_ENABLE();
	  __HAL_RCC_GPIOB_CLK_ENABLE();
	  __HAL_RCC_GPIOC_CLK_ENABLE();
	  __HAL_RCC_GPIOD_CLK_ENABLE();
	  __HAL_RCC_GPIOE_CLK_ENABLE();
	  __HAL_RCC_GPIOF_CLK_ENABLE();
	  __HAL_RCC_GPIOG_CLK_ENABLE();
	  __HAL_RCC_GPIOH_CLK_ENABLE();
	  __HAL_RCC_GPIOI_CLK_ENABLE();


  /* Configure QSPI device interrupt in interrupt controller (registering BSP ISR) */
  BSP_IntVectSet(BSP_INT_QUADSPI, &QUADSPI_IRQHandler);
#ifdef USE_DMA
  BSP_IntVectSet(BSP_INT_ID_DMA2_CH7, &QUADSPI_DMA_IRQ_HANDLER);
#endif

  GPIO_InitTypeDef GPIO_InitStruct;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable the QuadSPI memory interface clock */
  QSPI_CLK_ENABLE();
  /* Reset the QuadSPI memory interface */
//  QSPI_FORCE_RESET();
//  QSPI_RELEASE_RESET();

  /* Enable GPIO clocks */
  QSPI_CS_GPIO_CLK_ENABLE();
  QSPI_CLK_GPIO_CLK_ENABLE();
  QSPI_D0_GPIO_CLK_ENABLE();
  QSPI_D1_GPIO_CLK_ENABLE();
  QSPI_D2_GPIO_CLK_ENABLE();
  QSPI_D3_GPIO_CLK_ENABLE();

#ifdef USE_DMA
  /* Enable DMA clock */
  QSPI_DMA_CLK_ENABLE();
#endif

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* QSPI CS GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_CS_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = QSPI_CS_AF;
  HAL_GPIO_Init(QSPI_CS_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI CLK GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_CLK_PIN;
  GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = QSPI_CLK_AF;
  HAL_GPIO_Init(QSPI_CLK_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D0 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_D0_PIN;
  GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull 		= GPIO_NOPULL;
  GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = QSPI_D0_AF;
  HAL_GPIO_Init(QSPI_D0_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D1 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_D1_PIN;
  GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull 		= GPIO_NOPULL;
  GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = QSPI_D0_AF;
  HAL_GPIO_Init(QSPI_D1_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D2 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_D2_PIN;
  GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull 		= GPIO_NOPULL;
  GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = QSPI_D2_AF;
  HAL_GPIO_Init(QSPI_D2_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D3 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_D3_PIN;
  GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull 		= GPIO_NOPULL;
  GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = QSPI_D3_AF;
  HAL_GPIO_Init(QSPI_D3_GPIO_PORT, &GPIO_InitStruct);

  /*##-3- Configure the NVIC for QSPI #########################################*/
  /* NVIC configuration for QSPI interrupt */
  HAL_NVIC_SetPriority(QUADSPI_IRQn, 0x0F, 0);
  HAL_NVIC_EnableIRQ(QUADSPI_IRQn);

#ifdef USE_DMA
  /*##-4- Configure the DMA channel ###########################################*/
  /* QSPI DMA channel configuration */
  hdma.Init.Channel             = QSPI_DMA_CHANNEL;
  hdma.Init.PeriphInc           = DMA_PINC_DISABLE;
  hdma.Init.MemInc              = DMA_MINC_ENABLE;
  hdma.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hdma.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hdma.Init.Mode                = DMA_NORMAL;
  hdma.Init.Priority            = DMA_PRIORITY_LOW;
  hdma.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;        /* FIFO mode disabled     */
  hdma.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hdma.Init.MemBurst            = DMA_MBURST_SINGLE;           /* Memory burst           */
  hdma.Init.PeriphBurst         = DMA_PBURST_SINGLE;           /* Peripheral burst       */
  hdma.Init.Direction 			= DMA_PERIPH_TO_MEMORY;
  hdma.Instance                 = QSPI_DMA_INSTANCE;


 HAL_DMA_Init(&hdma);

 __HAL_LINKDMA(hqspi, hdma, hdma);

  /* NVIC configuration for DMA interrupt */
  HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 0x00, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);

#endif

}
#endif
/**
  * @brief QSPI MSP De-Initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO and NVIC configuration to their default state
  * @param hqspi: QSPI handle pointer
  * @retval None
  */
void HAL_QSPI_MspDeInit(QSPI_HandleTypeDef *hqspi)
{
  /*##-1- Disable the NVIC for QSPI ###########################################*/
  HAL_NVIC_DisableIRQ(QUADSPI_IRQn);

  /*##-2- Disable peripherals and GPIO Clocks ################################*/
  /* De-Configure QSPI pins */
  HAL_GPIO_DeInit(QSPI_CS_GPIO_PORT, QSPI_CS_PIN);
  HAL_GPIO_DeInit(QSPI_CLK_GPIO_PORT, QSPI_CLK_PIN);
  HAL_GPIO_DeInit(QSPI_D0_GPIO_PORT, QSPI_D0_PIN);
  HAL_GPIO_DeInit(QSPI_D1_GPIO_PORT, QSPI_D1_PIN);
  HAL_GPIO_DeInit(QSPI_D2_GPIO_PORT, QSPI_D2_PIN);
  HAL_GPIO_DeInit(QSPI_D3_GPIO_PORT, QSPI_D3_PIN);

  /*##-3- Reset peripherals ##################################################*/
  /* Reset the QuadSPI memory interface */
  QSPI_FORCE_RESET();
  QSPI_RELEASE_RESET();

  /* Disable the QuadSPI memory interface clock */
  QSPI_CLK_DISABLE();
}

/**
  * @brief  This function send a Write Enable and wait it is effective.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static CPU_BOOLEAN QSPI_WriteEnable(QSPI_HandleTypeDef *hqspi, CPU_BOOLEAN use_inst_single_Line)
{
  QSPI_CommandTypeDef     sCommand;
  QSPI_AutoPollingTypeDef sConfig;

  /* Enable write operations ------------------------------------------ */
	if (use_inst_single_Line)
	{
	  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
	}
  else
	{
	  sCommand.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
	}

  sCommand.Instruction       = WRITE_ENABLE_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_NONE;
  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
//  if (HAL_QSPI_Command_IT(&QSPIHandle, &sCommand) != HAL_OK)
  {
      // APP_TRACE_DBG(("Fail HAL_QSPI_Command in QSPI_WriteEnable \n\r"));
    return 0;
  }

  /* Configure automatic polling mode to wait for write enabling ---- */
  sConfig.Match           = 0x02;
  sConfig.Mask            = 0x02;
  sConfig.MatchMode       = QSPI_MATCH_MODE_AND;
  sConfig.StatusBytesSize = 1;
  sConfig.Interval        = 0x10;
  sConfig.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  sCommand.Instruction    = READ_STATUS_REG_CMD;
#ifdef MEMORY_TYPE_IS25LP128
  sCommand.DataMode   = QSPI_DATA_4_LINES;
#else
  sCommand.DataMode   = QSPI_DATA_1_LINE;
#endif

  if (HAL_QSPI_AutoPolling(&QSPIHandle, &sCommand, &sConfig, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
      // APP_TRACE_DBG(("Fail HAL_QSPI_AutoPolling in QSPI_WriteEnable \n\r"));
    return 0;
  }

  return 1;
}

/**
  * @brief  This function handles QUADSPI interrupt request.
  * @param  None
  * @retval None
  */
static void QUADSPI_IRQHandler(void)
{
	HAL_QSPI_IRQHandler(&QSPIHandle);
}

/**
  * @brief  This function read the SR of the memory and wait the EOP.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static CPU_BOOLEAN QSPI_AutoPollingMemReady(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef     sCommand;
  QSPI_AutoPollingTypeDef sConfig;

  /* Configure automatic polling mode to wait for memory ready ------ */
#ifdef MEMORY_TYPE_IS25LP128
  sCommand.InstructionMode   = QSPI_INSTRUCTION_4_LINES;
  sCommand.DataMode          = QSPI_DATA_4_LINES;
#else
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.DataMode          = QSPI_DATA_1_LINE;
#endif
  sCommand.Instruction       = READ_STATUS_REG_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;

  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  sConfig.Match           = 0x00;
  sConfig.Mask            = 0x01;
  sConfig.MatchMode       = QSPI_MATCH_MODE_AND;
  sConfig.StatusBytesSize = 1;
  sConfig.Interval        = 0x10;
  sConfig.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  QSPIHandle.Timeout		= QSPI_MEM_READY_TIMEOUT;

  StatusMatch = 0;
  if (HAL_QSPI_AutoPolling_IT(&QSPIHandle, &sCommand, &sConfig) != HAL_OK)
  {
      // APP_TRACE_DBG(("Fail HAL_QSPI_AutoPolling in QSPI_AutoPollingMemReady \n\r"));
    return 0;
  }

  return QSPI_Wait_For_Flag(&StatusMatch);
}

/**
  * @brief  Status Match callbacks
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_StatusMatchCallback(QSPI_HandleTypeDef *hqspi)
{
  StatusMatch = 1;
}
/**
  * @brief  Command completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_CmdCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  CmdCplt = 1;
}

/**
  * @brief  Rx Transfer completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_RxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  RxCplt = 1;
}

/**
  * @brief  Tx Transfer completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_TxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  TxCplt = 1;
}

CPU_BOOLEAN QSPI_Wait_For_Flag(CPU_INT08U* flag)
{
	CPU_INT32U timeOut = MAX_WAIT_FOR_QSPI_OP;

	while (!*flag && timeOut)
	{
		timeOut--;
	}

	if (timeOut)
	{
		return 1;
	}
	else
	{
		APP_TRACE_DBG("TIMEOUT in QSPI_Wait_For_Flag \n\r");
		return 0;
	}
}


void  USBD_StorageRdSubSector (USBD_STORAGE_LUN  *p_storage_lun,
                      CPU_INT32U         subSectorNum,
                      CPU_INT08U        *p_data_buf,
                      USBD_ERR          *p_err)
{

    CPU_INT08U  lun;
    CPU_INT32U  mem_area_size;
    CPU_INT32U  mem_size_read;

    // APP_TRACE_DBG(("USBD_StorageRdSubSector subSectorNum=%d\n\r", subSectorNum));
// 	CPU_INT32U y;

    lun = (*p_storage_lun).LunNbr;

     if (lun > USBD_QSPI_FLASH_CFG_NBR_UNITS)
     {
        *p_err = USBD_ERR_SCSI_LU_NOTSUPPORTED;
        // APP_TRACE_DBG(("USBD_ERR_SCSI_LU_NOTSUPPORTED in USBD_StorageRd \n\r"));
         return;
     }

     /* Configure automatic polling mode to wait for end of program ----- */
     if (!QSPI_AutoPollingMemReady(&QSPIHandle))
     {
		 *p_err = USBD_ERR_FAIL;
	        // APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd \n\r"));
		 return;
     }

     size_t address = subSectorNum * QSPI_FLASH_SUBSECTOR_SIZE;

	 RxCplt = 0;

#ifndef MEMORY_TYPE_IS25LP128
	 /* Configure Volatile Configuration register (with new dummy cycles) */
	 if (!QSPI_DummyCyclesCfg(&QSPIHandle))
	 {
		 *p_err = USBD_ERR_FAIL;
			// APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd \n\r"));
		 return;
	 }
#endif

	 /* Reading Sequence ------------------------------------------------ */
#ifdef MEMORY_TYPE_IS25LP128
	 sCommand.Instruction = FAST_READ_CMD;
#else
	 sCommand.Instruction = QUAD_INOUT_FAST_READ_CMD;
#endif
	 sCommand.DummyCycles = DUMMY_CLOCK_CYCLES_READ_QUAD;
	 sCommand.NbData      = QSPI_FLASH_SUBSECTOR_SIZE;
	 sCommand.Address     = address;
	 sCommand.AddressMode = QSPI_ADDRESS_4_LINES;
	 sCommand.DataMode    = QSPI_DATA_4_LINES;

	 if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	 {
		 *p_err = USBD_ERR_FAIL;
			// APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd \n\r"));
		 return;
	 }
#ifdef USE_DMA
	 if (HAL_QSPI_Receive_DMA(&QSPIHandle, p_data_buf) != HAL_OK)
#else
	 if (HAL_QSPI_Receive_IT(&QSPIHandle, p_data_buf) != HAL_OK)
#endif
	 {
		 *p_err = USBD_ERR_FAIL;
			// APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd \n\r"));
		 return;
	 }

	 // Wait for read command to complete
	 if (!QSPI_Wait_For_Flag(&RxCplt))
	 {
		 *p_err = USBD_ERR_FAIL;
			// APP_TRACE_DBG(("USBD_ERR_FAIL in USBD_StorageRd \n\r"));
		 return;
	 }

     *p_err = USBD_ERR_NONE;



}

void MX_GPIO_Init(void)
{


  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOI_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
#if 1
  HAL_GPIO_WritePin(GPIOI, USB0_SEL_SW_Pin|WiFi_MR_CE_Pin|Wifi_Reset_N_Pin|Wifi_CE_Pin
                          |USB_HS_Dest_Sel_Pin|USB1_SEL_SW_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PI2 PI1 PI0 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_1|GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : PD1 PD7 PD4 PD2
                           PD5 PD3 PD0 PD6
                           PD13 PD11 PD15 PD14
                           PD8 PD12 PD10 PD9 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_7|GPIO_PIN_4|GPIO_PIN_2
                          |GPIO_PIN_5|GPIO_PIN_3|GPIO_PIN_0|GPIO_PIN_6
                          |GPIO_PIN_13|GPIO_PIN_11|GPIO_PIN_15|GPIO_PIN_14
                          |GPIO_PIN_8|GPIO_PIN_12|GPIO_PIN_10|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PG12 PG11 PG10 PG15
                           PG13 PG5 PG4 PG6
                           PG3 PG2 PG8 PG7
                           PG0 PG1 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_11|GPIO_PIN_10|GPIO_PIN_15
                          |GPIO_PIN_13|GPIO_PIN_5|GPIO_PIN_4|GPIO_PIN_6
                          |GPIO_PIN_3|GPIO_PIN_2|GPIO_PIN_8|GPIO_PIN_7
                          |GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : PIPin PIPin PIPin PIPin
                           PIPin PIPin */
  GPIO_InitStruct.Pin = USB0_SEL_SW_Pin|WiFi_MR_CE_Pin|Wifi_Reset_N_Pin|Wifi_CE_Pin
                          |USB_HS_Dest_Sel_Pin|USB1_SEL_SW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);
#endif
#if 1
  /*Configure GPIO pins : PH13 PH14 PH15 PH5
                           PH9 PH10 PH8 PH11
                           PH3 PH12 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15|GPIO_PIN_5
                          |GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_8|GPIO_PIN_11
                          |GPIO_PIN_3|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : PB8 PB9 PB6 PB15
                           PB14 PB2 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_6|GPIO_PIN_15
                          |GPIO_PIN_14|GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PE5 PE14 PE9 PE13
                           PE12 PE11 PE7 PE15 */
  GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_14|GPIO_PIN_9|GPIO_PIN_13
                          |GPIO_PIN_12|GPIO_PIN_11|GPIO_PIN_7|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PC12 PC10 PC13 PC14
                           PC15 PC6 PC8 PC9
                           PC7 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_10|GPIO_PIN_13|GPIO_PIN_14
                          |GPIO_PIN_15|GPIO_PIN_6|GPIO_PIN_8|GPIO_PIN_9
                          |GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = WiFi_IRQ_n_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(WiFi_IRQ_n_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
//  GPIO_InitStruct.Pin = GPIO_PIN_8;
//  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//  GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
//  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA10 PA12 PA11 PA1
                           PA0 PA4 PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_0|GPIO_PIN_4|GPIO_PIN_6; //  GPIO_PIN_12|GPIO_PIN_11|GPIO_PIN_10|

  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = USB1_SEL_HW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB1_SEL_HW_GPIO_Port, &GPIO_InitStruct);
#endif
#if 1
  /*Configure GPIO pins : PF0 PF2 PF15 PF1
                           PF3 PF5 PF13 PF4
                           PF11 PF10 PF14 PF12 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_2|GPIO_PIN_15|GPIO_PIN_1
                          |GPIO_PIN_3|GPIO_PIN_5|GPIO_PIN_13|GPIO_PIN_4
                          |GPIO_PIN_11|GPIO_PIN_10|GPIO_PIN_14|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = USB0_SEL_HW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB0_SEL_HW_GPIO_Port, &GPIO_InitStruct);
#endif
#if 1
  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = MODEL_SEL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(MODEL_SEL_GPIO_Port, &GPIO_InitStruct);
#endif

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);
}


#ifdef USE_DMA
/**
  * @brief  This function handles QUADSPI DMA interrupt request.
  * @param  None
  * @retval None
  */
void QUADSPI_DMA_IRQ_HANDLER(void)
{
  HAL_DMA_IRQHandler(QSPIHandle.hdma);
}

/**
  * Enable DMA controller clock
  */
void MX_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);

}

#endif


CPU_BOOLEAN SINGLE_SOFTWARE_RESET_RESET_ENABLE(QSPI_HandleTypeDef *hqspi)
{
	  CmdCplt = 0;
      sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
	  sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
	  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
	  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

	  sCommand.Instruction       = RESET_ENABLE_CMD;
	  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
	  sCommand.DataMode          = QSPI_DATA_NONE;
	  sCommand.DummyCycles       = 0;

	  if (HAL_QSPI_Command_IT(hqspi, &sCommand) != HAL_OK)
	  {
		  return 0;
	  }

	  if (!QSPI_Wait_For_Flag(&CmdCplt))
	  {
		  return 0;
	  }

	  sCommand.Instruction       = RESET_MEMORY_CMD;
	  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
	  sCommand.DataMode          = QSPI_DATA_NONE;
	  sCommand.DummyCycles       = 0;

	  CmdCplt = 0;

	  if (HAL_QSPI_Command_IT(hqspi, &sCommand) != HAL_OK)
	  {
		  return 0;
	  }


	  return QSPI_Wait_For_Flag(&CmdCplt);
}

/*
*********************************************************************************************************
*                                          END
*********************************************************************************************************
*/
#endif // #ifdef QSPIFLASH_STORAGE
