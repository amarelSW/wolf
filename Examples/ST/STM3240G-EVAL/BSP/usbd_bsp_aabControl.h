
#ifndef USBD_BSP_AAACONTROL_H
#define USBD_BSP_AAACONTROL_H

#include <source/usbd_core.h>

const  USBD_DRV_EP_INFO  USBD_DrvEP_InfoTbl_AABControl[] =                   
    {
    		{USBD_EP_INFO_TYPE_CTRL | USBD_EP_INFO_DIR_IN,  0u,   64u},
    		{USBD_EP_INFO_TYPE_CTRL | USBD_EP_INFO_DIR_OUT,  0u,   64u},
		    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_OUT, 1u, 1024u},
		    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_IN,  1u, 1024u},
		    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_OUT, 2u, 1024u},
		    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_IN,  2u, 1024u},
		    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_OUT, 3u, 1024u},
		    {USBD_EP_INFO_TYPE_ISOC | USBD_EP_INFO_TYPE_BULK | USBD_EP_INFO_TYPE_INTR | USBD_EP_INFO_DIR_IN,  3u, 1024u},
			{DEF_BIT_NONE                                 ,  0u,    0u}
    };


#endif
